from django.db import models
from principal.models import Usuario
# Create your models here.


class Categoria(models.Model):
    nombre = models.CharField(max_length=150, blank=False)
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE, blank=False)

    def __str__(self):
        return self.nombre


class Contacto(models.Model):
    nombre = models.CharField(max_length=200, blank=False)
    telefono = models.CharField(max_length=30, blank=False)
    direccion = models.CharField(max_length=200, blank=False)
    categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE, blank=False)
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE, blank=False)
    activo=models.BooleanField(default=True)
    desde=models.TimeField((u"Conversation Time"), blank=False)
    hasta=models.TimeField((u"Conversation Time"), blank=False)

    def __str__(self):
        return self.nombre

class contactos_usuarios(models.Model):
    contacto=models.OneToOneField(Contacto,on_delete=models.CASCADE,unique=True)
    usuario=models.OneToOneField(Usuario,on_delete=models.CASCADE,unique=True)


    def __str__(self):
        return "Contacto: "+str(self.contacto)+" Usuario: "+str(self.usuario)

class Menu(models.Model):
    nombre = models.CharField(max_length=200,blank=False)
    contacto = models.ForeignKey(Contacto, on_delete=models.CASCADE, blank=False)
    def __str__(self):
        return self.nombre

class CatMenu(models.Model):
    nombre = models.CharField(max_length=200,blank=False)
    
    def __str__(self):
        return self.nombre

class ItemsMenu(models.Model):
    nombre = models.CharField(max_length=100,blank=False)
    precio = models.FloatField(blank=True,null=True)
    #El item pertenece a un menu
    menu=models.ForeignKey(Menu,on_delete=models.CASCADE,blank=False,null=False) 
    #El item pertenece a una categoria   
    CatMenu = models.ForeignKey(CatMenu,on_delete=models.CASCADE, blank=True,null=True)


    def __str__(self):
        return self.nombre


class Sugerencias(models.Model):
    nombre = models.CharField(max_length=200, blank=False)
    telefono = models.CharField(max_length=30, blank=False)
    direccion = models.CharField(max_length=200,  blank=True)
    categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE, blank=False)
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE, blank=False)
    aplicada = models.BooleanField(default=False)
    fecha = models.DateField(auto_now_add=True)

    def __str__(self):
        return "Sugerencia de: "+self.nombre


class Comentarios(models.Model):
    comentario = models.CharField(max_length=500, blank=False)
    fecha = models.DateField(auto_now_add=True)
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE, blank=False)
    contacto = models.ForeignKey(Contacto, on_delete=models.CASCADE, blank=False)

    def __str__(self):
        return self.comentario


class Rating(models.Model):
    rating = models.FloatField(blank=False)
    fecha = models.DateField(auto_now_add=True)
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE, blank=False)
    contacto = models.ForeignKey(Contacto, on_delete=models.CASCADE, blank=False)

    def __str__(self):
        return str(self.rating)
