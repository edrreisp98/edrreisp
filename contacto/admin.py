from django.contrib import admin
from .models import Contacto, Categoria, Sugerencias, Rating, Comentarios, Menu, CatMenu, ItemsMenu, contactos_usuarios
# Register your models here.
admin.site.register(Contacto)
admin.site.register(Categoria)
admin.site.register(Sugerencias)
admin.site.register(Rating)
admin.site.register(Comentarios)
admin.site.register(Menu)
admin.site.register(CatMenu)
admin.site.register(ItemsMenu)
admin.site.register(contactos_usuarios)
