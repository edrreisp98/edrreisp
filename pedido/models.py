from django.db import models
from principal.models import Usuario
from contacto.models import Contacto, ItemsMenu
# Create your models here.
class ConteoPorContactos(models.Model):
    contacto = models.ForeignKey(Contacto, on_delete=models.CASCADE, blank=False)
    cantidad=models.IntegerField(blank=False)

    def __str__(self):
        return "Contacto: "+str(self.contacto)+" Total de pedidos: "+str(self.cantidad)

class Pedido(models.Model):
    descripcion = models.CharField(max_length=150, blank=False)
    contacto = models.ForeignKey(Contacto, on_delete=models.CASCADE, blank=False)
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE, blank=False)
    estado = models.CharField(max_length=100, blank=False, default="Activo")
    fecha = models.DateField(auto_now_add=True)
    hora = models.TimeField(auto_now_add=True)
    itbis=models.BooleanField(blank=False)
    total =models.FloatField(blank=False)

    def __str__(self):
        return "Usuario: "+str(self.usuario)+" Descripcion:"+str(self.descripcion)

class DetallePedido(models.Model):
	pedido=models.ForeignKey(Pedido,on_delete=models.CASCADE,blank=False)
	item =models.ForeignKey(ItemsMenu,on_delete=models.PROTECT,blank=False)
	cantidad=models.IntegerField(blank=False)
	precio=models.FloatField(blank=False)
	importe=models.FloatField(blank=False)

	def __str__(self):
		return str(self.item)


class Notificacion(models.Model):
    tipo_notificacion=models.CharField(max_length=150,blank=False)
    usuario =models.ForeignKey(Usuario,on_delete=models.CASCADE,blank=False)
    pedido=models.ForeignKey(Pedido,on_delete=models.CASCADE,blank=True,null=True)
    contacto=models.ForeignKey(Contacto,on_delete=models.CASCADE,blank=True,null=True)
    estado=models.CharField(max_length=150,blank=False,default="Activa")
    fecha = models.DateField(auto_now_add=True, blank=False)
    hora =models.TimeField(auto_now_add=True,blank=False)

    def __str__(self):
        return "NOTIFICACION: "+str(self.pedido)+ "TIPO-NOTIFICACION: "+str(self.tipo_notificacion)


class Monto(models.Model):
    usuario=models.ForeignKey(Usuario,on_delete=models.CASCADE,blank=False)
    total=models.FloatField(blank=False)

    def __str__(self):
        return "Usuario: "+str(self.usuario)+" Monto:"+str(self.total)
