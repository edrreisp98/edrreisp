from django.contrib import admin
from .models import Pedido, DetallePedido, Notificacion, Monto,ConteoPorContactos
# Register your models here.
admin.site.register(Pedido)
admin.site.register(DetallePedido)
admin.site.register(Notificacion)
admin.site.register(Monto)
admin.site.register(ConteoPorContactos)