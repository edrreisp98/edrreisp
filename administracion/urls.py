

from django.conf.urls import url
from . import views

app_name = 'adminitracion'

urlpatterns = [


    #proyecto/administracion/aceptar_sugerencia/id
    url(r'aceptar_sugerencia/(?P<idsugerencia>[0-9]+)', views.aceptar_sugerencia, name="aceptar_sugerencia"),

    # proyecto.com/administracion
    url(r'^$', views.administracion, name="administracion"),


    #URL DE categoria_menus
    # proyecto.com/administracion/categoria_menus
    
    # proyecto.com/administracion/categoria_menus
    url(r'^categoria_menus$', views.categoria_menus, name="categoria_menus"),

    # URL de MENUS
    # proyecto.com/administracion/menus
    url(r'^menus$', views.menus, name="menus"),

    # URL de Montos
    # proyecto.com/administracion/montos
    url(r'^montos$', views.montos, name="montos"),


    # URL de USUARIOS
    # proyecto.com/administracion/usuarios
    url(r'^usuarios$', views.usuarios, name="usuarios"),


    # URL de CATEGORIAS
    # proyecto.com/administracion/categoria
    url(r'^categorias', views.categorias, name="categorias"),

    #proyecto/administracion/get_categories
    url(r'get_categories', views.get_categories, name="get_categories"),


    #proyecto/administracion/obtener_categorias
    url(r'obtener_categorias/(?P<filtro>.+)/(?P<orden>.+)/(?P<texto>.+)', views.obtener_categorias, name="obtener_categorias"),

    #proyecto/administracion/obtener_datos_categoria/id
    url(r'obtener_datos_categoria/(?P<idcategoria>[0-9]+)', views.obtener_datos_categoria, name="obtener_datos_categoria"),


    #proyecto/administracion/eliminar_categoria/id
    url(r'eliminar_categoria/(?P<idcategoria>[0-9]+)', views.eliminar_categoria, name="eliminar_categoria"),

    # proyecto/administracion/registro_categoria
    url(r'registro_categoria', views.registro_categoria, name="registro_categoria"),

    # proyecto/administracion/modifica_categoria
    url(r'modifica_categoria', views.modifica_categoria, name="modifica_categoria"),



    # URL de CONTACTOS
    # proyecto.com/administracion/contactos
    url(r'^contactos', views.contactos, name="contactos"),

    #proyecto/administracion/get_contacts
    url(r'get_contacts', views.get_contacts, name="get_contacts"),


    #proyecto/administracion/obtener_contactos/filtro/orden/texto
    url(r'obtener_contactos/(?P<filtro>.+)/(?P<orden>.+)/(?P<texto>.+)', views.obtener_contactos, name="obtener_contactos"),


    #proyecto/ver_menu/idmenu
    url(r'ver_menu/(?P<idmenu>[0-9]+)', views.ver_menu, name="ver_menu"),


    #proyecto/administracion/obtener_datos_contacto/id
    url(r'obtener_datos_contacto/(?P<idcontacto>[0-9]+)', views.obtener_datos_contacto, name="obtener_datos_contacto"),

    #proyecto/administracion/eliminar_contacto/id
    url(r'eliminar_contacto/(?P<idcontacto>[0-9]+)', views.eliminar_contacto, name="eliminar_contacto"),


    # proyecto/administracion/registro_contacto
    url(r'registro_contacto', views.registro_contacto, name="registro_contacto"),

    # proyecto/administracion/modifica_contacto
    url(r'modifica_contacto', views.modifica_contacto, name="modifica_contacto"),


    
    #proyecto/administracion/activar_contacto/id
    url(r'activar_contacto/(?P<idcontacto>[0-9]+)', views.activar_contacto, name="activar_contacto"),

    #proyecto/administracion/desactivar_contacto/id
    url(r'deactivate_contact/(?P<idcontacto>[0-9]+)', views.deactivate_contact, name="deactivate_contact"),

    




    # URL de SUGERENCIAS
    # proyecto.com/administracion/sugerencias
    url(r'^sugerencias', views.sugerencias, name="sugerencias"),
]


