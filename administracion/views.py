#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.views.defaults import page_not_found
from django.shortcuts import render, HttpResponse
from principal.models import Usuario, Rol, PreferenciaNotificacion
from contacto.models import Categoria, Contacto, Sugerencias, Menu, CatMenu, ItemsMenu, contactos_usuarios
from django.core import serializers
from django.views.decorators.csrf import ensure_csrf_cookie
from django.core.serializers.json import DjangoJSONEncoder
import json
from django.db.models import Sum
from django.db.models import Avg
# Create your views here.

def ver_menu(request,idmenu):
    if Soy_Admin(request):
        sesion = {}
        sesion = {
            'idusuario': request.session['idusuario'],
            'usuario': request.session['usuario'],
            'rol': request.session['rol']
        }
        try:
            menu = Menu.objects.get(pk=idmenu)
            nombre = menu.nombre
            contacto_data=Menu.objects.filter(pk=idmenu).values('contacto__pk')
        except Exception as e:
            nombre = ""
        preferencia=None
        if not request.session['idusuario'] is None:
            preferencia=PreferenciaNotificacion.objects.filter(usuario=Usuario.objects.get(pk=request.session['idusuario'])).values('n_alerta','n_correo','n_sonido','horas','minutos')
        contexto = {
            'preferencias':preferencia,
            'contacto_data':contacto_data,
            'idmenu':idmenu,
            'sesion': sesion,
            'nombre': nombre,
            'categorias_menu':CatMenu.objects.all(),
            'menu': 'SI',
            'es_admin': Soy_Admin(request)
        }
        return render(request, 'administracion/detalle_menus.html', contexto)
    else:
        return render(request, 'principal/principal.html')

def generarContexto(request):
    es_admin = False
    if 'idusuario' and 'usuario' and 'rol' in request.session:
        if request.session['idusuario'] != "" and request.session['usuario'] != "" and request.session['rol'] !="":
            sesion = {}
            sesion = {
                'idusuario': request.session['idusuario'],
                'usuario': request.session['usuario'],
                'rol': request.session['rol']
            }
            if str(request.session['rol']) == "Administrador":
                es_admin = True
            preferencia=None
            if not request.session['idusuario'] is None:
                preferencia=PreferenciaNotificacion.objects.filter(usuario=Usuario.objects.get(pk=request.session['idusuario'])).values('n_alerta','n_correo','n_sonido','horas','minutos')
            contexto = {}
            contexto = {
                'preferencias':preferencia,
                'sesion': sesion,
                'es_admin': es_admin
            }
            return contexto


def Soy_Contacto(request):
    es_contacto=False
    if 'idusuario' and 'usuario' and 'rol' in request.session:
        if request.session['idusuario'] != "" and request.session['usuario'] != "" and request.session['rol'] !="":
            sesion = {}
            sesion = {
                'idusuario': request.session['idusuario'],
                'usuario': request.session['usuario'],
                'rol': request.session['rol']
            }
            if str(request.session['rol']) == "Contacto":
                es_contacto = True
            contexto = {}
            contexto = {
                'sesion': sesion,
                'es_contacto': es_contacto
            }
            if es_contacto:
                return True
    return False

def Soy_Admin(request):
    es_admin = False
    if 'idusuario' and 'usuario' and 'rol' in request.session:
        if request.session['idusuario'] != "" and request.session['usuario'] != "" and request.session['rol'] !="":
            sesion = {}
            sesion = {
                'idusuario': request.session['idusuario'],
                'usuario': request.session['usuario'],
                'rol': request.session['rol']
            }
            if str(request.session['rol']) == "Administrador":
                es_admin = True
            contexto = {}
            contexto = {
                'sesion': sesion,
                'es_admin': es_admin
            }
            if es_admin:
                return True
    return False


def administracion(request):
    if Soy_Admin(request):
                return render(request, 'administracion/admin_panel.html', generarContexto(request))
    return render(request, 'principal/principal.html')

def categoria_menus(request):
    if Soy_Admin(request):
        if 'idusuario' and 'usuario' and 'rol' in request.session:
            if request.session['idusuario'] != "" and request.session['usuario'] != "" and request.session['rol'] !="":
                sesion = {}
                sesion = {
                    'idusuario': request.session['idusuario'],
                    'usuario': request.session['usuario'],
                    'rol': request.session['rol']
                }
        all_usuarios = Usuario.objects.all()
        roles = Rol.objects.all()
        contactos=Contacto.objects.all()
        preferencia=None
        if not request.session['idusuario'] is None:
            preferencia=PreferenciaNotificacion.objects.filter(usuario=Usuario.objects.get(pk=request.session['idusuario'])).values('n_alerta','n_correo','n_sonido','horas','minutos')
        contexto = {
        'preferencias':preferencia,
        'Roles':roles,
        'Contactos':contactos,
        'Usuarios':all_usuarios,
        'sesion':sesion}
        return render(request, "administracion/categoria_menus.html", contexto)
    return render(request, 'principal/principal.html')


def montos(request):
    if Soy_Admin(request):
        if 'idusuario' and 'usuario' and 'rol' in request.session:
            if request.session['idusuario'] != "" and request.session['usuario'] != "" and request.session['rol'] !="":
                sesion = {}
                sesion = {
                    'idusuario': request.session['idusuario'],
                    'usuario': request.session['usuario'],
                    'rol': request.session['rol']
                }
        all_usuarios = Usuario.objects.all()
        roles = Rol.objects.all()
        contactos=Contacto.objects.all()
        preferencia=None
        if not request.session['idusuario'] is None:
            preferencia=PreferenciaNotificacion.objects.filter(usuario=Usuario.objects.get(pk=request.session['idusuario'])).values('n_alerta','n_correo','n_sonido','horas','minutos')
        contexto = {
        'preferencias':preferencia,
        'Roles':roles,
        'Contactos':contactos,
        'Usuarios':all_usuarios,
        'sesion':sesion}
        return render(request, "administracion/montos.html", contexto)
    return render(request, 'principal/principal.html')

def menus(request):
    if Soy_Admin(request):
        if 'idusuario' and 'usuario' and 'rol' in request.session:
            if request.session['idusuario'] != "" and request.session['usuario'] != "" and request.session['rol'] !="":
                sesion = {}
                sesion = {
                    'idusuario': request.session['idusuario'],
                    'usuario': request.session['usuario'],
                    'rol': request.session['rol']
                }
        all_usuarios = Usuario.objects.all()
        roles = Rol.objects.all()
        contactos=Contacto.objects.all()
        preferencia=None
        if not request.session['idusuario'] is None:
            preferencia=PreferenciaNotificacion.objects.filter(usuario=Usuario.objects.get(pk=request.session['idusuario'])).values('n_alerta','n_correo','n_sonido','horas','minutos')
        contexto = {
        'preferencias':preferencia,
        'Roles':roles,
        'Contactos':contactos,
        'Usuarios':all_usuarios,
        'sesion':sesion}
        return render(request, "administracion/menus.html", contexto)
    return render(request, 'principal/principal.html')

    
def usuarios(request):
    if Soy_Admin(request):
        if 'idusuario' and 'usuario' and 'rol' in request.session:
            if request.session['idusuario'] != "" and request.session['usuario'] != "" and request.session['rol'] !="":
                sesion = {}
                sesion = {
                    'idusuario': request.session['idusuario'],
                    'usuario': request.session['usuario'],
                    'rol': request.session['rol']
                }
        all_usuarios = Usuario.objects.all()        
        roles = Rol.objects.all()
        #Debo traer los contactos, que no estan asociados a otro usuario
        contactos=Contacto.objects.exclude(id__in=contactos_usuarios.objects.all().values('contacto__pk'))
        preferencia=None
        if not request.session['idusuario'] is None:
            preferencia=PreferenciaNotificacion.objects.filter(usuario=Usuario.objects.get(pk=request.session['idusuario'])).values('n_alerta','n_correo','n_sonido','horas','minutos')
        contexto = {
        'preferencias':preferencia,
        'Roles':roles,
        'Contactos':contactos,
        'Usuarios':all_usuarios,
        'sesion':sesion}

        return render(request, "administracion/usuarios.html", contexto)
    return render(request, 'principal/principal.html')

def sugerencias(request):
    if Soy_Admin(request):
        sesion = {}
        sesion = {
            'idusuario': request.session['idusuario'],
            'usuario': request.session['usuario'],
            'rol': request.session['rol']
        }
        sugerencias = Sugerencias.objects.all()     
        preferencia=None
        if not request.session['idusuario'] is None:
            preferencia=PreferenciaNotificacion.objects.filter(usuario=Usuario.objects.get(pk=request.session['idusuario'])).values('n_alerta','n_correo','n_sonido','horas','minutos')   
        contexto = {
        'preferencias':preferencia,
        'sesion':sesion,
        'Sugerencias': sugerencias,
        'usuario':request.session['usuario'],
        'idusuario':request.session['idusuario']
        }
        return render(request, 'administracion/sugerencias.html',contexto)
    return render(request, 'principal/principal.html')

def contactos(request):
    if Soy_Admin(request):
        contactos = Contacto.objects.all()        
        categorias = Categoria.objects.all()
        sesion = {
                    'idusuario': request.session['idusuario'],
                    'usuario': request.session['usuario'],
                    'rol': request.session['rol']
                }
        preferencia=None
        if not request.session['idusuario'] is None:
            preferencia=PreferenciaNotificacion.objects.filter(usuario=Usuario.objects.get(pk=request.session['idusuario'])).values('n_alerta','n_correo','n_sonido','horas','minutos')
        contexto = {
        'preferencias':preferencia,
        'Contactos': contactos, 
        'Categorias': categorias,
        'usuario':request.session['usuario'],
        'idusuario':request.session['idusuario'],
        'sesion':sesion}
        return render(request, 'administracion/contactos.html',contexto)
    return render(request, 'principal/principal.html')

def categorias(request):
    if Soy_Admin(request):
        categorias = Categoria.objects.all()
        return render(request, 'administracion/categoria.html', generarContexto(request))
    return render(request, 'principal/principal.html')


def registro_categoria(request):
    if Soy_Admin(request):
        if request.method == 'POST':
            categoria = request.POST['categoria']

            try:
                usuario_objeto = Usuario.objects.get(pk=request.session['idusuario'])
                Categoria.objects.create(
                    nombre=categoria,
                    usuario=usuario_objeto
                )
                return HttpResponse("SI")
            except Exception as e:
                return HttpResponse("Error1")
        else:
            return HttpResponse("Error2")
    return render(request, 'principal/principal.html')

def modifica_categoria(request):
    if Soy_Admin(request):
        if request.method == 'POST':
            idcategoria = request.POST['idcategoria']
            categoria = request.POST['categoria']
            try:
                categoria_modificar = Categoria.objects.get(pk=idcategoria)
                categoria_modificar.nombre = categoria
                categoria_modificar.save()
                return HttpResponse("SI")
            except Exception as e:
                return HttpResponse("Error1")
        else:
            return HttpResponse("Error2")
    return render(request, 'principal/principal.html')

def eliminar_categoria(request, idcategoria):
    if Soy_Admin(request):
        try:
            categoria = Categoria.objects.get(pk=idcategoria)        
            if categoria:
                contacto_cat=Contacto.objects.filter(categoria=categoria)
                if not contacto_cat:
                    categoria.delete()
                    return HttpResponse("SI")
                else:
                    return HttpResponse("TC")
            else:
                return HttpResponse("NO")
        except:
            return HttpResponse("NO")
    return render(request, 'principal/principal.html')

def activar_contacto(request, idcontacto):
    if Soy_Admin(request):
        try:
            contacto=Contacto.objects.get(pk=idcontacto)
            if contacto:
                contacto.activo=True
                contacto.save()
                return HttpResponse("SI")
            else:
                return HttpResponse("NO")
        except:
            return HttpResponse("NO")
    return render(request, 'principal/principal.html')

def deactivate_contact(request, idcontacto):
    if Soy_Admin(request):
        try:
            contacto=Contacto.objects.get(pk=idcontacto)
            if contacto:
                contacto.activo=False
                contacto.save()
                return HttpResponse("SI")
            else:
                return HttpResponse("NO")
        except:
            return HttpResponse("NO")
    return render(request, 'principal/principal.html')

def eliminar_contacto(request, idcontacto):
    if Soy_Admin(request):
        try:
            contacto = Contacto.objects.get(pk=idcontacto)
            if contacto:
                contacto.delete()
                return HttpResponse("SI")
            else:
                return HttpResponse("NO")
        except Exception as e:
            return HttpResponse("NO")
    return render(request, 'principal/principal.html')

def get_contacts(request):
    if Soy_Admin(request):
        todas_contactos = Contacto.objects.all().values('pk','desde','hasta','nombre', 'telefono', 'direccion', 'categoria__nombre', 'usuario__usuario','activo','usuario__imagen').annotate(average_rating=Avg('rating__rating')).order_by('pk')
        if todas_contactos:
            data = json.dumps(list(todas_contactos), cls=DjangoJSONEncoder)
        else:
            data = serializers.serialize('json', "")
        return HttpResponse(data, content_type="application/json")
    return render(request, 'principal/principal.html')

def obtener_contactos(request,filtro,orden,texto):
    if Soy_Admin(request):
        if orden == "ascendente":
            orden = ""
        elif orden == "descendente":
            orden = "-"

        if filtro == "categoria":
            orden=orden+"categoria__nombre"
        elif filtro =="nombre":
            orden=orden+"nombre"
        elif filtro =="rating":
            orden=orden+"average_rating"
        
        if texto == "___":
            todas_contactos = Contacto.objects.all().values('pk','desde','hasta','nombre', 'telefono', 'direccion', 'categoria__nombre', 'usuario__usuario','activo','usuario__imagen').annotate(average_rating=Avg('rating__rating')).order_by(orden)
        else:
            if filtro == "categoria":
                todas_contactos = Contacto.objects.values('pk','desde','hasta','nombre', 'telefono', 'direccion', 'categoria__nombre', 'usuario__usuario','activo','usuario__imagen').annotate(average_rating=Avg('rating__rating')).filter(categoria__nombre__contains=texto).order_by(orden)
            elif filtro == "nombre":
                todas_contactos = Contacto.objects.values('pk','desde','hasta','nombre', 'telefono', 'direccion', 'categoria__nombre', 'usuario__usuario','activo','usuario__imagen').annotate(average_rating=Avg('rating__rating')).filter(nombre__contains=texto).order_by(orden)
            elif filtro == "rating":
                todas_contactos = Contacto.objects.values('pk','desde','hasta','nombre', 'telefono','direccion', 'categoria__nombre', 'usuario__usuario','activo','usuario__imagen').annotate(average_rating=Avg('rating__rating')).filter(average_rating__contains=texto).order_by(orden)
        if todas_contactos:
            data = json.dumps(list(todas_contactos), cls=DjangoJSONEncoder)
        else:
            data = serializers.serialize('json', "")
        return HttpResponse(data, content_type="application/json")
    return render(request, 'principal/principal.html')


def get_categories(request):
    if Soy_Admin(request):        
        todas_categorias=Categoria.objects.all().values('pk', 'nombre', 'usuario__usuario','usuario__imagen').order_by('pk')
        if todas_categorias:
                data = json.dumps(list(todas_categorias), cls=DjangoJSONEncoder)
        else:
            data = serializers.serialize('json', "")
        return HttpResponse(data, content_type="application/json")
    return render(request, 'principal/principal.html')

def obtener_categorias(request,filtro,orden,texto):
    if Soy_Admin(request):
        if orden == "ascendente":
            orden = ""
        elif orden == "descendente":
            orden = "-"

        if filtro == "usuario":
            orden=orden+"usuario__usuario"
        elif filtro =="nombre":
            orden=orden+"nombre"

        if texto == "___":
            todas_categorias=Categoria.objects.all().values('pk', 'nombre', 'usuario__usuario','usuario__imagen').order_by(orden)
        else:
            if filtro == "usuario":
                todas_categorias=Categoria.objects.filter(usuario__usuario__contains=texto).values('pk', 'nombre', 'usuario__usuario').order_by(orden)
            elif filtro == "nombre":
                todas_categorias=Categoria.objects.filter(nombre__contains=texto).values('pk', 'nombre', 'usuario__usuario').order_by(orden)
        if todas_categorias:
            data = json.dumps(list(todas_categorias), cls=DjangoJSONEncoder)
        else:
            data = serializers.serialize('json', "")
        return HttpResponse(data, content_type="application/json")
    return render(request, 'principal/principal.html')

from django.utils.timezone import localtime, now
from datetime import datetime
def aceptar_sugerencia(request, idsugerencia):
    if Soy_Admin(request):
        try:
            Sugerencia_datos = Sugerencias.objects.get(pk=idsugerencia)

            nombre = Sugerencia_datos.nombre
            telefono = Sugerencia_datos.telefono
            direccion = Sugerencia_datos.direccion
            categoria = Sugerencia_datos.categoria

            usuario = request.session['idusuario']
            usuario_sesion = Usuario.objects.get(pk=usuario)

            Contacto.objects.create(
                nombre=nombre,
                telefono=telefono,
                direccion=direccion,
                categoria=categoria,
                usuario=usuario_sesion,
                desde=datetime.now(),
                hasta=datetime.now()
            )

            Sugerencia_datos.aplicada = True
            Sugerencia_datos.save()
            return HttpResponse("SI")
        except Exception as e:
            return HttpResponse(e)
    return HttpResponse("Error2")


def obtener_datos_categoria(request, idcategoria):
    if Soy_Admin(request):
        todo_categorias = Categoria.objects.filter(pk=idcategoria)
        if todo_categorias:
            data = serializers.serialize('json', todo_categorias)
        else:
            data = serializers.serialize('json', "")
        return HttpResponse(data, content_type="application/json")
    return render(request, 'principal/principal.html')


def obtener_datos_contacto(request, idcontacto):
    if Soy_Admin(request):
        todo_contactos = Contacto.objects.filter(pk=idcontacto)
        if todo_contactos:
            data = serializers.serialize('json', todo_contactos)
        else:
            data = serializers.serialize('json', "")
        return HttpResponse(data, content_type="application/json")
    return render(request, 'principal/principal.html')


def registro_contacto(request):
    if Soy_Admin(request):
        if request.method == 'POST':
            contacto = request.POST['contacto']
            telefono = request.POST['telefono']
            direccion = request.POST['direccion']
            categoria = request.POST['categoria']
            desde = request.POST['desde']
            hasta = request.POST['hasta']
            try:
                usuario_objeto = Usuario.objects.get(pk=request.session['idusuario'])
                categoria = Categoria.objects.get(pk=categoria)
                Contacto.objects.create(
                    nombre=contacto,
                    telefono=telefono,
                    direccion=direccion,
                    categoria=categoria,
                    usuario=usuario_objeto,
                    desde=desde,
                    hasta=hasta
                )
                return HttpResponse("SI")
            except Exception as e:
                return HttpResponse("Error1")
        else:
            return HttpResponse("Error2")
    return render(request, 'principal/principal.html')


def modifica_contacto(request):
    if Soy_Admin(request):
        if request.method == 'POST':
            try:
                categoria_nueva = Categoria.objects.get(pk=request.POST['categoria'])
                contacto_modificar = Contacto.objects.get(pk=request.POST['contacto2'])

                contacto_modificar.nombre = request.POST['contacto']
                contacto_modificar.telefono = request.POST['telefono']
                contacto_modificar.direccion = request.POST['direccion']
                contacto_modificar.categoria = categoria_nueva
                contacto_modificar.desde=request.POST['desde']
                contacto_modificar.hasta=request.POST['hasta']
                contacto_modificar.save()
                return HttpResponse("SI")
            except Exception as e:
                return HttpResponse("Error1")
        else:
            return HttpResponse("Error2")
    return render(request, 'principal/principal.html')
