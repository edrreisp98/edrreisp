from django.conf.urls import url
from . import views

app_name = 'principal'

urlpatterns = [
    
    # proyecto/contarContactosDisponibles
    url(r'contarContactosDisponibles', views.contarContactosDisponibles, name="contarContactosDisponibles"),

    # proyecto/obtenerNombrecontactos
    url(r'obtenerNombrecontactos', views.obtenerNombrecontactos, name="obtenerNombrecontactos"),

    # proyecto/ver_mi_perfil
    url(r'ver_mi_perfil', views.ver_mi_perfil, name="ver_mi_perfil"),
    
    # proyecto/cuadro_de_mandos
    url(r'cuadro_de_mandos', views.cuadro_de_mandos, name="cuadro_de_mandos"),
    
    # proyecto/cambiame_foto
    url(r'cambiame_foto', views.cambiame_foto, name="cambiame_foto"),

    
    # proyecto/giveme_pk_and_name_contacto
    url(r'giveme_pk_and_name_contacto/(?P<idusuario>[0-9]+)', views.giveme_pk_and_name_contacto, name="giveme_pk_and_name_contacto"),
    
    # proyecto/actualizar_monto/id
    url(r'actualizar_monto/(?P<idmonto>[0-9]+)/(?P<monto>[0-9]+)', views.actualizar_monto, name="actualizar_monto"),

    
    # proyecto/crear_notificacion_pre_alerta_cierre_contacto
    url(r'crear_notificacion_pre_alerta_cierre_contacto', views.crear_notificacion_pre_alerta_cierre_contacto, name="crear_notificacion_pre_alerta_cierre_contacto"),

    
    

    # proyecto/crear_notificacion_cierre_contacto
    url(r'crear_notificacion_cierre_contacto', views.crear_notificacion_cierre_contacto, name="crear_notificacion_cierre_contacto"),

    # proyecto/cambiame_correo
    url(r'cambiame_correo/(?P<correo>.+)', views.cambiame_correo, name="cambiame_correo"),
    
    
    # proyecto/cambiame_direccion
    url(r'cambiame_direccion/(?P<direccion>.+)', views.cambiame_direccion, name="cambiame_direccion"),
    

    # proyecto/cambiame_nombre
    url(r'cambiame_nombre/(?P<nombre>.+)', views.cambiame_nombre, name="cambiame_nombre"),
    
    
    # proyecto/notificame_este_pedido/id
    url(r'notificame_este_pedido/(?P<idnotificacion>[0-9]+)', views.notificame_este_pedido, name="notificame_este_pedido"),

    # proyecto/marcar_como_leida/id
    url(r'marcar_como_leida/(?P<idnotificacion>[0-9]+)', views.marcar_como_leida, name="marcar_como_leida"),
    
    
     # proyecto/get_my_teams/id
    url(r'get_my_teams/(?P<idusuario>[0-9]+)', views.get_my_teams, name="get_my_teams"),

    # proyecto/obtener_notificaciones_recoger_pedido
    url(r'obtener_notificaciones_recoger_pedido', views.obtener_notificaciones_recoger_pedido, name="obtener_notificaciones_recoger_pedido"),

    
    # proyecto/obtener_notificacion_users_pedido_despachado
    url(r'obtener_notificacion_users_pedido_despachado', views.obtener_notificacion_users_pedido_despachado, name="obtener_notificacion_users_pedido_despachado"),

    
    # proyecto/obtener_notificacion_users_pedido_llego
    url(r'obtener_notificacion_users_pedido_llego', views.obtener_notificacion_users_pedido_llego, name="obtener_notificacion_users_pedido_llego"),

    
    # proyecto/obtener_notificacion_users_casi_cierre_contactos
    url(r'obtener_notificacion_users_casi_cierre_contactos', views.obtener_notificacion_users_casi_cierre_contactos, name="obtener_notificacion_users_casi_cierre_contactos"),

    
    # proyecto/obtener_notificacion_admins_cierre_contactos
    url(r'obtener_notificacion_admins_cierre_contactos', views.obtener_notificacion_admins_cierre_contactos, name="obtener_notificacion_admins_cierre_contactos"),


    # proyecto/obtener_notificacion_admins_pedidos_llegados
    url(r'obtener_notificacion_admins_pedidos_llegados', views.obtener_notificacion_admins_pedidos_llegados, name="obtener_notificacion_admins_pedidos_llegados"),

    # proyecto/obtener_notificacion_admins_pedidos_nuevos
    url(r'obtener_notificacion_admins_pedidos_nuevos', views.obtener_notificacion_admins_pedidos_nuevos, name="obtener_notificacion_admins_pedidos_nuevos"),

    # proyecto/obtener_alertas_pedido
    url(r'obtener_alertas_pedido', views.obtener_alertas_pedido, name="obtener_alertas_pedido"),

    # proyecto/obtener_sugerencias_notificacion
    url(r'obtener_sugerencias_notificacion', views.obtener_sugerencias_notificacion, name="obtener_sugerencias_notificacion"),

    # proyecto/registro_sugerencia
    url(r'registro_sugerencia', views.registro_sugerencia, name="registro_sugerencia"),

    # proyecto/todas_notificaciones
    url(r'todas_notificaciones', views.todas_notificaciones, name="todas_notificaciones"),

    # proyecto/modifica_equipos
    url(r'modifica_equipos', views.modifica_equipos, name="modifica_equipos"),

    
    # proyecto/cual_es_mi_foto
    url(r'cual_es_mi_foto', views.cual_es_mi_foto, name="cual_es_mi_foto"),
    
    # proyecto/total_mis_sugerencias
    url(r'total_mis_sugerencias', views.total_mis_sugerencias, name="total_mis_sugerencias"),

    # proyecto/total_mis_notificaciones
    url(r'total_mis_notificaciones', views.total_mis_notificaciones, name="total_mis_notificaciones"),

    # proyecto/miconfiguracion
    url(r'miconfiguracion', views.miconfiguracion, name="miconfiguracion"),
    
    
    # proyecto/guardar_configuracion
    url(r'guardar_configuracion', views.guardar_configuracion, name="guardar_configuracion"),

    # proyecto/cambio_clave
    url(r'cambio_clave', views.cambio_clave, name="cambio_clave"),

    # proyecto/reporte_grafico
    url(r'reporte_grafico', views.reporte_grafico, name="reporte_grafico"),

    
    # proyecto/oye_ponlo_aqui
    url(r'oye_ponlo_aqui/(?P<idusuario>.+)/(?P<idequipo>.+)', views.oye_ponlo_aqui, name="oye_ponlo_aqui"),

    # proyecto/agrega_usuarios_equipo
    url(r'agrega_usuarios_equipo', views.agrega_usuarios_equipo, name="agrega_usuarios_equipo"),

    # proyecto/registro_equipos
    url(r'registro_equipos', views.registro_equipos, name="registro_equipos"),

    # proyecto/get_debts
    url(r'get_debts', views.get_debts, name="get_debts"),


    # proyecto/obtener_montos
    url(r'obtener_montos/(?P<filtro>.+)/(?P<orden>.+)/(?P<texto>.+)', views.obtener_montos, name="obtener_montos"),

    
    # proyecto/equipos_del_modal
    url(r'equipos_del_modal', views.equipos_del_modal, name="equipos_del_modal"),

    # proyecto/obtener_sugerencias
    url(r'get_suggestions', views.get_suggestions, name="get_suggestions"),


    # proyecto/obtener_sugerencias
    url(r'obtener_sugerencias/(?P<filtro>.+)/(?P<orden>.+)/(?P<texto>.+)', views.obtener_sugerencias, name="obtener_sugerencias"),

    # proyecto/get_suggestions_personals
    url(r'get_suggestions_personals', views.get_suggestions_personals, name="get_suggestions_personals"),

    # proyecto/get_suggestions_directory
    url(r'get_suggestions_directory', views.get_suggestions_directory, name="get_suggestions_directory"),


    # proyecto/obtener_sugerencias_directorio
    url(r'obtener_sugerencias_directorio/(?P<filtro>.+)/(?P<orden>.+)/(?P<texto>.+)', views.obtener_sugerencias_directorio, name="obtener_sugerencias_directorio"),

    # proyecto/get_directory_list
    url(r'get_directory_list', views.get_directory_list, name="get_directory_list"),


    # proyecto/obtener_contactos_directorio/filtro/orden/texto
    url(r'obtener_contactos_directorio/(?P<filtro>.+)/(?P<orden>.+)/(?P<texto>.+)', views.obtener_contactos_directorio, name="obtener_contactos_directorio"),

        # proyecto.com/equipos
    url(r'^equipos', views.equipos, name="equipos"),

    # proyecto.com/menus
    url(r'^menus', views.menus, name="menus"),
    
     # proyecto.com/mispedidos
    url(r'^mispedidos', views.mispedidos, name="mispedidos"),

    # proyecto.com/
    url(r'^get_my_orders', views.get_my_orders, name="get_my_orders"),


    # proyecto.com/Obtenermispedidos/contacto/ascendente/___
    url(r'^Obtenermispedidos/(?P<filtro>.+)/(?P<orden>.+)/(?P<texto>.+)', views.Obtenermispedidos, name="Obtenermispedidos"),

    # proyecto.com/pedidos
    url(r'^pedidos', views.pedidos, name="pedidos"),

    
    # proyecto.com/cargar_contactos_libres
    url(r'^cargar_contactos_libres', views.cargar_contactos_libres, name="cargar_contactos_libres"),


    # proyecto.com/obtener_usuariosDeveloper_paraEquipos
    url(r'^obtener_usuariosDeveloper_paraEquipos', views.obtener_usuariosDeveloper_paraEquipos, name="obtener_usuariosDeveloper_paraEquipos"),

    
    # proyecto.com/get_teams_ff
    url(r'^get_teams_ff', views.get_teams_ff, name="get_teams_ff"),


    # proyecto.com/obtener_equipos_ff
    url(r'^obtener_equipos_ff/(?P<filtro>.+)/(?P<orden>.+)/(?P<texto>.+)', views.obtener_equipos_ff, name="obtener_equipos_ff"),


    # proyecto.com/crearRate
    url(r'^crearRate', views.crearRate, name="crearRate"),

    # proyecto.com/registro_pedido
    url(r'^registro_pedido', views.registro_pedido, name="registro_pedido"),

    # proyecto.com/get_orders
    url(r'^get_orders/(?P<id_equipo>.+)', views.get_orders, name="get_orders"),


    # proyecto.com/obtener_pedidos/idusuario_solicitante
    url(r'^obtener_pedidos/(?P<filtro>.+)/(?P<orden>.+)/(?P<texto>.+)', views.obtener_pedidos, name="obtener_pedidos"),

    # proyecto.com/editar_comentario
    url(r'^editar_comentario', views.editar_comentario, name="editar_comentario"),

    # proyecto.com/registrar_comentario
    url(r'^registrar_comentario', views.registrar_comentario, name="registrar_comentario"),

    # proyecto.com/obtener_comentarios_contacto/idcontacto
    url(r'^obtener_comentarios_contacto/(?P<idcontacto>[0-9]+)', views.obtener_comentarios_contacto, name="obtener_comentarios_contacto"),

    
    # proyecto.com/obtener_ratings_contacto_personal/idcontacto
    url(r'^obtener_ratings_contacto_personal/(?P<idcontacto>[0-9]+)', views.obtener_ratings_contacto_personal, name="obtener_ratings_contacto_personal"),

    # proyecto.com/obtener_ratings_contacto/idcontacto
    url(r'^obtener_ratings_contacto/(?P<idcontacto>[0-9]+)', views.obtener_ratings_contacto, name="obtener_ratings_contacto"),

    
    # proyecto.com/despachar_pedido/idpedido
    url(r'^despachar_pedido/(?P<idpedido>[0-9]+)', views.despachar_pedido, name="despachar_pedido"),


    # proyecto.com/duplicar_pedido/idpedido
    url(r'^duplicar_pedido/(?P<idpedido>[0-9]+)', views.duplicar_pedido, name="duplicar_pedido"),

    # proyecto.com/cancelar_pedido/idpedido
    url(r'^cancelar_pedido/(?P<idpedido>[0-9]+)', views.cancelar_pedido, name="cancelar_pedido"),

    # proyecto.com/recibir_pedido/idpedido
    url(r'^recibir_pedido/(?P<idpedido>[0-9]+)', views.recibir_pedido, name="recibir_pedido"),


    # proyecto.com/recibir_pedidos_automaticos
    url(r'^recibir_pedidos_automaticos', views.recibir_pedidos_automaticos, name="recibir_pedidos_automaticos"),

    # proyecto.com/contacto
    url(r'^contacto$', views.contacto, name="contacto"),

    # proyecto.com/sugerencia
    url(r'^sugerencia', views.sugerencia, name="sugerencia"),

    # proyecto.com/administrar_sugerencia
    url(r'^admin_sugerencia', views.admin_sugerencia, name="admin_sugerencia"),

    # proyecto.com/
    url(r'^$', views.index, name="index"),

    # proyecto.com/registro
    url(r'registro$', views.registro, name="registro"),

    # proyecto.com/login
    url(r'login$', views.login, name="login"),

    # proyecto.com/activar/id
    url(r'activar/(?P<codigo>.+)', views.activar, name="activar"),

    # proyecto.com/comprobarUsuario/usuario
    url(r'comprobarUsuario/(?P<usuario>.+)', views.comprobarUsuario, name="comprobarUsuario"),

    # proyecto.com/principal
    url(r'principal', views.principal, name="principal"),

    
    # proyecto.com/resetear_monto
    url(r'resetear_monto/(?P<idmonto>[0-9]+)', views.resetear_monto, name="resetear_monto"),


    # proyecto.com/eliminar_comentario
    url(r'eliminar_comentario/(?P<idcomentario>[0-9]+)', views.eliminar_comentario, name="eliminar_comentario"),


    # proyecto.com/eliminar_equipo
    url(r'eliminar_equipo/(?P<idequipo>[0-9]+)', views.eliminar_equipo, name="eliminar_equipo"),

    
     # proyecto.com/dameDetallesdePedido/idpedido
    url(r'dameDetallesdePedido/(?P<idpedido>[0-9]+)', views.dameDetallesdePedido, name="dameDetallesdePedido"),
    
    # proyecto.com/ver_items/idmenu
    url(r'ver_items/(?P<idmenu>[0-9]+)', views.ver_items, name="ver_items"),

    # proyecto.com/editar_rate/idrate
    url(r'editar_rate/(?P<idrate>[0-9]+)', views.editar_rate, name="editar_rate"),

    # proyecto.com/obtener_datos_comentario
    url(r'obtener_datos_comentario/(?P<idcomentario>[0-9]+)', views.obtener_datos_comentario, name="obtener_datos_comentario"),

    # proyecto.com/obtener_datos_equipo
    url(r'obtener_datos_equipo/(?P<idequipo>[0-9]+)', views.obtener_datos_equipo, name="obtener_datos_equipo"),

    # proyecto.com/cerrar_sesion
    url(r'cerrar_sesion', views.cerrar_sesion, name="cerrar_sesion"),

    #proyecto/get_items_menu
    url(r'get_items_menu/(?P<idmenu>.+)', views.get_items_menu, name="get_items_menu"),

    #proyecto/obtener_items_menu
    url(r'obtener_items_menu/(?P<filtro>.+)/(?P<orden>.+)/(?P<texto>.+)', views.obtener_items_menu, name="obtener_items_menu"),

    #royecto/get_categories_menu
    url(r'get_categories_menu', views.get_categories_menu, name="get_categories_menu"),

    
    #proyecto/obtener_categoria_menus
    url(r'obtener_categoria_menus/(?P<filtro>.+)/(?P<orden>.+)/(?P<texto>.+)', views.obtener_categoria_menus, name="obtener_categoria_menus"),

    
    #proyecto/dameLosItemsMenu
    url(r'dameLosItemsMenu/(?P<idmenu>[0-9]+)', views.dameLosItemsMenu, name="dameLosItemsMenu"),
    
    #proyecto/dameLosMenusContacto
    url(r'dameLosMenusContacto/(?P<idcontacto>[0-9]+)', views.dameLosMenusContacto, name="dameLosMenusContacto"),

    #proyecto/get_menus
    url(r'get_menus', views.get_menus, name="get_menus"),


    #proyecto/obtener_menus
    url(r'obtener_menus/(?P<filtro>.+)/(?P<orden>.+)/(?P<texto>.+)', views.obtener_menus, name="obtener_menus"),

    #proyecto/get_users
    url(r'get_users', views.get_users, name="get_users"),


    #proyecto/obtener_usuarios
    url(r'obtener_users/(?P<filtro>.+)/(?P<orden>.+)/(?P<texto>.+)', views.obtener_usuarios, name="obtener_usuarios"),


    #proyecto/activar_usuario
    url(r'activar_usuario/(?P<idusuario>[0-9]+)', views.activar_usuario, name="activar_usuario"),

    
    #proyecto/eliminar_item_menu
    url(r'eliminar_item_menu/(?P<iditem>[0-9]+)', views.eliminar_item_menu, name="eliminar_item_menu"),
    
    #proyecto/eliminar_categoria_menu
    url(r'eliminar_categoria_menu/(?P<idcategoria>[0-9]+)', views.eliminar_categoria_menu, name="eliminar_categoria_menu"),
        
    #proyecto/eliminar_menu
    url(r'eliminar_menu/(?P<idmenu>[0-9]+)', views.eliminar_menu, name="eliminar_menu"),

    #proyecto/eliminar_usuario
    url(r'eliminar_usuario/(?P<idusuario>[0-9]+)', views.eliminar_usuarios, name="eliminar_usuarios"),


    # proyecto/sacar_del_equipo
    url(r'sacar_del_equipo/(?P<idusuario>[0-9]+)/(?P<idequipo>[0-9]+)', views.sacar_del_equipo, name="sacar_del_equipo"),

    
    # proyecto.com/registro_item_menu
    url(r'registro_item_menu', views.registro_item_menu, name="registro_item_menu"),
    
        
    # proyecto.com/registro_categoria_menu
    url(r'registro_categoria_menu', views.registro_categoria_menu, name="registro_categoria_menu"),
    
    # proyecto.com/registro_menu
    url(r'registro_menu', views.registro_menu, name="registro_menu"),

    

    # proyecto.com/registro_usuario
    url(r'registro_usuario', views.registro_usuario, name="registro_usuario"),

    
     # proyecto.com/modifica_item_menu
    url(r'modifica_item_menu', views.modifica_item_menu, name="modifica_item_menu"),


    # proyecto.com/modifica_categoria_menu
    url(r'modifica_categoria_menu', views.modifica_categoria_menu, name="modifica_categoria_menu"),

    # proyecto.com/modifica_menu
    url(r'modifica_menu', views.modifica_menu, name="modifica_menu"),

    # proyecto.com/modifica_usuario
    url(r'modifica_usuario', views.modifica_usuario, name="modifica_usuario"),

    
    

    #proyecto/obtener_usuario_Equipo
    url(r'obtener_usuario_Equipo/(?P<idequipo>[0-9]+)', views.obtener_usuario_Equipo, name="obtener_usuario_Equipo"),

    
    #proyecto/obtener_datos_item_menu
    url(r'obtener_datos_item_menu/(?P<iditem>[0-9]+)', views.obtener_datos_item_menu, name="obtener_datos_item_menu"),

    
    #proyecto/obtener_datos_categoria_menu
    url(r'obtener_datos_categoria_menu/(?P<idcategoria>[0-9]+)', views.obtener_datos_categoria_menu, name="obtener_datos_categoria_menu"),

    
    #proyecto/obtener_datos_menu
    url(r'obtener_datos_menu/(?P<idmenu>[0-9]+)', views.obtener_datos_menu, name="obtener_datos_menu"),

    
    #proyecto/obtener_items
    url(r'obtener_items/(?P<idmenu>[0-9]+)', views.obtener_items, name="obtener_items"),


    #proyecto/obtener_datos_usuario
    url(r'obtener_datos_usuario/(?P<idusuario>[0-9]+)', views.obtener_datos_usuario, name="obtener_datos_usuario"),

    #proyecto/eliminar_sugerencia
    url(r'eliminar_sugerencia/(?P<idsugerencia>[0-9]+)', views.eliminar_sugerencia, name="eliminar_sugerencia"),


    #proyecto/ver_sugerencia
    url(r'ver_sugerencia/(?P<idsugerencia>[0-9]+)', views.ver_sugerencia, name="ver_sugerencia"),


    #proyecto/vista_detalle
    url(r'vista_detalle/(?P<idsugerencia>[0-9]+)', views.vista_detalle, name="vista_detalle"),

    #proyecto/detalle_equipo/idequipo
    url(r'detalle_equipo/(?P<idequipo>[0-9]+)', views.detalle_equipo, name="detalle_equipo"),

    #proyecto/ver_equipo/idequipo
    url(r'ver_equipo/(?P<idequipo>[0-9]+)', views.ver_equipo, name="ver_equipo"),

    #proyecto/ver_contacto/idcontacto
    url(r'ver_contacto/(?P<idcontacto>[0-9]+)', views.ver_contacto, name="ver_contacto"),

    
    #proyecto/notificar_recoger_Pedido/idpedido
    url(r'notificar_recoger_Pedido/(?P<idpedido>[0-9]+)', views.notificar_recoger_Pedido, name="notificar_recoger_Pedido"),

    
    #proyecto/notificarPedido/idpedido
    url(r'notificarPedido/(?P<idpedido>[0-9]+)', views.notificarPedido, name="notificarPedido"),

    #proyecto/cambiar_clave
    url(r'cambiar_clave', views.cambiar_clave, name="cambiar_clave"),

    #proyecto/reporte_mis_pedidos
    url(r'reporte_mis_pedidos', views.reporte_mis_pedidos, name="reporte_mis_pedidos"),

    #proyecto/reporte_total_pedidos
    url(r'reporte_total_pedidos', views.reporte_total_pedidos, name="reporte_total_pedidos"),

    
    #proyecto/todos_los_montos
    url(r'todos_los_montos', views.todos_los_montos, name="todos_los_montos"),
    
    #proyecto/reporte_pedido/idpedido
    url(r'reporte_pedido/(?P<idpedido>[0-9]+)', views.reporte_pedido, name="reporte_pedido"),

    
    #proyecto/reporte_pedido_contacto/idcontacto
    url(r'reporte_pedido_contacto/(?P<idcontacto>[0-9]+)', views.reporte_pedido_contacto, name="reporte_pedido_contacto"),

    #proyecto/reporte_monto_usuario/idusuario
    url(r'reporte_monto_usuario/(?P<idusuario>[0-9]+)', views.reporte_monto_usuario, name="reporte_monto_usuario"),

]
