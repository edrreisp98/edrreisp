from django.db import models
# Create your models here.

class Rol(models.Model):
    rol = models.CharField(max_length=50, blank=False)

    def __str__(self):
        return self.rol


class Equipo(models.Model):
    nombre = models.CharField(max_length=100, blank=False)
    usuario_creacion = models.CharField(max_length=100, blank=False)
    id_usuario = models.IntegerField(blank=True)

    def __str__(self):
        return self.nombre

#REVISAR, creo que hay que agregar una tabla PIVOT porque un usuario puede crear varios equipos y puede pertenecer a varios
class Usuario(models.Model):
    nombre=models.CharField(max_length=100,blank=False)
    usuario = models.CharField(max_length=100, blank=False, unique=True)
    clave = models.CharField(max_length=250, blank=False)
    correo = models.EmailField(max_length=200, blank=False)
    rol = models.ForeignKey(Rol, on_delete=models.CASCADE)
    activo = models.BooleanField(default=False)
    codigo_generado = models.CharField(max_length=200, blank=False)
    imagen = models.FileField(null=True,blank=True,max_length=500)
    direccion=models.CharField(max_length=400,blank=True)#ESTO HAY QUE QUITARLO

    def __str__(self):
        return '{0}'.format(self.usuario)

class equipos_usuarios(models.Model):
    equipo=models.ForeignKey(Equipo,on_delete=models.CASCADE)
    usuario=models.ForeignKey(Usuario,on_delete=models.CASCADE)

    def __str__(self):
        return '{0}'.format(self.usuario)

class PreferenciaNotificacion(models.Model):
    usuario=models.ForeignKey(Usuario,on_delete=models.CASCADE,blank=False,null=False)
    horas=models.IntegerField(blank=False,default=1)
    minutos=models.IntegerField(blank=False,default=0)
    n_correo=models.BooleanField(default=True,blank=False)
    n_alerta=models.BooleanField(default=True,blank=False)
    n_sonido=models.BooleanField(default=False,blank=False)


