from django.contrib import admin
from .models import Usuario, Rol, Equipo, PreferenciaNotificacion,equipos_usuarios
# Register your models here.

admin.site.register(Usuario)
admin.site.register(Rol)
admin.site.register(Equipo)
admin.site.register(PreferenciaNotificacion)
admin.site.register(equipos_usuarios)