#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.shortcuts import render, redirect
from .models import Rol, Usuario, Equipo, PreferenciaNotificacion, equipos_usuarios
from contacto.models import Contacto, Sugerencias, Categoria, Rating, Comentarios, Menu, CatMenu, ItemsMenu, contactos_usuarios
from django.http import HttpResponse
from django.conf import settings
import datetime
from django.core.mail import send_mail
from django.template import RequestContext
from django.views.decorators.csrf import ensure_csrf_cookie
from django.core import serializers
from django.core.serializers.json import DjangoJSONEncoder
from pedido.models import Pedido, DetallePedido, Notificacion, Monto, ConteoPorContactos
from django.db.models import Q
import json
from django.db.models import Sum
from django.db.models import Avg
from django.core.mail import EmailMultiAlternatives
from django.core.exceptions import ObjectDoesNotExist                        

def Estoy_Log(request):
    if 'idusuario' and 'usuario' and 'rol' in request.session:
        if request.session['idusuario'] != "" and request.session['usuario'] != "" and request.session['rol'] != "":
            return True
    return False

def Soy_Contacto(request):
    es_contacto=False
    if 'idusuario' and 'usuario' and 'rol' in request.session:
        if request.session['idusuario'] != "" and request.session['usuario'] != "" and request.session['rol'] !="":
            sesion = {}
            sesion = {
                'idusuario': request.session['idusuario'],
                'usuario': request.session['usuario'],
                'rol': request.session['rol']
            }
            if str(request.session['rol']) == "Contacto":
                es_contacto = True
            contexto = {}
            contexto = {
                'sesion': sesion,
                'es_contacto': es_contacto
            }
            if es_contacto:
                return True
    return False

def Soy_Admin(request):
    es_admin = False
    if 'idusuario' and 'usuario' and 'rol' in request.session:
        if request.session['idusuario'] != "" and request.session['usuario'] != "" and request.session['rol'] !="":
            sesion = {}
            sesion = {
                'idusuario': request.session['idusuario'],
                'usuario': request.session['usuario'],
                'rol': request.session['rol']
            }
            if str(request.session['rol']) == "Administrador":
                es_admin = True
            contexto = {}
            contexto = {
                'sesion': sesion,
                'es_admin': es_admin
            }
            if es_admin:
                return True
    return False

import pdfkit

def principal(request):
    es_admin = False
    es_contacto=False
    if 'idusuario' and 'usuario' and 'rol' in request.session:
        if request.session['idusuario'] != "" and request.session['usuario'] != "" and request.session['rol'] != "":
            sesion = {}
            sesion = {
                'usuario': request.session['usuario'],
                'rol': request.session['rol']
            }
            contexto = {}
            if str(request.session['rol']) == "Administrador":
                es_admin = True
            elif str(request.session['rol']) == "Contacto":
                es_contacto=True
            preferencia=None
            if not request.session['idusuario'] is None:
                preferencia=PreferenciaNotificacion.objects.filter(usuario=Usuario.objects.get(pk=request.session['idusuario'])).values('n_alerta','n_correo','n_sonido','horas','minutos')
            contexto = {
                'preferencias':preferencia,            
                'sesion': sesion,
                'menu': 'SI',
                'es_admin': es_admin,
                'es_contacto':es_contacto
            }
            return render(request, "principal/principal.html", contexto)
    return render(request, "principal/principal.html", {'menu': 'SI'})


def index(request):
    credenciales = {}
    if 'idusuario' and 'usuario' and 'rol' and 'cerrar_sesion' in request.session:
        if request.session['cerrar_sesion']=="NO" and request.session['idusuario'] != "" and request.session['usuario'] != "" and request.session['rol'] != "":
            sesion = {}
            sesion = {
                'usuario': request.session['usuario'],
                'rol': request.session['rol']
            }
            contexto = {}
            es_admin=False
            if str(request.session['rol']) == "Administrador":
                es_admin = True
            else:
                es_admin=False
            preferencia=None
            if not request.session['idusuario'] is None:
                preferencia=PreferenciaNotificacion.objects.filter(usuario=Usuario.objects.get(pk=request.session['idusuario'])).values('n_alerta','n_correo','n_sonido','horas','minutos')
            contexto = {
                'preferencias':preferencia,         
                'sesion': sesion,
                'menu': 'SI',
                'es_admin': es_admin
            }

            return render(request, "principal/principal.html", contexto)
    return render(request, 'principal/index.html', {'credenciales': credenciales})


def sacar_del_equipo(request, idusuario,idequipo):
    if Estoy_Log(request):
        try:
            usuario = Usuario.objects.get(pk=idusuario)
            equipo_anterior=Equipo.objects.get(pk=idequipo)

            #NUEVA MANERA

            relacion_equipos=equipos_usuarios.objects.get(usuario=usuario,equipo=equipo_anterior)

            if relacion_equipos:
                relacion_equipos.delete()
                sin_equipo = Equipo.objects.get(pk=1)
                equipos_usuarios.objects.create(
                    usuario=usuario,
                    equipo=sin_equipo
                )
                return HttpResponse("SI")
            else:
                return HttpResponse("NO")

        except Exception as e:
            return HttpResponse(e)
    return HttpResponse("NO")


def activar_usuario(request, idusuario):
    if Estoy_Log(request):
        try:
            usuario = Usuario.objects.get(pk=idusuario)
            if usuario:
                usuario.activo = True
                usuario.save()
                return HttpResponse("SI")
            else:
                return HttpResponse("NO")
        except:
            return HttpResponse("NO")
    return HttpResponse("NO")


def eliminar_item_menu(request,iditem):
    try:
        item = ItemsMenu.objects.get(pk=iditem)
        if item:
            item.delete()
            return HttpResponse("SI")
        else:
            return HttpResponse("NO")
    except Exception as e:
        return HttpResponse("NO")


def eliminar_categoria_menu(request,idcategoria):
    try:
        categoria = CatMenu.objects.get(pk=idcategoria)
        if categoria:
            categoria.delete()
            return HttpResponse("SI")
        else:
            return HttpResponse("NO")
    except Exception as e:
        return HttpResponse("NO")


def dameDetallesdePedido(request,idpedido):
    if Estoy_Log(request):
        try:
            pedido=Pedido.objects.get(pk=idpedido)
            detalles=DetallePedido.objects.filter(pedido=pedido).values('item__nombre','cantidad','precio','importe','pedido__itbis')
            if detalles:
                data = json.dumps(list(detalles), cls=DjangoJSONEncoder)
            else:
                data = serializers.serialize('json', "")
            return HttpResponse(data, content_type="application/json")
        except Exception as e:
            data = serializers.serialize('json', "")
    return render(request, 'principal/principal.html')


def ver_items(request,idmenu):
    if Estoy_Log(request):
        sesion = {}
        sesion = {
            'idusuario': request.session['idusuario'],
            'usuario': request.session['usuario'],
            'rol': request.session['rol']
        }
        try:
            menu=Menu.objects.get(pk=idmenu)
            items = ItemsMenu.objects.filter(menu=menu)
            preferencia=None
            if not request.session['idusuario'] is None:
                preferencia=PreferenciaNotificacion.objects.filter(usuario=Usuario.objects.get(pk=request.session['idusuario'])).values('n_alerta','n_correo','n_sonido','horas','minutos')
            contexto = {
                'preferencias':preferencia,
                'items':items,
                'sesion': sesion,
                'menu': 'SI',
                'es_admin': Soy_Admin(request)
            }
            return render(request, 'contacto/detalle_menu.html', contexto)
        except Exception as e:
            print(e)        
    else:
        return render(request, 'principal/principal.html')


def eliminar_menu(request,idmenu):
    try:
        menu = Menu.objects.get(pk=idmenu)
        if menu:
            menu.delete()
            return HttpResponse("SI")
        else:
            return HttpResponse("NO")
    except Exception as e:
        return HttpResponse("NO")


def eliminar_usuarios(request, idusuario):
    try:
        usuario = Usuario.objects.get(pk=idusuario)
        if usuario:            
            try:
                #primero elimino todos los PIVOT de equipo que tiene este usuario
                #Es decir, lo saco de todos los equipos
                relaciones_equipos=equipos_usuarios.objects.filter(usuario=usuario)
                for relacion_equipo in relaciones_equipos:
                    relacion_equipo.delete()

                #luego compruebo si esta asociado a un contacto, si es asi, elimino la relacion
                usuario.delete()
                return HttpResponse("SI")
            except Exception as e:
                return HttpResponse(e)
        else:
            return HttpResponse("NO")
    except:
        return HttpResponse("NO")

def comprobarUsuario(request, usuario):
    try:
        usuario = Usuario.objects.get(usuario=usuario,activo=True)
        if usuario:
            return HttpResponse('SI')
        else:
            return HttpResponse('NO')
    except Exception as e:
        return HttpResponse('NO')


def cerrar_sesion(request):
    request.session.delete()
    return redirect('principal/index.html', {'credenciales': ''})


def vista_detalle(request, idsugerencia):
    if Soy_Admin(request):
        sesion={}
        sesion = {
            'idusuario': request.session['idusuario'],
            'usuario': request.session['usuario'],
            'rol': request.session['rol']
        }
        preferencia=None
        if not request.session['idusuario'] is None:
            preferencia=PreferenciaNotificacion.objects.filter(usuario=Usuario.objects.get(pk=request.session['idusuario'])).values('n_alerta','n_correo','n_sonido','horas','minutos')
        contexto = {
            'preferencias':preferencia,
            'sesion': sesion,
            'id': idsugerencia,
            'usuario':request.session['usuario'],
            'idusuario':request.session['idusuario']
        }
        return render(request, "administracion/detalle_sugerencia.html", contexto)
    else:
        return render(request, "principal/principal.html")


def ver_sugerencia(request, idsugerencia):
    if Soy_Admin(request):
        sugerencia = Sugerencias.objects.filter(pk=idsugerencia).values('pk', 'nombre', 'telefono', 'direccion', 'categoria__nombre',
                                                        'usuario__usuario','usuario__imagen','aplicada','fecha')
        if sugerencia:
            data = json.dumps(list(sugerencia), cls=DjangoJSONEncoder)
        else:
            data = serializers.serialize('json', "")
        return HttpResponse(data, content_type="application/json")
    return HttpResponse("No permitido")


def eliminar_sugerencia(request, idsugerencia):
    if Estoy_Log(request):
        try:
            sugerencia = Sugerencias.objects.get(pk=idsugerencia)
            if sugerencia:
                sugerencia.delete()
                return HttpResponse("SI")
            else:
                return HttpResponse("NO")
        except:
            return HttpResponse("NO")
    else:
        return HttpResponse("No hay sesion")

def cambiar_clave(request):
    if Estoy_Log(request):
        sesion = {}
        sesion = {
            'idusuario': request.session['idusuario'],
            'usuario': request.session['usuario'],
            'rol': request.session['rol']
        }
        preferencia=None
        if not request.session['idusuario'] is None:
            preferencia=PreferenciaNotificacion.objects.filter(usuario=Usuario.objects.get(pk=request.session['idusuario'])).values('n_alerta','n_correo','n_sonido','horas','minutos')
        contexto = {
            'preferencias':preferencia,        
            'clave':Usuario.objects.filter(pk=request.session['idusuario']).values('clave'),
            'sesion': sesion,
            'menu': 'SI',
            'es_admin': Soy_Admin(request),
            'es_contacto':Soy_Contacto(request)
        }
        return render(request, 'principal/cambiar_clave.html', contexto)
    else:
        return render(request, 'principal/principal.html')

def cuadro_de_mandos(request):
    if Estoy_Log(request):
        sesion = {}
        sesion = {
            'idusuario': request.session['idusuario'],
            'usuario': request.session['usuario'],
            'rol': request.session['rol']
        }
        preferencia=None
        if not request.session['idusuario'] is None:
            preferencia=PreferenciaNotificacion.objects.filter(usuario=Usuario.objects.get(pk=request.session['idusuario'])).values('n_alerta','n_correo','n_sonido','horas','minutos')
        contexto = {
            'comentarios10':Comentarios.objects.all().values('comentario','fecha','usuario__nombre','usuario__imagen','contacto__nombre').order_by('-pk')[:10],
            'top5contactos':ConteoPorContactos.objects.all().values('cantidad','contacto__nombre').order_by('cantidad')[:5],
            'ultimos3contactos':Contacto.objects.all().values('nombre').order_by('-pk')[:3],
            'preferencias':preferencia, 
            'usuario':Usuario.objects.filter(usuario=Usuario.objects.get(pk=request.session['idusuario'])).values('pk','usuario','nombre','correo','rol__rol','activo','imagen'),    
            'equipos':equipos_usuarios.objects.filter(usuario=Usuario.objects.get(pk=request.session['idusuario'])).values('equipo__nombre','equipo__usuario_creacion','usuario__usuario'),
            'sesion': sesion,
            'menu': 'SI',
            'es_admin': Soy_Admin(request),
            'es_contacto':Soy_Contacto(request)
        }
        return render(request, 'principal/cuadro_de_mandos.html', contexto)
    else:
        return render(request, 'principal/principal.html')

def ver_mi_perfil(request):
    if Estoy_Log(request):
        sesion = {}
        sesion = {
            'idusuario': request.session['idusuario'],
            'usuario': request.session['usuario'],
            'rol': request.session['rol']
        }
        preferencia=None
        if not request.session['idusuario'] is None:
            preferencia=PreferenciaNotificacion.objects.filter(usuario=Usuario.objects.get(pk=request.session['idusuario'])).values('n_alerta','n_correo','n_sonido','horas','minutos')
        contexto = {
            'preferencias':preferencia, 
            'usuario':Usuario.objects.filter(usuario=Usuario.objects.get(pk=request.session['idusuario'])).values('pk','usuario','nombre','correo','rol__rol','activo','imagen','direccion'),    
            'equipos':equipos_usuarios.objects.filter(usuario=Usuario.objects.get(pk=request.session['idusuario'])).values('equipo__nombre','equipo__usuario_creacion','usuario__usuario'),
            'sesion': sesion,
            'menu': 'SI',
            'es_admin': Soy_Admin(request),
            'es_contacto':Soy_Contacto(request)
        }
        return render(request, 'principal/mi_perfil.html', contexto)
    else:
        return render(request, 'principal/principal.html')


def todas_notificaciones(request):
    if Estoy_Log(request):
        sesion = {}
        sesion = {
            'idusuario': request.session['idusuario'],
            'usuario': request.session['usuario'],
            'rol': request.session['rol']
        }
        preferencia=None
        if not request.session['idusuario'] is None:
            preferencia=PreferenciaNotificacion.objects.filter(usuario=Usuario.objects.get(pk=request.session['idusuario'])).values('n_alerta','n_correo','n_sonido','horas','minutos')
        contexto = {
            'preferencias':preferencia,         
            'sesion': sesion,
            'menu': 'SI',
            'es_admin': Soy_Admin(request)
        }
        return render(request, 'principal/todas_notificaciones.html', contexto)
    else:
        return render(request, 'principal/principal.html')

def reporte_grafico(request):
    if Estoy_Log(request):
        sesion = {}
        sesion = {
            'idusuario': request.session['idusuario'],
            'usuario': request.session['usuario'],
            'rol': request.session['rol']
        }
        preferencia=None
        if not request.session['idusuario'] is None:
            preferencia=PreferenciaNotificacion.objects.filter(usuario=Usuario.objects.get(pk=request.session['idusuario'])).values('n_alerta','n_correo','n_sonido','horas','minutos')
        contexto = {
            'preferencias':preferencia,         
            'sesion': sesion,
            'menu': 'SI',
            'es_admin': Soy_Admin(request)
        }
        return render(request, 'principal/graficos/line_basic.html', contexto)
    else:
        return render(request, 'principal/principal.html')

def miconfiguracion(request):
    if Estoy_Log(request):
        sesion = {}
        sesion = {
            'idusuario': request.session['idusuario'],
            'usuario': request.session['usuario'],
            'rol': request.session['rol']
        }
        preferencia=None
        if not request.session['idusuario'] is None:
            preferencia=PreferenciaNotificacion.objects.filter(usuario=Usuario.objects.get(pk=request.session['idusuario'])).values('n_alerta','n_correo','n_sonido','horas','minutos')
        contexto = {
            'preferencias':preferencia,         
            'clave':Usuario.objects.filter(pk=request.session['idusuario']).values('clave'),
            'sesion': sesion,
            'menu': 'SI',
            'es_admin': Soy_Admin(request)
        }
        return render(request, 'principal/configuracion.html', contexto)
    else:
        return render(request, 'principal/principal.html')


def ver_contacto(request, idcontacto):
    if Estoy_Log(request):
        sesion = {}
        sesion = {
            'idusuario': request.session['idusuario'],
            'usuario': request.session['usuario'],
            'rol': request.session['rol']
        }
        try:
            contacto = Contacto.objects.get(pk=idcontacto)
            nombre = contacto.nombre
        except Exception as e:
            nombre = ""
        contexto = {
            'idcontacto':idcontacto,
            'sesion': sesion,
            'nombre': nombre,
            'menu': 'SI',
            'es_admin': Soy_Admin(request)
        }
        return render(request, 'contacto/detalle_contactos.html', contexto)
    else:
        return render(request, 'principal/principal.html')


def ver_equipo(request, idequipo):
    if Estoy_Log(request):
        sesion = {}
        sesion = {
            'idusuario': request.session['idusuario'],
            'usuario': request.session['usuario'],
            'rol': request.session['rol']
        }
        try:
            equipo = Equipo.objects.get(pk=idequipo)
            nombre = equipo.nombre
            u_creacion = equipo.usuario_creacion
        except Exception as e:
            nombre = ""
        preferencia=None
        if not request.session['idusuario'] is None:
            preferencia=PreferenciaNotificacion.objects.filter(usuario=Usuario.objects.get(pk=request.session['idusuario'])).values('n_alerta','n_correo','n_sonido','horas','minutos')
        contexto = {
            'preferencias':preferencia,
            'sesion': sesion,
            'menu': 'SI',
            'idequipo': idequipo,
            'equipo': nombre,
            'u_creacion': u_creacion,
            'es_admin': Soy_Admin(request)
        }
        return render(request, 'equipos/detalle_equipos.html', contexto)
    else:
        return render(request, 'principal/principal.html')


def detalle_equipo(request, idequipo):
    if Estoy_Log(request):
        usuariosDelEquipo = Usuario.objects.filter(equipo=idequipo)
        if usuariosDelEquipo:
            data = serializers.serialize('json', usuariosDelEquipo)
        else:
            data = serializers.serialize('json', "")
        return HttpResponse(data, content_type="application/json")
    return render(request, 'principal/principal.html')

def resetear_monto(request, idmonto):
    if Soy_Admin(request):
        try:
            monto = Monto.objects.get(pk=idmonto)
            if monto:
                monto.total=0
                monto.save()
                return HttpResponse("SI")
            else:
                return HttpResponse("NO")
        except:
            return HttpResponse("NO")
    return HttpResponse("NO")

def actualizar_monto(request, idmonto,monto):
    if Soy_Admin(request):
        try:
            monto_actual = Monto.objects.get(pk=idmonto)
            if monto_actual:
                if monto_actual.total>0 and (float(monto))<=monto_actual.total:
                    monto_actual.total=monto_actual.total-(float(monto))
                    monto_actual.save()
                    return HttpResponse("SI")
                else:
                    return HttpResponse("NO")
            else:
                return HttpResponse("NO")
        except Exception as e:
            return HttpResponse(e)
    return HttpResponse("NO")

def eliminar_comentario(request, idcomentario):
    if Estoy_Log(request):
        try:
            comentario = Comentarios.objects.get(pk=idcomentario)
            if comentario:
                comentario.delete()
                return HttpResponse("SI")
            else:
                return HttpResponse("NO")
        except:
            return HttpResponse("NO")
    return HttpResponse("NO")

def total_mis_sugerencias(request):
    if Soy_Admin(request):
        try:
            total=Sugerencias.objects.filter(aplicada=False).count()
            return HttpResponse(total)
        except Exception as e:
            return HttpResponse(e)
    return HttpResponse(0)


def total_mis_notificaciones(request):
    if Estoy_Log(request):
        try:
            total=Notificacion.objects.filter(estado="Activa",usuario=Usuario.objects.get(pk=request.session['idusuario'])).count()
            return HttpResponse(total)
        except Exception as e:
            return HttpResponse(e)
    return render(request, 'principal/principal.html')                     



def notificar_recoger_Pedido(request,idpedido):
    if Estoy_Log(request):
        try:
            # encoding=utf8  
            #Funciona en python 2x
            
            import sys  

            reload(sys)  
            sys.setdefaultencoding('utf8')
            

            respuesta=""
            respuesta2=""
            pedido=Pedido.objects.get(pk=idpedido)
            idusuario=Pedido.objects.filter(pk=idpedido).values('usuario')
            usuario=Usuario.objects.get(pk=idusuario)

            #Obtener las preferencias de notificacion del usuario
            preferencias=PreferenciaNotificacion.objects.get(usuario=usuario)

                       
            se_notifico=Notificacion.objects.filter(pedido=pedido,tipo_notificacion="pedido_recoger")
            if se_notifico:
                return HttpResponse("NOTIFICADO")
            else:
                if preferencias.n_alerta:                    
                    """
                    ● El usuario que registra el pedido.
                    """
                    Notificacion.objects.create(
                        tipo_notificacion="pedido_recoger",
                        usuario=usuario,
                        pedido=pedido
                    )
                    """
                    ● El FF del equipo al que pertenezca la persona SI ES DEVELOPER.
                    """
                    if usuario.rol=='Developer':                        
                        id_equipo=Usuario.objects.filter(pk=idusuario).values('equipo')
                        usuario_creacion_equipo=Equipo.objects.filter(pk=id_equipo).values('usuario_creacion')
                        if usuario_creacion_equipo:
                            Notificacion.objects.create(
                                tipo_notificacion="pedido_recoger",
                                usuario=Usuario.objects.get(usuario=usuario_creacion_equipo),
                                pedido=pedido
                            )    
                    """
                    ● Los administradores del sistema SI NO ES ADMINISTRADOR                  
                    """  
                    #Consigo todos los admin                   

                    admins=Usuario.objects.filter(rol__rol="Administrador").values('pk')       

                    for admin in admins:
                        Notificacion.objects.create(
                            tipo_notificacion="pedido_recoger",
                            usuario=Usuario.objects.get(pk=admin['pk']),
                            pedido=pedido
                        )

                    respuesta="SI"                    
                if preferencias.n_correo:
                    try:
                        mail = EmailMultiAlternatives(
                          subject="Notificación para recoger pedido",
                          body='Hey, Tu pedido está listo para ser recogido. Pedido: '+pedido.descripcion+' Total: '+str(pedido.total),
                          from_email="Reisp Solutions <paradoxproject0@gmail.com>",
                          to=[usuario.correo],
                          headers={"Reply-To": "support@reispsolutions.com"}
                        )
                        mail.attach_alternative('<h1>Hey, Tu pedido está listo para ser recogido. Pedido: '+pedido.descripcion+' Total: '+str(pedido.total)+'</h1>', "text/html")
                        mail.send()
                        respuesta2="SI"   
                    except Exception as a:
                        return HttpResponse(a)

            if respuesta== "SI" or respuesta2=="SI":
                return HttpResponse("SI")
            elif respuesta=="NO" and respuesta2=="NO":
                return HttpResponse("NO")  
        except Exception as e:
            return HttpResponse(e)
    return render(request, 'principal/principal.html')


def notificarPedido(request,idpedido):
    if Estoy_Log(request):
        try:
            respuesta=""
            respuesta2=""
            pedido=Pedido.objects.get(pk=idpedido)
            idusuario=Pedido.objects.filter(pk=idpedido).values('usuario')
            usuario=Usuario.objects.get(pk=idusuario)

            #Obtener las preferencias de notificacion del usuario
            preferencias=PreferenciaNotificacion.objects.get(usuario=usuario)

            se_alerto=Notificacion.objects.filter(pedido=pedido,tipo_notificacion="pedido_llego")
            if se_alerto:
                return HttpResponse("NOTIFICADO")
            else:
                if preferencias.n_alerta:                    
                    Notificacion.objects.create(
                        tipo_notificacion="pedido_llego",
                        usuario=usuario,
                        pedido=pedido
                    )
                    respuesta="SI"
                if preferencias.n_correo:
                    try:
                        mail = EmailMultiAlternatives(
                          subject="Alerta de Pedido",
                          body='Hey, Tu pedido ha llegado. Pedido: '+pedido.descripcion+' Total: '+str(pedido.total),
                          from_email="Reisp Solutions <paradoxproject0@gmail.com>",
                          to=[usuario.correo],
                          headers={"Reply-To": "support@reispsolutions.com"}
                        )
                        mail.attach_alternative('<h1>Hey, Tu pedido ha llegado. Pedido: '+pedido.descripcion+' Total: '+str(pedido.total)+'</h1>', "text/html")
                        mail.send()
                        respuesta2="SI"
                    except Exception as a:
                        return HttpResponse(a)
            if respuesta== "SI" or respuesta2=="SI":
                return HttpResponse("SI")
            elif respuesta=="NO" and respuesta2=="NO":
                return HttpResponse("NO")                               
        except Exception as e:
            return HttpResponse(e)
    return render(request, 'principal/principal.html')

def eliminar_equipo(request, idequipo):
    try:
        equipo_eliminar = Equipo.objects.get(pk=idequipo)
        sin_equipo=Equipo.objects.get(pk=1)
        if equipo_eliminar:
            #Primero borro las relaciones del equipo y luego el equipo
            relaciones_del_equipo=equipos_usuarios.objects.filter(equipo=equipo_eliminar)
            
            lista_usuarios=[]

            for relacion_del_equipo in relaciones_del_equipo:
                lista_usuarios.append(relacion_del_equipo.usuario)
                relacion_del_equipo.delete()

            equipo_eliminar.delete()

            #ahora meto a todos estos usuarios sueltos en SIN equipo
            #siempre y cuando no esten en otros equipos
            for usuario in lista_usuarios:
                esta_en_otro_equipo=equipos_usuarios.objects.filter(usuario=Usuario.objects.get(usuario=usuario)).count()
                if esta_en_otro_equipo == 0:

                    equipos_usuarios.objects.create(
                        equipo=sin_equipo,
                        usuario=Usuario.objects.get(usuario=usuario)
                    )

            return HttpResponse("SI")
        else:
            return HttpResponse("NO")
    except Exception as e:
        return HttpResponse(e)

def cual_es_mi_foto(request):
    try:
        usuario=Usuario.objects.filter(pk=request.session['idusuario']).values('imagen')
        if usuario:
            data = json.dumps(list(usuario), cls=DjangoJSONEncoder)
        else:
            data = serializers.serialize('json', "")
    except Exception as e:
        data =""
    return HttpResponse(data, content_type="application/json")


    
def obtener_comentarios_contacto(request, idcontacto):
    try:
        contacto = Contacto.objects.get(pk=idcontacto)
        todo_comentarios = Comentarios.objects.filter(contacto=contacto).values('pk','comentario','fecha','usuario','usuario__usuario','contacto__nombre','usuario__imagen')
        if todo_comentarios:
            data = json.dumps(list(todo_comentarios), cls=DjangoJSONEncoder)
        else:
            data = serializers.serialize('json', "")
    except Exception as e:
        data = ""
    return HttpResponse(data, content_type="application/json")


def todos_los_montos(request):
    try:
        montos=Monto.objects.all().values('total','usuario__usuario')

        if montos:
            data =json.dumps(list(montos),cls=DjangoJSONEncoder)
        else:
            data=serializers.serialize('json', "")
    except Exception as e:
        data = ""
    return HttpResponse(data, content_type="application/json")

def obtener_ratings_contacto_personal(request,idcontacto):
    try:
        contacto =Contacto.objects.get(pk=idcontacto)
        rating_personal=Rating.objects.filter(contacto=contacto,usuario=request.session['idusuario']).values('pk','rating','fecha','usuario','usuario__usuario','contacto__nombre','usuario__imagen')
        if rating_personal:
            data =json.dumps(list(rating_personal),cls=DjangoJSONEncoder)
        else:
            data=serializers.serialize('json', "")
    except Exception as e:
        data = ""
    return HttpResponse(data, content_type="application/json")

def obtener_ratings_contacto(request, idcontacto):
    try:
        contacto = Contacto.objects.get(pk=idcontacto)
        todo_ratings = Rating.objects.filter(contacto=contacto).values('pk','rating','fecha','usuario','usuario__usuario','contacto__nombre','usuario__imagen')
        if todo_ratings:
            data = json.dumps(list(todo_ratings), cls=DjangoJSONEncoder)
        else:
            data = serializers.serialize('json', "")
    except Exception as e:
        data = ""
    return HttpResponse(data, content_type="application/json")


def obtener_datos_comentario(request, idcomentario):
    todo_comm = Comentarios.objects.filter(pk=idcomentario)
    if todo_comm:
        data = serializers.serialize('json', todo_comm)
    else:
        data = serializers.serialize('json', "")
    return HttpResponse(data, content_type="application/json")


def obtener_datos_equipo(request, idequipo):
    todo_equipos = Equipo.objects.filter(pk=idequipo)
    if todo_equipos:
        data = serializers.serialize('json', todo_equipos)
    else:
        data = serializers.serialize('json', "")
    return HttpResponse(data, content_type="application/json")

def obtener_items(request,idmenu):
    try:
        menu=Menu.objects.get(pk=idmenu)
        todo_items=ItemsMenu.objects.filter(menu=menu).values('nombre','precio','CatMenu__nombre')
        if todo_items:
            data = json.dumps(list(todo_items), cls=DjangoJSONEncoder)
        else:
            data = serializers.serialize('json', "")
        return HttpResponse(data, content_type="application/json")
    except Exception as e:
        return HttpResponse(data, content_type="application/json")
    
def giveme_pk_and_name_contacto(request,idusuario):
    datos=contactos_usuarios.objects.filter(usuario=Usuario.objects.get(pk=idusuario)).values('contacto__pk','contacto__nombre')
    if datos:
        data = json.dumps(list(datos), cls=DjangoJSONEncoder)
    else:
        data = serializers.serialize('json', "")
    return HttpResponse(data, content_type="application/json")

def obtener_datos_usuario(request, idusuario):
    todo_usuarios = Usuario.objects.filter(pk=idusuario).values('pk','usuario','nombre','correo','rol','activo','imagen','direccion')
    if todo_usuarios:
        data = json.dumps(list(todo_usuarios), cls=DjangoJSONEncoder)
    else:
        data = serializers.serialize('json', "")
    return HttpResponse(data, content_type="application/json")

def obtener_datos_item_menu(request,iditem):
    todo_items=ItemsMenu.objects.filter(pk=iditem);
    if todo_items:
        data = serializers.serialize('json', todo_items)
    else:
        data = serializers.serialize('json', "")
    return HttpResponse(data, content_type="application/json")

def obtener_datos_categoria_menu(request,idcategoria):
    todo_categorias=CatMenu.objects.filter(pk=idcategoria);
    if todo_categorias:
        data = serializers.serialize('json', todo_categorias)
    else:
        data = serializers.serialize('json', "")
    return HttpResponse(data, content_type="application/json")


def obtener_datos_menu(request,idmenu):
    todo_menus = Menu.objects.filter(pk=idmenu)
    if todo_menus:
        data = serializers.serialize('json', todo_menus)
    else:
        data = serializers.serialize('json', "")
    return HttpResponse(data, content_type="application/json")



def activar(request, codigo):
    try:
        usuario = Usuario.objects.get(codigo_generado=codigo)
        if usuario:
            if usuario.activo:
                return render(request, 'principal/notificacion.html', {'mensaje': "ACTIVADO",
                                                                       'hora': datetime.datetime.now()})
            else:
                usuario.activo = True
                usuario.save()
                return render(request, 'principal/notificacion.html', {'mensaje': "SI", 'hora': datetime.datetime.now()})
        else:
            return render(request, 'principal/notificacion.html', {'mensaje': "ERROR"})
    except Exception as e:
        return render(request, 'principal/notificacion.html', {'mensaje': "EXPLOTO"})


def login(request):
    if request.method == 'POST':
        usuario_ = request.POST['usuario']
        clave_ = request.POST['clave']
        cerrar_sesion = request.POST['cerrar_sesion']
        try:
            usuario_objeto = Usuario.objects.get(usuario=usuario_, clave=clave_)
            if usuario_objeto.activo:
                request.session['idusuario'] = int(usuario_objeto.id)
                request.session['usuario'] = usuario_
                request.session['cerrar_sesion']=cerrar_sesion
                if str(usuario_objeto.rol) == "Administrador":
                    request.session['rol'] = "Administrador"
                elif str(usuario_objeto.rol) == 'Developer':
                    request.session['rol'] = 'Developer'
                elif str(usuario_objeto.rol) == "Firefighter":
                    request.session['rol'] = 'Firefighter'
                elif str(usuario_objeto.rol) == "Invitado":
                    request.session['rol'] = 'Invitado'
                elif str(usuario_objeto.rol) == "Contacto":
                    request.session['rol'] = 'Contacto'
                else:
                    request.session['rol'] = 'Perfil inválido'
                return HttpResponse("SI")
            else:
                return HttpResponse("No-activo")
        except Exception as e:
            return HttpResponse("Error2")
    else:
        return HttpResponse("Error1")


def registro_sugerencia(request):
    if Estoy_Log(request):
        if request.method == 'POST':
            contacto = request.POST['contacto']
            telefono = request.POST['telefono']
            direccion = request.POST['direccion']
            categoria = request.POST['categoria']
            try:
                usuario_objeto = Usuario.objects.get(pk=request.session['idusuario'])
                categoria = Categoria.objects.get(pk=categoria)
                Sugerencias.objects.create(
                    nombre=contacto,
                    telefono=telefono,
                    direccion=direccion,
                    categoria=categoria,
                    usuario=usuario_objeto,
                    aplicada=False
                )
                return HttpResponse("SI")
            except Exception as e:
                return HttpResponse("Error1")
        else:
            return HttpResponse("Error2")
    return render(request, 'principal/principal.html')


def registro(request):

    if request.method == 'GET':
        nombre = request.GET['nombre']
        usuario = request.GET['usuario']
        clave = request.GET['clave']
        correo = request.GET['correo']
        activo = False
        codigo = request.GET['codigo']
        direccion = request.GET['direccion']

        try:
            usuarios_total = Usuario.objects.count()
            roles = Rol.objects.count()
            if roles == 0:
                Rol.objects.create(rol='Administrador')
                Rol.objects.create(rol='Firefighter')
                Rol.objects.create(rol='Developer')
                Rol.objects.create(rol='Invitado')
                Rol.objects.create(rol='Contacto')
            equipos = Equipo.objects.count()
            if equipos == 0:
                Equipo.objects.create(nombre='Sin equipo', usuario_creacion='Admin', id_usuario=0)
            sin_equipo = Equipo.objects.get(nombre='Sin equipo')
            if usuarios_total > 0:
                rol_developer = Rol.objects.get(rol='Developer')
            else:
                rol_developer = Rol.objects.get(rol='Administrador')

            usuario_crear=Usuario()
            usuario_crear.nombre=nombre
            usuario_crear.usuario=usuario
            usuario_crear.clave=clave
            usuario_crear.correo=correo
            usuario_crear.rol=rol_developer
            usuario_crear.activo=activo
            usuario_crear.codigo_generado=codigo
            usuario_crear.direccion=direccion
            usuario_crear.save()

            #INICIO nueva manera
            introduccion_a_equipo=equipos_usuarios()
            introduccion_a_equipo.equipo=sin_equipo
            introduccion_a_equipo.usuario=usuario_crear
            introduccion_a_equipo.save()
            #FIN nueva manera

            preferencia_crear=PreferenciaNotificacion()
            preferencia_crear.usuario=usuario_crear
            preferencia_crear.save()

            try:
                send_mail(
                    'Activación de cuenta',
                    #'Hey, para activar tu cuenta de solucions Reisp, debes darle click a este link 10.0.0.10:8000/activar/' + codigo,
                    'Hey, '+str(usuario_crear.nombre)+' para activar tu cuenta de Delivery App, debes darle click a este link http://edrreisp.herokuapp.com/activar/' + codigo,
                    #'Hey, para activar tu cuenta de solucions Reisp, debes darle click a este link https://reispsolutions.herokuapp.com/activar/' + codigo,
                    settings.EMAIL_HOST_USER,
                    [correo],
                    fail_silently=False,
                )
            except Exception as a:
                return HttpResponse("Fallo al enviar el correo, pongase en contacto con un administrador")

            return HttpResponse("SI")
        except Exception as e:
            return HttpResponse(e)
    else:
        return HttpResponse("Error2")

def agrega_usuarios_equipo(request):
    if Estoy_Log(request):
        if request.method == 'POST':
            usuario = request.POST['usuario']
            equipo_id = request.POST['equipo']
            try:
                usuario_agregar = Usuario.objects.get(pk=usuario)
                equipo_nuevo = Equipo.objects.get(pk=equipo_id)

                
                #compruebo si este usuario no esta en "SIN EQUIPO"
                sin_equipo=Equipo.objects.get(nombre="Sin equipo")
                esta_en_sin_equipo=None
                try:
                    esta_en_sin_equipo=equipos_usuarios.objects.get(usuario=usuario_agregar,equipo=sin_equipo)
                except ObjectDoesNotExist:
                    esta_en_sin_equipo=None

                if esta_en_sin_equipo:
                        esta_en_sin_equipo.delete()
                
                #compruebo si este usuario no esta en otro equipo para agregarle la relacion
                comprobar_si_esta_en_otro_equipo=equipos_usuarios.objects.filter(usuario=usuario_agregar)
                if comprobar_si_esta_en_otro_equipo:
                    return HttpResponse("YA_ESTA_EN_EQUIPO")
                else:               
                    equipos_usuarios.objects.create(
                        equipo=equipo_nuevo,
                        usuario=usuario_agregar
                    )
                    return HttpResponse("SI")
            except Exception as e:
                return HttpResponse(e)
        else:
            return HttpResponse("Error2")
    return render(request, 'principal/principal.html')



def registro_equipos(request):
    if Estoy_Log(request):
        if request.method == 'POST':
            equipo = request.POST['equipo']
            usuario = request.session['usuario']
            idusuario = request.session['idusuario']
            try:
                equipo_crear=Equipo()
                equipo_crear.nombre=equipo
                equipo_crear.usuario_creacion=usuario
                equipo_crear.id_usuario=idusuario
                equipo_crear.save()

                #Ahora procedo a agregar al FF a su mismo equipo          
                usuario_ff_actual = Usuario.objects.get(pk=request.session['idusuario'])
                equipo = Equipo.objects.get(pk=equipo_crear.pk)
                
                equipos_usuarios.objects.create(
                    usuario=usuario_ff_actual,
                    equipo=equipo
                )
                #y luego lo saco de sinequipo
                sin_equipo=Equipo.objects.get(nombre="Sin equipo")
                esta_en_sin_equipo=None
                try:
                    esta_en_sin_equipo=equipos_usuarios.objects.get(usuario=usuario_ff_actual,equipo=sin_equipo)
                except ObjectDoesNotExist:
                    esta_en_sin_equipo=None

                if esta_en_sin_equipo:
                    esta_en_sin_equipo.delete()



                return HttpResponse("SI")
            except Exception as e:
                return HttpResponse("Error1")
        else:
            return HttpResponse("Error2")
    return render(request, 'principal/principal.html')

def guardar_configuracion(request):
    if Estoy_Log(request):
        if request.method == 'POST':
            n_alerta=request.POST['n_alerta']
            n_correo=request.POST['n_correo']
            n_sonido=request.POST['n_sonido']
            horas=request.POST.get('horas')
            minutos=request.POST.get('minutos')
            try:
                usuario=Usuario.objects.get(pk=request.session['idusuario'])

                preferencia_del_usuario=PreferenciaNotificacion.objects.get(usuario=usuario)
                if n_alerta== "true":
                    preferencia_del_usuario.n_alerta=True
                else:
                    preferencia_del_usuario.n_alerta=False
                if n_correo== "true":
                    preferencia_del_usuario.n_correo=True
                else:
                    preferencia_del_usuario.n_correo=False
                if n_sonido== "true":
                    preferencia_del_usuario.n_sonido=True
                else:
                    preferencia_del_usuario.n_sonido=False
                preferencia_del_usuario.horas=horas
                preferencia_del_usuario.minutos=minutos
                preferencia_del_usuario.save()
                return HttpResponse("SI")
            except Exception as e:
                return HttpResponse(e)
        else:
            return HttpResponse("Error1")
    return render(request, 'principal/principal.html')


def cambio_clave(request):
    if Estoy_Log(request):
        if request.method == 'POST':
            clave = request.POST['clave']
            try:
                usuario =Usuario.objects.get(pk=request.session['idusuario'])
                if usuario:
                    usuario.clave=clave
                    usuario.save()
                    return HttpResponse("SI")
                else:
                    return HttpResponse("NO")
            except Exception as e:
                return HttpResponse("Error1")
        else:
            return HttpResponse("Error2")
    return render(request, 'principal/principal.html')


def cambiame_correo(request,correo):
    if Estoy_Log(request):
        try:
            usuario=Usuario.objects.get(pk=request.session['idusuario'])
            if usuario:
                usuario.correo=correo
                usuario.save()
                return HttpResponse("SI")
            else:
                return HttpResponse("NO")
        except Exception as e:
            return HttpResponse("NO")
    return render(request, 'principal/principal.html')

def cambiame_direccion(request,direccion):
    if Estoy_Log(request):
        try:
            usuario=Usuario.objects.get(pk=request.session['idusuario'])
            if usuario:
                usuario.direccion=direccion
                usuario.save()
                return HttpResponse("SI")
            else:
                return HttpResponse("NO")
        except Exception as e:
            return HttpResponse("NO")
    return render(request, 'principal/principal.html')

def cambiame_nombre(request,nombre):
    if Estoy_Log(request):
        try:
            usuario=Usuario.objects.get(pk=request.session['idusuario'])
            if usuario:
                usuario.nombre=nombre
                usuario.save()
                return HttpResponse("SI")
            else:
                return HttpResponse("NO")
        except Exception as e:
            return HttpResponse("NO")
    return render(request, 'principal/principal.html')

def modifica_equipos(request):
    if Estoy_Log(request):
        if request.method == 'POST':
            id = request.POST['equipo2']
            equipo = request.POST['equipo']
            try:
                equipo_modificar= Equipo.objects.get(pk=id)
                if equipo_modificar:
                    equipo_modificar.nombre=equipo
                    equipo_modificar.save()
                    return HttpResponse("SI")
                else:
                    return HttpResponse("Error1")
            except Exception as e:
                return HttpResponse("Error1")
        else:
            return HttpResponse("Error2")
    return render(request, 'principal/principal.html')

def modifica_item_menu(request):
    if request.method == 'POST':
        iditem=request.POST['id']
        item = request.POST['item']
        precio=request.POST['precio']
        idmenu=request.POST['menu']
        idcategoria=request.POST['categoria']

        if precio == "":
            precio=0
        try:
            categoria=CatMenu.objects.get(pk=idcategoria)
        except Exception as e:
            categoria=None
        try:
            menu=Menu.objects.get(pk=idmenu)
            item_modificar = ItemsMenu.objects.get(pk=iditem)
            item_modificar.nombre = item
            item_modificar.precio = precio
            item_modificar.menu = menu
            item_modificar.CatMenu=categoria
            item_modificar.save()
            return HttpResponse("SI")
        except Exception as e:
            return HttpResponse("Error1")
    else:
        return HttpResponse("Error2")

def modifica_categoria_menu(request):
    if request.method == 'POST':
        categoria = request.POST['categoria']
        idcategoria=request.POST['idcategoria']
        try:
            categoria_modificar = CatMenu.objects.get(pk=idcategoria)
            categoria_modificar.nombre = categoria
            categoria_modificar.save()
            return HttpResponse("SI")
        except Exception as e:
            return HttpResponse("Error1")
    else:
        return HttpResponse("Error2")

def modifica_menu(request):
    if request.method == 'POST':
        idmenu = request.POST['idmenu']
        menu = request.POST['nombre']
        contacto = request.POST['contacto']
        try:
            contacto = Contacto.objects.get(id=contacto)
            menu_modificar = Menu.objects.get(pk=idmenu)
            menu_modificar.nombre = menu
            menu_modificar.contacto = contacto
            menu_modificar.save()
            return HttpResponse("SI")
        except Exception as e:
            return HttpResponse("Error1")
    else:
        return HttpResponse("Error2")

def modifica_usuario(request):
    if request.method == 'POST':
        idusuario = request.POST['idusuario']
        nombre = request.POST['nombre']
        usuario = request.POST['usuario']
        correo = request.POST['correo']
        rol = request.POST['rol']        
        contacto=request.POST['contacto']
        direccion=request.POST['direccion']
        imagen=request.FILES.get('imagen')
        try:
            rol_ = Rol.objects.get(id=rol)
            usuario_modificar = Usuario.objects.get(pk=idusuario)
            usuario_modificar.usuario = usuario
            usuario_modificar.nombre = nombre
            usuario_modificar.correo = correo
            usuario_modificar.rol = rol_
            usuario_modificar.direccion=direccion
            
            

            if imagen:
                usuario_modificar.imagen=imagen
            usuario_modificar.save()
            return HttpResponse("SI")
        except Exception as e:
            return HttpResponse(e)
    else:
        return HttpResponse("Error2")

def editar_rate(request, idrate):
    if Estoy_Log(request):
        if request.method == 'GET':
            rate=request.GET['rate']
            try:
                rating_modificar=Rating.objects.get(pk=idrate)
                if rating_modificar:
                    rating_modificar.rating=rate
                    rating_modificar.save()
                    return HttpResponse("SI")
                else:
                    return HttpResponse("NO")
            except Exception as e:
                return HttpResponse(e)
        else:
            return HttpResponse("Error1")
    return HttpResponse("NO")


def registro_item_menu(request):
    if request.method == 'POST':
        item = request.POST['item']
        precio = request.POST['precio']
        idcategoria = request.POST['categoria']
        idmenu= request.POST['menu']

        if precio =="":
            precio=0
        try:
            categoria=CatMenu.objects.get(pk=idcategoria)
        except Exception as e:
            categoria=None        
        try:
            
            menu=Menu.objects.get(pk=idmenu)
            ItemsMenu.objects.create(
                nombre=item,
                precio=precio,
                CatMenu=categoria,
                menu=menu
            )
            return HttpResponse("SI")
        except Exception as e:
            return HttpResponse(e)
    else:
        return HttpResponse("Error2")

def registro_categoria_menu(request):
    if request.method == 'POST':
        categoria = request.POST['categoria']
        try:
            CatMenu.objects.create(
                nombre=categoria
            )
            return HttpResponse("SI")
        except Exception as e:
            return HttpResponse("Error1")
    else:
        return HttpResponse("Error2")

def registro_menu(request):
    if request.method == 'POST':
        nombre = request.POST['nombre']
        contacto = request.POST['contacto']
        try:
            contacto = Contacto.objects.get(id=contacto)
            Menu.objects.create(
                nombre=nombre,
                contacto=contacto
            )
            return HttpResponse("SI")
        except Exception as e:
            return HttpResponse("Error1")
    else:
        return HttpResponse("Error2")

def cambiame_foto(request):
    import datetime
    now = datetime.datetime.now()
    if request.method == 'POST':
        imagen=request.FILES.get('imagen')
        if imagen:
            imagen.name=str(now.strftime("%Y-%m-%d %H:%M"))+str(imagen.name)        
        try:
            usuario=Usuario.objects.get(pk=request.session['idusuario'])
            if usuario:
                usuario.imagen=imagen
                usuario.save()
                return HttpResponse("SI")
            else:
                return HttpResponse("NO")
        except Exception as e:
            return HttpResponse("NO")      
    else:
        return HttpResponse("Error1")      

def registro_usuario(request):
    import datetime
    now = datetime.datetime.now()
    
    if request.method == 'POST':
        nombre = request.POST['nombre']
        usuario = request.POST['usuario']
        clave = request.POST['clave']
        correo = request.POST['correo']
        activo = False
        codigo = request.POST['codigo']
        rol = request.POST['rol']
        contacto=request.POST['contacto']
        direccion=request.POST['direccion']
        imagen=request.FILES.get('imagen')
        if imagen:
            imagen.name=str(now.strftime("%Y-%m-%d %H:%M"))+str(imagen.name)
        sin_equipo = Equipo.objects.get(nombre='Sin equipo')
        try:
            rol = Rol.objects.get(id=rol)

            usuario_crear=Usuario()
            usuario_crear.nombre=nombre
            usuario_crear.usuario=usuario
            usuario_crear.clave=clave
            usuario_crear.correo=correo
            usuario_crear.rol=rol
            usuario_crear.activo=activo
            usuario_crear.codigo_generado=codigo
            usuario_crear.imagen=imagen
            usuario_crear.direccion=direccion
            usuario_crear.save()
            
            #Si el contacto que se mando es diferente a 0, lo asocio con este usuario
            if not contacto=="0":
                contactos_usuarios.objects.create(
                    contacto=Contacto.objects.get(pk=contacto),
                    usuario=usuario_crear
                )


            #INICIO nueva manera
            introduccion_a_equipo=equipos_usuarios()
            introduccion_a_equipo.equipo=sin_equipo
            introduccion_a_equipo.usuario=usuario_crear
            introduccion_a_equipo.save()
            #FIN nueva manera



            preferencia_crear=PreferenciaNotificacion()
            preferencia_crear.usuario=usuario_crear
            preferencia_crear.save()

            return HttpResponse("SI")
        except Exception as e:
            return HttpResponse(e)
    else:
        return HttpResponse("Error2")

def equipos_del_modal(request):
    if Estoy_Log(request):
        equipos=None
        if str(request.session['rol']) == "Administrador":
            #Si soy admin puedo ver todos los equipos, menos el Sin equipo
            equipos=Equipo.objects.filter(~Q(id_usuario = 0)).values('pk','nombre','usuario_creacion')

        elif str(request.session['rol']) == "Firefighter":
            equipos=Equipo.objects.filter(id_usuario=request.session['idusuario']).values('pk','nombre','usuario_creacion')
            #Si soy FF solo podre ver mis equipos
        if equipos:
            data = json.dumps(list(equipos), cls=DjangoJSONEncoder)
        else:
            data = serializers.serialize('json', "")
        return HttpResponse(data, content_type="application/json")
    return render(request, 'principal/principal.html')

def get_teams_ff(request):
    if Estoy_Log(request):
        todos_equipos = Equipo.objects.all().values('pk','nombre','usuario_creacion').order_by('pk')
        if todos_equipos:
            data = json.dumps(list(todos_equipos), cls=DjangoJSONEncoder)
        else:
            data = serializers.serialize('json', "")
        return HttpResponse(data, content_type="application/json")
    return render(request, 'principal/principal.html')
    
def obtener_equipos_ff(request,filtro,orden,texto):
    if Estoy_Log(request):
        if orden == "ascendente":
            orden=""
        elif orden == "descendente":
            orden="-"

        if filtro == "equipo":
            orden=orden+"nombre"
        elif filtro == "usuario":
            orden=orden+"usuario_creacion"

        if texto == "___":
            todos_equipos = Equipo.objects.all().values('pk','nombre','usuario_creacion').order_by(orden)
        else:
            if filtro =="equipo":
                todos_equipos = Equipo.objects.all().values('pk','nombre','usuario_creacion').filter(nombre__contains=texto).order_by(orden)
            elif filtro =="usuario":
                todos_equipos = Equipo.objects.all().values('pk','nombre','usuario_creacion').filter(usuario_creacion__contains=texto).order_by(orden)
        if todos_equipos:
            data = json.dumps(list(todos_equipos), cls=DjangoJSONEncoder)
        else:
            data = serializers.serialize('json', "")
        return HttpResponse(data, content_type="application/json")
    return render(request, 'principal/principal.html')

def get_directory_list(request):
    if Estoy_Log(request):
        todas_contactos = Contacto.objects.all().values('pk','desde','hasta','nombre', 'telefono','direccion', 'categoria__nombre', 'usuario__usuario','activo','usuario__imagen').annotate(average_rating=Avg('rating__rating')).order_by('categoria__nombre')
        if todas_contactos:
            data = json.dumps(list(todas_contactos), cls=DjangoJSONEncoder)
        else:
            data = serializers.serialize('json', "")
        return HttpResponse(data, content_type="application/json")
    return render(request, 'principal/principal.html')
def obtener_contactos_directorio(request,filtro,orden,texto):
    if Estoy_Log(request):
        if orden == "ascendente":
            orden = ""
        elif orden == "descendente":
            orden = "-"

        if filtro == "categoria":
            orden=orden+"categoria__nombre"
        elif filtro =="nombre":
            orden=orden+"nombre"
        elif filtro =="rating":
            orden=orden+"average_rating"
        
        if texto == "___":
            #'pk', 'nombre', 'telefono',direccion', 'categoria__nombre', 'usuario__usuario'
            todas_contactos = Contacto.objects.all().values('pk','desde','hasta','nombre', 'telefono','direccion', 'categoria__nombre', 'usuario__usuario','activo','usuario__imagen').annotate(average_rating=Avg('rating__rating')).order_by(orden)
        else:
            if filtro == "categoria":
                todas_contactos = Contacto.objects.values('pk','desde','hasta','nombre', 'telefono','direccion', 'categoria__nombre', 'usuario__usuario','activo','usuario__imagen').annotate(average_rating=Avg('rating__rating')).filter(categoria__nombre__contains=texto).order_by(orden)
            elif filtro == "nombre":
                todas_contactos = Contacto.objects.values('pk','desde','hasta','nombre', 'telefono','direccion', 'categoria__nombre', 'usuario__usuario','activo','usuario__imagen').annotate(average_rating=Avg('rating__rating')).filter(nombre__contains=texto).order_by(orden)
            elif filtro == "rating":
                todas_contactos = Contacto.objects.values('pk','desde','hasta','nombre', 'telefono','direccion', 'categoria__nombre', 'usuario__usuario','activo','usuario__imagen').annotate(average_rating=Avg('rating__rating')).filter(average_rating__contains=texto).order_by(orden)                

        if todas_contactos:
            data = json.dumps(list(todas_contactos), cls=DjangoJSONEncoder)
        else:
            data = serializers.serialize('json', "")
        return HttpResponse(data, content_type="application/json")
    return render(request, 'principal/principal.html')



def obtener_usuariosDeveloper_paraEquipos(request):
    if Estoy_Log(request):        
        try:
            sin_equipo=Equipo.objects.get(nombre="Sin equipo")
            usuarios= equipos_usuarios.objects.values('usuario__pk','usuario__usuario').filter(equipo=sin_equipo,usuario__rol=3)
            if usuarios:
                data = json.dumps(list(usuarios), cls=DjangoJSONEncoder)
            else:
                data = serializers.serialize('json', "")            
            return HttpResponse(data, content_type="application/json") 
        except Exception as e:
            return HttpResponse("", content_type="application/json")
        
         
def get_items_menu(request,idmenu):
    if Soy_Admin(request):
        try:
            menu=Menu.objects.get(pk=idmenu)
            todo_items_menu=ItemsMenu.objects.filter(menu=menu).values('pk','nombre','CatMenu__nombre','precio').order_by('pk')
            if todo_items_menu:
                data = json.dumps(list(todo_items_menu), cls=DjangoJSONEncoder)
            else:
                data = serializers.serialize('json', "")
            return HttpResponse(data, content_type="application/json")
        except Exception as e:
            return HttpResponse(e)
    return render(request, 'principal/principal.html') 
    
def obtener_items_menu(request,filtro,orden,texto):
    if Soy_Admin(request):
        if request.method == 'POST':
            if orden =="ascendente":
                orden=""
            elif orden=="descendente":
                orden="-"

            if filtro =="item":
                orden=orden+"nombre"
            elif filtro =="categoria":
                orden=orden+"CatMenu__nombre"
            elif filtro =="precio":
                orden=orden+"precio"
            try:
                menu=Menu.objects.get(pk=request.POST['idmenu'])
                if texto =="___":
                    todo_items_menu=ItemsMenu.objects.filter(menu=menu).values('pk','nombre','CatMenu__nombre','precio').order_by(orden)
                else:
                    if filtro =="item":
                        todo_items_menu=ItemsMenu.objects.filter(menu=menu,nombre__contains=texto).values('pk','nombre','CatMenu__nombre','precio').order_by(orden)
                    elif filtro =="categoria":
                        todo_items_menu=ItemsMenu.objects.filter(menu=menu,CatMenu__nombre__contains=texto).values('pk','nombre','CatMenu__nombre','precio').order_by(orden)
                    elif filtro =="precio":
                        todo_items_menu=ItemsMenu.objects.filter(menu=menu,precio__contains=texto).values('pk','nombre','CatMenu__nombre','precio').order_by(orden)
                    
                if todo_items_menu:
                    data = json.dumps(list(todo_items_menu), cls=DjangoJSONEncoder)
                else:
                    data = serializers.serialize('json', "")
                return HttpResponse(data, content_type="application/json")
            except Exception as e:
                return HttpResponse(e)
    return render(request, 'principal/principal.html') 

def get_categories_menu(request):
    if Soy_Admin(request):
        todo_categoria_menu=CatMenu.objects.all().values('pk','nombre').order_by('pk')
        if todo_categoria_menu:
            data = json.dumps(list(todo_categoria_menu), cls=DjangoJSONEncoder)
        else:
            data = serializers.serialize('json', "")
        return HttpResponse(data, content_type="application/json")
    return render(request, 'principal/principal.html') 
def obtener_categoria_menus(request,filtro,orden,texto):
    if Soy_Admin(request):
        if request.method == 'POST':
            if orden =="ascendente":
                orden=""
            elif orden=="descendente":
                orden="-"

            if filtro =="categoria":
                orden=orden+"nombre"
            try:
                if texto =="___":
                    todo_categoria_menu=CatMenu.objects.all().values('pk','nombre').order_by(orden)
                else:
                    if filtro =="categoria":
                        todo_categoria_menu=CatMenu.objects.filter(nombre__contains=texto).values('pk','nombre').order_by(orden)

                if todo_categoria_menu:
                    data = json.dumps(list(todo_categoria_menu), cls=DjangoJSONEncoder)
                else:
                    data = serializers.serialize('json', "")
                return HttpResponse(data, content_type="application/json")
            except Exception as e:
                return HttpResponse(e)
    return render(request, 'principal/principal.html') 

def oye_ponlo_aqui(request,idusuario,idequipo):
    if Estoy_Log(request):        
        try:
            usuario_agregar = Usuario.objects.get(pk=idusuario)
            equipo_nuevo = Equipo.objects.get(pk=idequipo)
            
            #FORMA NUEVA
            #compruebo si este usuario no esta en "SIN EQUIPO"
            sin_equipo=Equipo.objects.get(nombre="Sin equipo")
            esta_en_sin_equipo=None
            try:
                esta_en_sin_equipo=equipos_usuarios.objects.get(usuario=usuario_agregar,equipo=sin_equipo)
            except ObjectDoesNotExist:
                esta_en_sin_equipo=None

            if esta_en_sin_equipo:
                esta_en_sin_equipo.delete()           

            #FIN comprobacion
            comprobar_si_esta_en_otro_equipo=equipos_usuarios.objects.filter(usuario=usuario_agregar)
            if comprobar_si_esta_en_otro_equipo:
                return HttpResponse("YA_ESTA_EN_EQUIPO")
            else:               
                equipos_usuarios.objects.create(
                    equipo=equipo_nuevo,
                    usuario=usuario_agregar
                )
                return HttpResponse("SI")
        except Exception as e:
            return HttpResponse(e)        
    return render(request, 'principal/principal.html')


def dameLosItemsMenu(request,idmenu):
    if Estoy_Log(request):
        try:
            menu=Menu.objects.get(pk=idmenu)
            items_mios=ItemsMenu.objects.filter(menu=menu).values('pk','nombre','precio','CatMenu__nombre')
            if items_mios:
                data =json.dumps(list(items_mios),cls=DjangoJSONEncoder)
            else:
                data =serializers.serialize('json',"")            
        except Exception as e:
            data =serializers.serialize('json',"")
        return HttpResponse(data,content_type="application/json")
    return render(request, 'principal/principal.html') 

def dameLosMenusContacto(request, idcontacto):
    if Estoy_Log(request):
        try:
            contacto=Contacto.objects.get(pk=idcontacto)
            menus_mios=Menu.objects.filter(contacto=contacto).values('pk','nombre')
            if menus_mios:
                data =json.dumps(list(menus_mios),cls=DjangoJSONEncoder)
            else:
                data =serializers.serialize('json',"")            
        except Exception as e:
            data =serializers.serialize('json',"")
        return HttpResponse(data,content_type="application/json")
    return render(request, 'principal/principal.html') 

def get_menus(request):
    if Soy_Admin(request):
        todo_menus=Menu.objects.all().values('pk','nombre','contacto__nombre').order_by('pk')
        if todo_menus:
            data = json.dumps(list(todo_menus), cls=DjangoJSONEncoder)
        else:
            data = serializers.serialize('json', "")
        return HttpResponse(data, content_type="application/json")
    return render(request, 'principal/principal.html') 
def obtener_menus(request, filtro, orden, texto):
    if Soy_Admin(request):        
        if orden =="ascendente":
            orden=""
        elif orden=="descendente":
            orden="-"

        if filtro =="menu":
            orden=orden+"nombre"
        elif filtro == "contacto":
            orden=orden+"contacto__nombre"
        
        if texto == '___':
            todo_menus=Menu.objects.all().values('pk','nombre','contacto__nombre').order_by(orden)
        else:
            if filtro=="menu":
                todo_menus=Menu.objects.filter(nombre__contains=texto).values('pk','nombre','contacto__nombre').order_by(orden)
            elif filtro =="contacto":
                todo_menus=Menu.objects.filter(contacto__nombre__contains=texto).values('pk','nombre','contacto__nombre').order_by(orden)
        if todo_menus:
            data = json.dumps(list(todo_menus), cls=DjangoJSONEncoder)
        else:
            data = serializers.serialize('json', "")
        return HttpResponse(data, content_type="application/json")
    return render(request, 'principal/principal.html') 

def get_users(request):
    todo_usuarios =Usuario.objects.all().values('pk','usuario','nombre','correo','rol__rol','activo','imagen','direccion').order_by('pk')
    if todo_usuarios:
        data = json.dumps(list(todo_usuarios), cls=DjangoJSONEncoder)
    else:
        data = serializers.serialize('json', "")
    return HttpResponse(data, content_type="application/json")

def obtener_usuarios(request,filtro,orden,texto):
    if orden =="ascendente":
        orden=""
    elif orden=="descendente":
        orden="-"

    if filtro =="usuario":
        orden=orden+"usuario"
    elif filtro == "correo":
        orden=orden+"correo"
    elif filtro=="rol":
        orden=orden+"rol__rol"

    if texto == "___":
        todo_usuarios =Usuario.objects.all().values('pk','usuario','nombre','correo','rol__rol','activo','imagen').order_by(orden)
    else:
        if filtro =="usuario":
            todo_usuarios =Usuario.objects.filter(usuario__contains=texto).values('pk','usuario','nombre','correo','rol__rol','activo','imagen').order_by(orden)
        elif filtro == "correo":
            todo_usuarios =Usuario.objects.filter(correo__contains=texto).values('pk','usuario','nombre','correo','rol__rol','activo','imagen').order_by(orden)
        elif filtro =="rol":
            todo_usuarios =Usuario.objects.filter(rol__rol__contains=texto).values('pk','usuario','nombre','correo','rol__rol','activo','imagen').order_by(orden)            

    if todo_usuarios:
        data = json.dumps(list(todo_usuarios), cls=DjangoJSONEncoder)
    else:
        data = serializers.serialize('json', "")
    return HttpResponse(data, content_type="application/json")


def get_suggestions_personals(request):
    objeto_usuario=Usuario.objects.get(pk=request.session['idusuario'])
    todas_sugerencias = Sugerencias.objects.filter(usuario=objeto_usuario).values('pk','nombre', 'telefono', 'direccion', 'categoria__nombre', 'usuario__usuario','usuario', 'aplicada', 'fecha','usuario__imagen').order_by('pk')
    if todas_sugerencias:
        data = json.dumps(list(todas_sugerencias), cls=DjangoJSONEncoder)
    else:
        data = serializers.serialize('json', "")
    return HttpResponse(data, content_type="application/json")

def get_suggestions_directory(request):
    todas_sugerencias = Sugerencias.objects.all().values('pk','nombre', 'telefono', 'direccion', 'categoria__nombre', 'usuario__usuario','usuario', 'aplicada', 'fecha','usuario__imagen').order_by('pk')
    if todas_sugerencias:
        data = json.dumps(list(todas_sugerencias), cls=DjangoJSONEncoder)
    else:
        data = serializers.serialize('json', "")
    return HttpResponse(data, content_type="application/json")
def obtener_sugerencias_directorio(request,filtro,orden,texto):
    if orden =="ascendente":
        orden=""
    elif orden=="descendente":
        orden="-"

    if filtro == "nombre":
        orden=orden+"nombre"
    elif filtro == "usuario":
        orden=orden+"usuario__usuario"
    elif filtro == "categoria":
        orden=orden+"categoria__nombre"
    elif filtro == "telefono":
        orden=orden+"telefono"
    elif filtro == "direccion":
        orden=orden+"direccion"
    elif filtro =="aplicada":
        orden=orden+"aplicada"
    elif filtro =="pendiente":
        orden=orden+"aplicada"

    if texto == "___":
        todas_sugerencias = Sugerencias.objects.all().values('pk','nombre', 'telefono', 'direccion', 'categoria__nombre', 'usuario__usuario','usuario', 'aplicada', 'fecha','usuario__imagen').order_by(orden)
    else:
        if filtro == "nombre":
            todas_sugerencias = Sugerencias.objects.filter(nombre__contains=texto).values('pk','nombre', 'telefono', 'direccion', 'categoria__nombre', 'usuario__usuario','usuario', 'aplicada', 'fecha','usuario__imagen').order_by(orden)
        elif filtro =="usuario":
            todas_sugerencias = Sugerencias.objects.filter(usuario__usuario__contains=texto).values('pk','nombre', 'telefono', 'direccion', 'categoria__nombre', 'usuario__usuario','usuario', 'aplicada', 'fecha','usuario__imagen').order_by(orden)
        elif filtro =="categoria":
            todas_sugerencias = Sugerencias.objects.filter(categoria__nombre__contains=texto).values('pk','nombre', 'telefono', 'direccion', 'categoria__nombre', 'usuario__usuario','usuario', 'aplicada', 'fecha','usuario__imagen').order_by(orden)
        elif filtro == "telefono":
            todas_sugerencias = Sugerencias.objects.filter(telefono__contains=texto).values('pk','nombre', 'telefono', 'direccion', 'categoria__nombre', 'usuario__usuario','usuario', 'aplicada', 'fecha','usuario__imagen').order_by(orden)
        elif filtro == "direccion":
            todas_sugerencias = Sugerencias.objects.filter(direccion__contains=texto).values('pk','nombre', 'telefono', 'direccion', 'categoria__nombre', 'usuario__usuario','usuario', 'aplicada', 'fecha','usuario__imagen').order_by(orden)
    if todas_sugerencias:
        data = json.dumps(list(todas_sugerencias), cls=DjangoJSONEncoder)
    else:
        data = serializers.serialize('json', "")
    return HttpResponse(data, content_type="application/json")

def get_debts(request):
    todos_montos=Monto.objects.all().values('pk','usuario__usuario','total','usuario__imagen').order_by('pk')
    if todos_montos:
        data = json.dumps(list(todos_montos), cls=DjangoJSONEncoder)
    else:
        data = serializers.serialize('json', "")
    return HttpResponse(data, content_type="application/json")
def obtener_montos(request,filtro,orden,texto):
    if orden =="ascendente":
        orden=""
    elif orden=="descendente":
        orden="-"

    if filtro == "usuario":
        orden=orden+"usuario__usuario"
    elif filtro == "monto":
        orden=orden+"total"

    if texto == "___":
        todos_montos=Monto.objects.all().values('pk','usuario__usuario','total','usuario__imagen').order_by(orden)
    else:
        if filtro =="usuario":
            todos_montos=Monto.objects.filter(usuario__usuario__contains=texto).values('pk','usuario__usuario','total','usuario__imagen').order_by(orden)
        elif filtro =="monto":
            todos_montos=Monto.objects.filter(total=texto).values('pk','usuario__usuario','total','usuario__imagen').order_by(orden)

    if todos_montos:
        data = json.dumps(list(todos_montos), cls=DjangoJSONEncoder)
    else:
        data = serializers.serialize('json', "")
    return HttpResponse(data, content_type="application/json")

def get_suggestions(request):
    todas_sugerencias = Sugerencias.objects.all().values('pk','nombre', 'telefono', 'direccion', 'categoria__nombre', 'usuario__usuario','usuario', 'aplicada', 'fecha','usuario__imagen').order_by('pk')
    if todas_sugerencias:
        data = json.dumps(list(todas_sugerencias), cls=DjangoJSONEncoder)
    else:
        data = serializers.serialize('json', "")
    return HttpResponse(data, content_type="application/json")
def obtener_sugerencias(request,filtro,orden,texto):
    if orden =="ascendente":
        orden=""
    elif orden=="descendente":
        orden="-"

    if filtro == "nombre":
        orden=orden+"nombre"
    elif filtro == "usuario":
        orden=orden+"usuario__usuario"
    elif filtro == "categoria":
        orden=orden+"categoria__nombre"
    elif filtro == "telefono":
        orden=orden+"telefono"
    elif filtro == "direccion":
        orden=orden+"direccion"
    elif filtro =="aplicada":
        orden=orden+"aplicada"
    elif filtro =="pendiente":
        orden=orden+"aplicada"

    if texto == "___":
        todas_sugerencias = Sugerencias.objects.all().values('pk','nombre', 'telefono', 'direccion', 'categoria__nombre', 'usuario__usuario','usuario', 'aplicada', 'fecha','usuario__imagen').order_by(orden)
    else:
        if filtro == "nombre":
            todas_sugerencias = Sugerencias.objects.filter(nombre__contains=texto).values('pk','nombre', 'telefono', 'direccion', 'categoria__nombre', 'usuario__usuario','usuario', 'aplicada', 'fecha','usuario__imagen').order_by(orden)
        elif filtro =="usuario":
            todas_sugerencias = Sugerencias.objects.filter(usuario__usuario__contains=texto).values('pk','nombre', 'telefono', 'direccion', 'categoria__nombre', 'usuario__usuario','usuario', 'aplicada', 'fecha','usuario__imagen').order_by(orden)
        elif filtro =="categoria":
            todas_sugerencias = Sugerencias.objects.filter(categoria__nombre__contains=texto).values('pk','nombre', 'telefono', 'direccion', 'categoria__nombre', 'usuario__usuario','usuario', 'aplicada', 'fecha','usuario__imagen').order_by(orden)
        elif filtro == "telefono":
            todas_sugerencias = Sugerencias.objects.filter(telefono__contains=texto).values('pk','nombre', 'telefono', 'direccion', 'categoria__nombre', 'usuario__usuario','usuario', 'aplicada', 'fecha','usuario__imagen').order_by(orden)
        elif filtro == "direccion":
            todas_sugerencias = Sugerencias.objects.filter(direccion__contains=texto).values('pk','nombre', 'telefono', 'direccion', 'categoria__nombre', 'usuario__usuario','usuario', 'aplicada', 'fecha','usuario__imagen').order_by(orden)
    if todas_sugerencias:
        data = json.dumps(list(todas_sugerencias), cls=DjangoJSONEncoder)
    else:
        data = serializers.serialize('json', "")
    return HttpResponse(data, content_type="application/json")


def obtener_sugerencias_notificacion(request):
    todas_sugerencias = Sugerencias.objects.filter(aplicada=False).values('pk', 'nombre', 'telefono', 'direccion', 'categoria__nombre',
                                                         'usuario__usuario', 'usuario', 'aplicada', 'fecha')
    if todas_sugerencias:
        data = json.dumps(list(todas_sugerencias), cls=DjangoJSONEncoder)
    else:
        data = serializers.serialize('json', "")
    return HttpResponse(data, content_type="application/json")

def obtener_alertas_pedido(request):
    todas_alertas = Notificacion.objects.filter(estado="Activa",tipo_notificacion="pedido_llego",usuario=Usuario.objects.get(pk=request.session['idusuario'])).values('pk', 'usuario__usuario', 'pedido__descripcion', 'pedido__total')
    if todas_alertas:
        data = json.dumps(list(todas_alertas), cls=DjangoJSONEncoder)
    else:
        data = serializers.serialize('json', "")
    return HttpResponse(data, content_type="application/json")

def notificame_este_pedido(request,idnotificacion):
    if Estoy_Log(request):
        try:
            notificacion_seleccionada=Notificacion.objects.get(pk=idnotificacion)
            if notificacion_seleccionada:
                notificacion_seleccionada.estado="Notificada"
                notificacion_seleccionada.save()
                #ahora creo una notificacion para el que creo ese pedido
                pedido=notificacion_seleccionada.pedido
                id_usuario_genera_pedido=Pedido.objects.filter(pk=pedido.pk).values('usuario__pk')
                """
                ● El usuario que registra el pedido.
                """
                user=Usuario.objects.get(pk=id_usuario_genera_pedido)
                
                Notificacion.objects.create(
                    tipo_notificacion="mi_pedido_llego",
                    usuario=user,
                    pedido=pedido,
                    estado="Activa"
                )
                

                """
                ● El FF del equipo al que pertenezca la persona SI ES DEVELOPER Y SI TIENE EQUIPOS.
                
                if str(user.rol)=='Developer':    
                    #compruebo que el usuario pertenece a un equipo
                    sin_equipo=Equipo.objects.get(nombre="Sin equipo")                    
                    relacion_equipo=equipos_usuarios.objects.get(Q(usuario=user),~Q(equipo=sin_equipo))
                    if relacion_equipo:                        
                        #obtengo el creador de ese equipo                        
                        equipo=relacion_equipo.equipo                        
                        Notificacion.objects.create(
                            tipo_notificacion="mi_pedido_llego",
                            usuario=Usuario.objects.get(usuario=equipo.usuario_creacion),
                            pedido=pedido,
                            estado="Activa"
                        )  
                    else:
                        return HttpResponse("NO hay relacion")
                else:
                    return HttpResponse(str(user.rol)+" NO es developer")
                """
                    

                """
                ● Los administradores del sistema SI NO ES ADMINISTRADOR                  
                """  
                #Consigo todos los admin                   
                """
                admins=Usuario.objects.filter(rol__rol="Administrador").values('pk')       

                for admin in admins:
                    Notificacion.objects.create(
                        tipo_notificacion="pedido_recoger",
                        usuario=Usuario.objects.get(pk=admin['pk']),
                        pedido=pedido
                    )
                """
                return HttpResponse("SI")
            else:
                return HttpResponse("NO")
        except Exception as e:
            return HttpResponse(e)
    return render(request, 'principal/principal.html')

def marcar_como_leida(request,idnotificacion):
    if Estoy_Log(request):
        try:
            notificacion_seleccionada=Notificacion.objects.get(pk=idnotificacion)
            if notificacion_seleccionada:
                notificacion_seleccionada.estado="Leida"
                notificacion_seleccionada.save()
                return HttpResponse("SI")
            else:
                return HttpResponse("NO")
        except Exception as e:
            return HttpResponse(e)
    return render(request, 'principal/principal.html')

def get_my_teams(request, idusuario):  
    if Soy_Admin(request):
        try:
            equipos=equipos_usuarios.objects.filter(usuario=Usuario.objects.get(pk=idusuario)).values('usuario__usuario','equipo__usuario_creacion','equipo__nombre')
            if equipos:
                data = json.dumps(list(equipos), cls=DjangoJSONEncoder)
            else:
                data = serializers.serialize('json', "")
        except Exception as e:
            data=""
        return HttpResponse(data, content_type="application/json")
    return render(request, 'principal/principal.html')

def obtener_notificacion_users_pedido_despachado(request):
    if Estoy_Log(request):
        pedidos_llegados=Notificacion.objects.filter(estado="Activa",tipo_notificacion="pedido_despachado",usuario=Usuario.objects.get(pk=request.session['idusuario'])).values('pk','pedido__pk','usuario__usuario','pedido__descripcion','pedido__total','fecha','hora','contacto__nombre')
        if pedidos_llegados:
            data=json.dumps(list(pedidos_llegados),cls=DjangoJSONEncoder)
        else:
            data=serializers.serialize('json',"")
        return HttpResponse(data,content_type="application/json")

def obtener_notificacion_users_pedido_llego(request):
    if Estoy_Log(request):
        pedidos_llegados=Notificacion.objects.filter(estado="Activa",tipo_notificacion="mi_pedido_llego",usuario=Usuario.objects.get(pk=request.session['idusuario'])).values('pk','pedido__pk','usuario__usuario','pedido__descripcion','pedido__total','fecha','hora')
        if pedidos_llegados:
            data=json.dumps(list(pedidos_llegados),cls=DjangoJSONEncoder)
        else:
            data=serializers.serialize('json',"")
        return HttpResponse(data,content_type="application/json")

def obtener_notificacion_users_casi_cierre_contactos(request):
    if Estoy_Log(request):
        contactos_casi_no_disponibles=Notificacion.objects.filter(estado="Activa",tipo_notificacion="contacto_casi_no_disponible",usuario=Usuario.objects.get(pk=request.session['idusuario'])).values('pk','contacto__pk','usuario__usuario','contacto__nombre','contacto__hasta','fecha','hora')
        if contactos_casi_no_disponibles:
            data=json.dumps(list(contactos_casi_no_disponibles),cls=DjangoJSONEncoder)
        else:
            data=serializers.serialize('json',"")
        return HttpResponse(data,content_type="application/json")

def obtener_notificacion_admins_cierre_contactos(request):
    if Soy_Admin(request):
        contactos_no_disponibles=Notificacion.objects.filter(estado="Activa",tipo_notificacion="contacto_ya_no_disponible",usuario=Usuario.objects.get(pk=request.session['idusuario'])).values('pk','contacto__pk','usuario__usuario','contacto__nombre','contacto__hasta','fecha','hora')
        if contactos_no_disponibles:
            data=json.dumps(list(contactos_no_disponibles),cls=DjangoJSONEncoder)
        else:
            data=serializers.serialize('json',"")
        return HttpResponse(data,content_type="application/json")

def cargar_contactos_libres(request):
    if Estoy_Log(request):
        contactos=Contacto.objects.exclude(id__in=contactos_usuarios.objects.all().values('contacto__pk')).values('pk','nombre')
        if contactos:
            data=json.dumps(list(contactos),cls=DjangoJSONEncoder)
        else:
            data=serializers.serialize('json',"")
        return HttpResponse(data,content_type="application/json")

def obtener_notificacion_admins_pedidos_llegados(request):
    if Soy_Admin(request):
        pedidos_nuevos=Notificacion.objects.filter(estado="Activa",tipo_notificacion="admin_pedido_llego",usuario=Usuario.objects.get(pk=request.session['idusuario'])).values('pk','pedido__pk','usuario__usuario','pedido__descripcion','pedido__total','fecha','hora')
        if pedidos_nuevos:
            data=json.dumps(list(pedidos_nuevos),cls=DjangoJSONEncoder)
        else:
            data=serializers.serialize('json',"")
        return HttpResponse(data,content_type="application/json")

def obtener_notificacion_admins_pedidos_nuevos(request):
    datos=[]
    if Soy_Admin(request):
        pedidos_nuevos=Notificacion.objects.filter(estado="Activa",tipo_notificacion="admin_pedido_nuevo",usuario=Usuario.objects.get(pk=request.session['idusuario'])).values('pk','pedido__pk','pedido__descripcion','pedido__total','fecha','hora')
        if pedidos_nuevos:
            datos=json.dumps(list(pedidos_nuevos),cls=DjangoJSONEncoder)
        else:
            datos=serializers.serialize('json',"")
    return HttpResponse(datos,content_type="application/json")

def obtener_notificaciones_recoger_pedido(request):
    todas_notificaciones = Notificacion.objects.filter(estado="Activa",tipo_notificacion="pedido_recoger",usuario=Usuario.objects.get(pk=request.session['idusuario'])).values('pk', 'usuario__usuario', 'pedido__descripcion', 'pedido__total','fecha','hora')
    if todas_notificaciones:
        data = json.dumps(list(todas_notificaciones), cls=DjangoJSONEncoder)
    else:
        data = serializers.serialize('json', "")
    return HttpResponse(data, content_type="application/json")

from django.utils.timezone import localtime, now
def contarContactosDisponibles(request):
    contactos = Contacto.objects.filter(activo=True,hasta__gte=localtime(now()), desde__lte=localtime(now())).count()
    return HttpResponse(str(contactos))


from datetime import datetime, timedelta
def crear_notificacion_pre_alerta_cierre_contacto(request):
    if Estoy_Log(request):
        try:
            user_logged=Usuario.objects.get(pk=request.session['idusuario'])
            OP=PreferenciaNotificacion.objects.get(usuario=user_logged)
            hora_usuario=OP.horas
            minuto_usuario=OP.minutos
            hora_pre_alerta=localtime(now()) + timedelta(hours=hora_usuario,minutes=minuto_usuario)            
            contactos_casi_no_disponibles = Contacto.objects.filter(Q(activo=True),Q(hasta__contains=hora_pre_alerta.hour) & Q(hasta__contains=hora_pre_alerta.minute)).values('pk', 'nombre')
            if contactos_casi_no_disponibles:
                #Recorro la lista de contactos casi no disponibles
                for contacto_casi_no_disponible in contactos_casi_no_disponibles:
                    #compruebo que la notificacion de este contacto NO EXISTA
                    contacto_actual=Contacto.objects.get(pk=contacto_casi_no_disponible['pk'])
                    notificacion=Notificacion.objects.filter(usuario=user_logged,tipo_notificacion="contacto_casi_no_disponible",contacto=contacto_actual,fecha=localtime(now()))
                    if not notificacion:
                        Notificacion.objects.create(
                            usuario=user_logged,
                            contacto=contacto_actual,
                            tipo_notificacion="contacto_casi_no_disponible",
                            estado="Activa"
                        )
            return HttpResponse("SI")  
        except Exception as e:
            return HttpResponse(e)
    return render(request, 'principal/principal.html')

def crear_notificacion_cierre_contacto(request):
    if Soy_Admin(request):
        try:            
            contactos_no_disponibles = Contacto.objects.filter(Q(activo=True),Q(hasta__contains=localtime(now()).hour) & Q(hasta__contains=localtime(now()).minute)).values('pk', 'nombre')
            user_logged=Usuario.objects.get(pk=request.session['idusuario'])
            if contactos_no_disponibles:
                #Recorro la lista de contactos no disponibles
                for contacto_no_disponible in contactos_no_disponibles:
                    #compruebo que la notificacion de este contacto NO EXISTA
                    contacto_actual=Contacto.objects.get(pk=contacto_no_disponible['pk'])
                    notificacion=Notificacion.objects.filter(usuario=user_logged,tipo_notificacion="contacto_ya_no_disponible",contacto=contacto_actual,fecha=localtime(now()))
                    if not notificacion:
                        Notificacion.objects.create(
                            usuario=user_logged,
                            contacto=contacto_actual,
                            tipo_notificacion="contacto_ya_no_disponible",
                            estado="Activa"
                        )                   
            return HttpResponse("SI")
        except Exception as e:
            return HttpResponse(e)        
    return render(request, 'principal/principal.html')


def obtenerNombrecontactos(request):

    contactos = Contacto.objects.filter(activo=True,hasta__gte=localtime(now()), desde__lte=localtime(now())).values('pk', 'nombre')
    if contactos:
        data = json.dumps(list(contactos), cls=DjangoJSONEncoder)
    else:
        data = serializers.serialize('json', "")
    return HttpResponse(data, content_type="application/json")


def crearRate(request):
    if Estoy_Log(request):
        if request.method == 'GET':
            rating = request.GET['rating']
            usuario = request.GET['usuario']
            contacto = request.GET['contacto']
            try:
                usuario = Usuario.objects.get(pk=usuario)
                contacto = Contacto.objects.get(pk=contacto)
                Rating.objects.create(
                    rating=rating,
                    usuario=usuario,
                    contacto=contacto
                )
                return HttpResponse("SI")
            except Exception as e:
                return HttpResponse("NO")
        else:
            return HttpResponse("Error1")
    return HttpResponse("NO")


def editar_comentario(request):
    if Estoy_Log(request):
        if request.method == 'GET':
            id = request.GET['comentario2']
            comentario = request.GET['comentario']
            try:
                Comentario_modificar = Comentarios.objects.get(pk=id)
                Comentario_modificar.comentario = comentario
                Comentario_modificar.save()
                return HttpResponse("SI")
            except Exception as e:
                return HttpResponse("NO")
        else:
            return HttpResponse("Error1")
    return HttpResponse("NO")


def registrar_comentario(request):
    if Estoy_Log(request):
        if request.method == 'GET':
            comentario = request.GET['comentario']
            usuario = request.GET['usuario']
            contacto = request.GET['contacto']
            try:
                usuario = Usuario.objects.get(pk=usuario)
                contacto = Contacto.objects.get(pk=contacto)
                Comentarios.objects.create(
                    comentario=comentario,
                    usuario=usuario,
                    contacto=contacto
                )
                return HttpResponse("SI")
            except Exception as e:
                return HttpResponse("NO")
        else:
            return HttpResponse("Error1")
    return HttpResponse("NO")



def registro_pedido(request):
    if Estoy_Log(request):
        if request.method == 'POST':
            descripcion = request.POST['descripcion']
            total=request.POST['total']           
            itbis=request.POST['itbis']

            DETALLES = json.loads(request.POST.get('DETALLES'))
            try:
                usuario_objeto = Usuario.objects.get(pk=request.session['idusuario'])
                contacto_objeto = Contacto.objects.get(pk=request.POST['contacto'])
                pedido_crear=Pedido()

                pedido_crear.descripcion=descripcion
                pedido_crear.contacto=contacto_objeto
                pedido_crear.usuario=usuario_objeto
                pedido_crear.total=total 

                if itbis=="true":
                    pedido_crear.itbis=True          
                else:
                    pedido_crear.itbis=False          
                pedido_crear.save()

                #obtener el conteo de ese contacto, para agregarselo a su acumulado               
                conteo=ConteoPorContactos.objects.filter(contacto=contacto_objeto)

                if conteo:
                    #Si ya tiene conteo lo actualizamos
                    conteo=ConteoPorContactos.objects.get(contacto=contacto_objeto)
                    conteo.cantidad=conteo.cantidad+1
                    conteo.save()
                else:
                    #Si no tiene lo creamos
                    ConteoPorContactos.objects.create(
                        contacto=contacto_objeto,
                        cantidad=1
                    )


               
                for item in DETALLES:
                    id_item=item['iditem']
                    cantidad=item['cantidad']
                    precio=item['precio']
                    importe=item['importe']
                    DetallePedido.objects.create(
                        pedido=pedido_crear,
                        item=ItemsMenu.objects.get(pk=item['iditem']),
                        cantidad=item['cantidad'],
                        precio=item['precio'],
                        importe=item['importe']
                    )
                respuesta=1
                #CODIGO NUEVO (ENVIAR UNA NOTIFICACION A LOS ADMIN DE QUE HAY PEDIDOS NUEVOS)
                respuesta2=0
                pedido=pedido_crear
                admins=Usuario.objects.filter(rol=1)

                #NOTIFICO AL USUARIO-CONTACTO que se le hizo un pedido, si es que tiene un usuario
                contacto_usuarios=None
                try:
                    contacto_usuarios=contactos_usuarios.objects.get(contacto=contacto_objeto)
                except ObjectDoesNotExist:
                    contacto_usuarios=None

                if contacto_usuarios:
                    preferencia_usuario_contacto=PreferenciaNotificacion.objects.get(usuario=contacto_usuarios.usuario)
                    if preferencia_usuario_contacto.n_alerta:                    
                        Notificacion.objects.create(
                            tipo_notificacion="pedido_para_contacto",
                            usuario=contacto_usuarios.usuario,
                            pedido=pedido
                        )
                    if preferencia_usuario_contacto.n_correo:
                        try:
                            mail = EmailMultiAlternatives(
                              subject="Pedido nuevo registrado",
                              body='Se ha registrado un Pedido nuevo: '+pedido.descripcion+' Total: '+str(pedido.total),
                              from_email="Reisp Solutions <paradoxproject0@gmail.com>",
                              to=[contacto_usuarios.usuario.correo],
                              headers={"Reply-To": "support@reispsolutions.com"}
                            )
                            mail.attach_alternative('<h1>Se ha registrado un Pedido nuevo: '+pedido.descripcion+' Total: '+str(pedido.total)+'</h1>', "text/html")
                            mail.send()                            
                        except Exception as a:
                            nada=None

                ##Ahora procedo a enviarle la notificacion de pedido a todos los admins
                for admin in admins:
                    usuario=Usuario.objects.get(pk=admin.pk)
                    #Obtener las preferencias de notificacion del usuario actual
                    preferencias=PreferenciaNotificacion.objects.get(usuario=usuario)                    
                    
                    if preferencias.n_alerta:                    
                        Notificacion.objects.create(
                            tipo_notificacion="admin_pedido_nuevo",
                            usuario=usuario,
                            pedido=pedido
                        )
                        respuesta=respuesta+1
                    if preferencias.n_correo:
                        try:
                            mail = EmailMultiAlternatives(
                              subject="Pedido nuevo registrado",
                              body='Se ha registrado un Pedido nuevo: '+pedido.descripcion+' Total: '+str(pedido.total),
                              from_email="Reisp Solutions <paradoxproject0@gmail.com>",
                              to=[usuario.correo],
                              headers={"Reply-To": "support@reispsolutions.com"}
                            )
                            mail.attach_alternative('<h1>Se ha registrado un Pedido nuevo: '+pedido.descripcion+' Total: '+str(pedido.total)+'</h1>', "text/html")
                            mail.send()
                            respuesta2=respuesta2+1
                        except Exception as a:
                            return HttpResponse(a)
                if respuesta>0 or respuesta2 >0:
                    return HttpResponse("SI")
                elif respuesta==0 and respuesta2==0:
                    return HttpResponse("NO")                   
            except Exception as e:
                return HttpResponse(e)
        else:
            return HttpResponse("Error2")
    return render(request, 'principal/principal.html')


def duplicar_pedido(request, idpedido):
    if Estoy_Log(request):
        try:
            usuario_objeto = Usuario.objects.get(pk=request.session['idusuario'])
            pedido_duplicar = Pedido.objects.get(pk=idpedido)
            if pedido_duplicar:
                #CREO EL PEDIDO
                pedido_duplicado=Pedido()

                pedido_duplicado.descripcion=pedido_duplicar.descripcion
                pedido_duplicado.contacto=pedido_duplicar.contacto
                pedido_duplicado.usuario=usuario_objeto
                pedido_duplicado.total=pedido_duplicar.total
                pedido_duplicado.itbis=pedido_duplicar.itbis
                pedido_duplicado.save()

                #obtener el conteo de ese contacto, para agregarselo a su acumulado               
                conteo=ConteoPorContactos.objects.filter(contacto=pedido_duplicar.contacto)

                if conteo:
                    #Si ya tiene conteo lo actualizamos
                    conteo=ConteoPorContactos.objects.get(contacto=pedido_duplicar.contacto)
                    conteo.cantidad=conteo.cantidad+1
                    conteo.save()
                else:
                    #Si no tiene lo creamos
                    ConteoPorContactos.objects.create(
                        contacto=pedido_duplicar.contacto,
                        cantidad=1
                    )

                #AHORA AGREGO SUS DETALLES
                detalles=DetallePedido.objects.filter(pedido=pedido_duplicar).values('item','cantidad','precio','importe')


                for item in detalles:
                    item_b=item['item']
                    cantidad=item['cantidad']
                    precio=item['precio']
                    importe=item['importe']
                    DetallePedido.objects.create(
                        pedido=pedido_duplicado,
                        item=ItemsMenu.objects.get(pk=item_b),
                        cantidad=item['cantidad'],
                        precio=item['precio'],
                        importe=item['importe']
                    )          
                #CODIGO NUEVO (ENVIAR UNA NOTIFICACION A LOS ADMIN DE QUE HAY PEDIDOS NUEVOS)
                respuesta2=0
                pedido=pedido_duplicado
                admins=Usuario.objects.filter(rol=1)

                #NOTIFICO AL USUARIO-CONTACTO que se le hizo un pedido, si es que tiene un usuario
                contacto_usuarios=None
                try:
                    contacto_usuarios=contactos_usuarios.objects.get(contacto=pedido_duplicar.contacto)
                except ObjectDoesNotExist:
                    contacto_usuarios=None

                if contacto_usuarios:
                    preferencia_usuario_contacto=PreferenciaNotificacion.objects.get(usuario=contacto_usuarios.usuario)
                    if preferencia_usuario_contacto.n_alerta:                    
                        Notificacion.objects.create(
                            tipo_notificacion="pedido_para_contacto",
                            usuario=contacto_usuarios.usuario,
                            pedido=pedido
                        )
                    if preferencia_usuario_contacto.n_correo:
                        try:
                            mail = EmailMultiAlternatives(
                              subject="Pedido nuevo registrado",
                              body='Se ha registrado un Pedido nuevo: '+pedido.descripcion+' Total: '+str(pedido.total),
                              from_email="Reisp Solutions <paradoxproject0@gmail.com>",
                              to=[contacto_usuarios.usuario.correo],
                              headers={"Reply-To": "support@reispsolutions.com"}
                            )
                            mail.attach_alternative('<h1>Se ha registrado un Pedido nuevo: '+pedido.descripcion+' Total: '+str(pedido.total)+'</h1>', "text/html")
                            mail.send()                            
                        except Exception as a:
                            nada=None

                
                ##Ahora procedo a enviarle la notificacion de pedido a todos los admins
                for admin in admins:
                    usuario=Usuario.objects.get(pk=admin.pk)
                    #Obtener las preferencias de notificacion del usuario actual
                    preferencias=PreferenciaNotificacion.objects.get(usuario=usuario)                    
                    
                    if preferencias.n_alerta:                    
                        Notificacion.objects.create(
                            tipo_notificacion="admin_pedido_nuevo",
                            usuario=usuario,
                            pedido=pedido
                        )
                    if preferencias.n_correo:
                        try:
                            mail = EmailMultiAlternatives(
                              subject="Pedido nuevo registrado",
                              body='Se ha registrado un Pedido nuevo: '+pedido.descripcion+' Total: '+str(pedido.total),
                              from_email="Reisp Solutions <paradoxproject0@gmail.com>",
                              to=[usuario.correo],
                              headers={"Reply-To": "support@reispsolutions.com"}
                            )
                            mail.attach_alternative('<h1>Se ha registrado un Pedido nuevo: '+pedido.descripcion+' Total: '+str(pedido.total)+'</h1>', "text/html")
                            mail.send()
                        except Exception as a:
                            return HttpResponse(a)
                return HttpResponse("SI")
            else:
                return HttpResponse("NO")
        except Exception as e:
            return HttpResponse(e)
    return render(request, 'principal/principal.html')

def despachar_pedido(request, idpedido):
    if Estoy_Log(request):
        try:
            pedido=Pedido.objects.get(pk=idpedido)

            preferencias=PreferenciaNotificacion.objects.get(usuario=pedido.usuario)
            if preferencias.n_alerta:     
                #Compruebo que el pedido no haya sido despachado
                existe=Notificacion.objects.filter(tipo_notificacion="pedido_despachado",usuario=pedido.usuario,pedido=pedido)
                if not existe:                               
                    Notificacion.objects.create(
                        tipo_notificacion="pedido_despachado",
                        usuario=pedido.usuario,
                        pedido=pedido,
                        contacto=pedido.contacto                        
                    )
            if preferencias.n_correo:
                try:
                    mail = EmailMultiAlternatives(
                      subject="Pedido despachado",
                      body='Se ha despachado su pedido Pedido: '+pedido.descripcion+' Total: '+str(pedido.total),
                      from_email="Reisp Solutions <paradoxproject0@gmail.com>",
                      to=[contacto_usuarios.usuario.correo],
                      headers={"Reply-To": "support@reispsolutions.com"}
                    )
                    mail.attach_alternative('<h1>Se ha despachado su Pedido: '+pedido.descripcion+' Total: '+str(pedido.total)+'</h1>', "text/html")
                    mail.send()                            
                except Exception as a:
                    nada=None
        except Exception as e:
            return HttpResponse("NO")
        return HttpResponse("SI")


def cancelar_pedido(request, idpedido):
    if Estoy_Log(request):
        try:
            pedido_cancelar = Pedido.objects.get(pk=idpedido)
            if pedido_cancelar:
                pedido_cancelar.estado="Cancelado"
                pedido_cancelar.save()
                return HttpResponse("SI")
            else:
                return HttpResponse("NO")
        except Exception as e:
            return  HttpResponse("NO")
    return render(request, 'principal/principal.html')


def recibir_pedido(request, idpedido):
    if Estoy_Log(request):
        try:
            pedido_recibir = Pedido.objects.get(pk=idpedido)
            if pedido_recibir:
                pedido_recibir.estado="Recibido"
                pedido_recibir.save()

                idusuario=pedido_recibir.usuario
                monto=pedido_recibir.total
                #obtener el monto de ese pedido y el usuario, para agregarselo a su acumulado               
                monto_del_usuario=Monto.objects.filter(usuario=pedido_recibir.usuario)

                if monto_del_usuario:
                    #Si ya tiene monto lo actualizamos
                    mdu=Monto.objects.get(usuario=pedido_recibir.usuario)
                    mdu.total=mdu.total+monto
                    mdu.save()
                else:
                    #Si no tiene lo creamos
                    Monto.objects.create(
                        usuario=pedido_recibir.usuario,
                        total=monto
                    )
                ##AHORA PROCEDO A ENVIAR NOTIFICACION A LOS ADMIN DE QUE EL DELIVERY LLEGÓ
                #si soy admin, enviar a todos menos a mi
                #si no lo soy, enviar a todos
                admins=Usuario.objects.filter(rol=1)
                for admin in admins:
                    usuario=Usuario.objects.get(pk=admin.pk)
                    #Obtener las preferencias de notificacion del usuario actual
                    preferencias=PreferenciaNotificacion.objects.get(usuario=usuario)                    
                    
                    if preferencias.n_alerta:                    
                        Notificacion.objects.create(
                            tipo_notificacion="admin_pedido_llego",
                            usuario=usuario,
                            pedido=pedido_recibir
                        )                            
                    if preferencias.n_correo:
                        r=None
                        try:
                            mail = EmailMultiAlternatives(
                              subject="Un pedido ha llegado",
                              body='El pedido '+pedido_recibir.descripcion+' ha llegado,su Total es: '+str(pedido_recibir.total),
                              from_email="Reisp Solutions <paradoxproject0@gmail.com>",
                              to=[usuario.correo],
                              headers={"Reply-To": "support@reispsolutions.com"}
                            )
                            mail.attach_alternative('<h1>El pedido '+pedido_recibir.descripcion+' ha llegado su Total es: '+str(pedido_recibir.total)+'</h1>', "text/html")
                            mail.send()                            
                        except Exception as a:
                            r=a
                return HttpResponse("SI")
            else:
                return HttpResponse("NO")
        except Exception as e:
            return HttpResponse(e)
    return render(request, 'principal/principal.html')




def obtener_usuario_Equipo(request, idequipo):
    if Estoy_Log(request):
        usuarios= equipos_usuarios.objects.filter(equipo=Equipo.objects.get(pk=idequipo)).values('usuario__usuario', 'usuario__pk','usuario__rol', 'equipo__id_usuario','usuario__imagen').order_by('usuario__usuario')
        if usuarios:
            data = json.dumps(list(usuarios), cls=DjangoJSONEncoder)
        else:
            data = serializers.serialize('json', "")
        return HttpResponse(data, content_type="application/json")
    return render(request, 'principal/principal.html')


def get_my_orders(request):
    if Estoy_Log(request):        
        mis_pedidos=Pedido.objects.all().values('pk','descripcion','contacto__nombre','usuario__usuario','estado','fecha','hora','total').filter(usuario=request.session['idusuario']).order_by('pk')
        if mis_pedidos:
            data = json.dumps(list(mis_pedidos), cls=DjangoJSONEncoder)
        else:
            data = serializers.serialize('json', "")
        return HttpResponse(data, content_type="application/json")
    return render(request, 'principal/principal.html')
def Obtenermispedidos(request,filtro,orden,texto):
    if Estoy_Log(request):
        if orden == "ascendente":
            orden = ""
        elif orden == "descendente":
            orden = "-"

        if filtro == "contacto":
            orden=orden+"contacto__nombre"
        elif filtro =="descripcion":
            orden=orden+"descripcion"
        elif filtro =="estado":
            orden=orden+"estado"

        if texto == "___":
            mis_pedidos=Pedido.objects.all().values('pk','descripcion','contacto__nombre','usuario__usuario','estado','fecha','hora','total').filter(usuario=request.session['idusuario']).order_by(orden)
        else:
            if filtro == "contacto":
                mis_pedidos=Pedido.objects.all().values('pk','descripcion','contacto__nombre','usuario__usuario','estado','fecha','hora','total').filter(usuario=request.session['idusuario'],contacto__nombre__contains=texto).order_by(orden)
            elif filtro == "descripcion":
                mis_pedidos=Pedido.objects.all().values('pk','descripcion','contacto__nombre','usuario__usuario','estado','fecha','hora','total').filter(usuario=request.session['idusuario'],descripcion__contains=texto).order_by(orden)
            elif filtro == "estado":
                mis_pedidos=Pedido.objects.all().values('pk','descripcion','contacto__nombre','usuario__usuario','estado','fecha','hora','total').filter(usuario=request.session['idusuario'],estado__contains=texto).order_by(orden)
        if mis_pedidos:
            data = json.dumps(list(mis_pedidos), cls=DjangoJSONEncoder)
        else:
            data = serializers.serialize('json', "")
        return HttpResponse(data, content_type="application/json")
    return render(request, 'principal/principal.html')  

def get_orders(request,id_equipo):
    if Estoy_Log(request):
        usuario=Usuario.objects.get(pk=request.session["idusuario"])
        if str(usuario.rol) == "Administrador":
            if not id_equipo== "NINGUNO":
                listaUsuarios_Id = []
                equipo_objeto=Equipo.objects.get(pk=id_equipo)
                UsersTeamList=equipos_usuarios.objects.filter(equipo=equipo_objeto).values('usuario__pk')
                for usuario in UsersTeamList:
                    listaUsuarios_Id.append(usuario['usuario__pk'])
                todos_pedidos=Pedido.objects.filter(Q(usuario__in=listaUsuarios_Id)).values('usuario__direccion','pk', 'descripcion', 'contacto__nombre', 'usuario__usuario', 'estado', 'fecha', 'hora','total','usuario__imagen').order_by('-pk')
            else:
                todos_pedidos =Pedido.objects.all().values('usuario__direccion','pk', 'descripcion', 'contacto__nombre', 'usuario__usuario', 'estado', 'fecha', 'hora','total','usuario__imagen').order_by('-pk')  
        elif str(usuario.rol) == "Firefighter":
            listaUsuarios_Id = [] #listaUsuarios_Id = [request.session["idusuario"]]Agrego al arreglo, el id del usuario Firefighter
            #Busco los equipos del Firefighter
            equiposObjetos = Equipo.objects.filter(id_usuario=request.session["idusuario"])
            for equipo in equiposObjetos:
                #codigo nuevo
                UsersTeamList=equipos_usuarios.objects.filter(equipo=Equipo.objects.get(pk=equipo.pk)).values('usuario__pk')
                for usuario in UsersTeamList:
                    listaUsuarios_Id.append(usuario['usuario__pk'])
            todos_pedidos =Pedido.objects.filter(Q(usuario__in=listaUsuarios_Id)).values('usuario__direccion','pk', 'descripcion', 'contacto__nombre', 'usuario__usuario', 'estado', 'fecha', 'hora','total','usuario__imagen','usuario__rol').order_by('-pk')                                        
        elif str(usuario.rol) == "Developer":
            todos_pedidos =Pedido.objects.filter(usuario=usuario).values('usuario__direccion','pk', 'descripcion', 'contacto__nombre', 'usuario__usuario', 'estado', 'fecha', 'hora','total','usuario__imagen').order_by('-pk')
        elif str(usuario.rol) == "Invitado":
            todos_pedidos =Pedido.objects.filter(usuario=usuario).values('usuario__direccion','pk', 'descripcion', 'contacto__nombre', 'usuario__usuario', 'estado', 'fecha', 'hora','total','usuario__imagen').order_by('-pk')
        elif str(usuario.rol) == "Contacto":
            from django.utils.timezone import localtime, now
            try:
                contacto_usuario=Contacto.objects.get(Q(activo=True),Q(pk=contactos_usuarios.objects.filter(usuario=Usuario.objects.get(pk=request.session['idusuario'])).values('contacto__pk')))
            except ObjectDoesNotExist:
                contacto_usuario = None            
            todos_pedidos=None
            if contacto_usuario:                           
                todos_pedidos=Pedido.objects.filter(estado="Activo",contacto=contacto_usuario).values('pk', 'descripcion', 'contacto__nombre', 'usuario__usuario', 'estado', 'fecha', 'hora','total','usuario__imagen','usuario__direccion').order_by('-pk')
        if todos_pedidos:
            data = json.dumps(list(todos_pedidos), cls=DjangoJSONEncoder)
        else:
            data = serializers.serialize('json', "")
        return HttpResponse(data, content_type="application/json")
    return render(request, 'principal/principal.html')


def obtener_pedidos(request, filtro, orden, texto):
    if Estoy_Log(request):
        usuario=Usuario.objects.get(pk=request.session["idusuario"])
        if str(usuario.rol) == "Administrador":            
            if orden == "ascendente":
                orden = ""
            elif orden == "descendente":
                orden="-"

            if filtro == "contacto":
                orden=orden+"contacto__nombre"
            elif filtro== "descripcion":
                orden=orden+"descripcion"
            elif filtro == "estado":
                orden=orden+"estado"
            elif filtro =="usuario":
                orden=orden+"usuario__usuario"
            elif filtro =="equipo":
                orden=orden+"pk"
            elif filtro =="fecha":
                orden=orden+"fecha"

            if texto == "___":
                todos_pedidos =Pedido.objects.all().values('pk', 'descripcion', 'contacto__nombre', 'usuario__usuario', 'estado', 'fecha', 'hora','total','usuario__imagen').order_by('-pk',orden)
            else:
                if filtro =="contacto":
                    todos_pedidos=Pedido.objects.all().values('pk', 'descripcion', 'contacto__nombre', 'usuario__usuario', 'estado', 'fecha', 'hora','total','usuario__imagen').filter(contacto__nombre__contains=texto).order_by('-pk',orden)
                elif filtro == "descripcion":
                    todos_pedidos=Pedido.objects.all().values('pk', 'descripcion', 'contacto__nombre', 'usuario__usuario', 'estado', 'fecha', 'hora','total','usuario__imagen').filter(descripcion__contains=texto).order_by('-pk',orden)
                elif filtro == "estado":
                    todos_pedidos=Pedido.objects.all().values('pk', 'descripcion', 'contacto__nombre', 'usuario__usuario', 'estado', 'fecha', 'hora','total','usuario__imagen').filter(estado__contains=texto).order_by('-pk',orden)
                elif filtro == "usuario":
                    todos_pedidos=Pedido.objects.all().values('pk', 'descripcion', 'contacto__nombre', 'usuario__usuario', 'estado', 'fecha', 'hora','total','usuario__imagen').filter(usuario__usuario__contains=texto).order_by('-pk',orden)
                elif filtro =="equipo":
                    listaUsuarios_Id = []
                    equipo_objeto=Equipo.objects.get(pk=texto)
                    UsersTeamList=equipos_usuarios.objects.filter(equipo=equipo_objeto).values('usuario__pk')
                    for usuario in UsersTeamList:
                        listaUsuarios_Id.append(usuario['usuario__pk'])
                    todos_pedidos=Pedido.objects.filter(Q(usuario__in=listaUsuarios_Id)).values('pk', 'descripcion', 'contacto__nombre', 'usuario__usuario', 'estado', 'fecha', 'hora','total','usuario__imagen').order_by(orden)
                elif filtro == "fecha":
                    todos_pedidos=Pedido.objects.all().values('pk', 'descripcion', 'contacto__nombre', 'usuario__usuario', 'estado', 'fecha', 'hora','total','usuario__imagen').filter(fecha=texto).order_by('-pk',orden)
                
        elif str(usuario.rol) == "Firefighter":
            listaUsuarios_Id = [] #listaUsuarios_Id = [request.session["idusuario"]]Agrego al arreglo, el id del usuario Firefighter
            #Busco los equipos del Firefighter
            equiposObjetos = Equipo.objects.filter(id_usuario=request.session["idusuario"])
            for equipo in equiposObjetos:
                #codigo nuevo
                UsersTeamList=equipos_usuarios.objects.filter(equipo=Equipo.objects.get(pk=equipo.pk)).values('usuario__pk')
                for usuario in UsersTeamList:
                    listaUsuarios_Id.append(usuario['usuario__pk'])
                       
            if orden == "ascendente":
                orden = ""
            elif orden == "descendente":
                orden="-"

            if filtro == "contacto":
                orden=orden+"contacto__nombre"
            elif filtro== "descripcion":
                orden=orden+"descripcion"
            elif filtro == "estado":
                orden=orden+"estado"
            elif filtro =="usuario":
                orden=orden+"usuario__usuario"

            if texto == "___":
                todos_pedidos =Pedido.objects.filter(Q(usuario__in=listaUsuarios_Id)).values('pk', 'descripcion', 'contacto__nombre', 'usuario__usuario', 'estado', 'fecha', 'hora','total','usuario__imagen','usuario__rol').order_by('-pk',orden)                            
            else:
                if filtro =="contacto":
                    todos_pedidos=Pedido.objects.filter(Q(usuario__in=listaUsuarios_Id),contacto__nombre__contains=texto).values('pk', 'descripcion', 'contacto__nombre', 'usuario__usuario', 'estado', 'fecha', 'hora','total','usuario__imagen','usuario__rol').order_by('-pk',orden)
                elif filtro == "descripcion":
                    todos_pedidos=Pedido.objects.filter(Q(usuario__in=listaUsuarios_Id),descripcion__contains=texto).values('pk', 'descripcion', 'contacto__nombre', 'usuario__usuario', 'estado', 'fecha', 'hora','total','usuario__imagen','usuario__rol').order_by('-pk',orden)
                elif filtro == "estado":
                    todos_pedidos=Pedido.objects.filter(Q(usuario__in=listaUsuarios_Id),estado__contains=texto).values('pk', 'descripcion', 'contacto__nombre', 'usuario__usuario', 'estado', 'fecha', 'hora','total','usuario__imagen','usuario__rol').order_by('-pk',orden)
                elif filtro == "usuario":
                    todos_pedidos=Pedido.objects.filter(Q(usuario__in=listaUsuarios_Id),usuario__usuario__contains=texto).values('pk', 'descripcion', 'contacto__nombre', 'usuario__usuario', 'estado', 'fecha', 'hora','total','usuario__imagen','usuario__rol').order_by('-pk',orden)
                

        elif str(usuario.rol) == "Developer":
            if orden == "ascendente":
                orden = ""
            elif orden == "descendente":
                orden="-"

            if filtro == "contacto":
                orden=orden+"contacto__nombre"
            elif filtro== "descripcion":
                orden=orden+"descripcion"
            elif filtro == "estado":
                orden=orden+"estado"

            if texto == "___":
                todos_pedidos =Pedido.objects.filter(usuario=usuario).values('pk', 'descripcion', 'contacto__nombre', 'usuario__usuario', 'estado', 'fecha', 'hora','total','usuario__imagen').order_by('-pk',orden)
            else:
                if filtro =="contacto":
                    todos_pedidos=Pedido.objects.filter(usuario=usuario,contacto__nombre__contains=texto).values('pk', 'descripcion', 'contacto__nombre', 'usuario__usuario', 'estado', 'fecha', 'hora','total','usuario__imagen').order_by('-pk',orden)
                elif filtro == "descripcion":
                    todos_pedidos=Pedido.objects.filter(usuario=usuario,descripcion__contains=texto).values('pk', 'descripcion', 'contacto__nombre', 'usuario__usuario', 'estado', 'fecha', 'hora','total','usuario__imagen').order_by('-pk',orden)
                elif filtro == "estado":
                    todos_pedidos=Pedido.objects.filter(usuario=usuario,estado__contains=texto).values('pk', 'descripcion', 'contacto__nombre', 'usuario__usuario', 'estado', 'fecha', 'hora','total','usuario__imagen').order_by('-pk',orden)
        
        elif str(usuario.rol) == "Invitado":
            if orden == "ascendente":
                orden = ""
            elif orden == "descendente":
                orden="-"

            if filtro == "contacto":
                orden=orden+"contacto__nombre"
            elif filtro== "descripcion":
                orden=orden+"descripcion"
            elif filtro == "estado":
                orden=orden+"estado"

            if texto == "___":
                todos_pedidos =Pedido.objects.filter(usuario=usuario).values('pk', 'descripcion', 'contacto__nombre', 'usuario__usuario', 'estado', 'fecha', 'hora','total','usuario__imagen').order_by('-pk',orden)
            else:
                if filtro =="contacto":
                    todos_pedidos=Pedido.objects.filter(usuario=usuario,contacto__nombre__contains=texto).values('pk', 'descripcion', 'contacto__nombre', 'usuario__usuario', 'estado', 'fecha', 'hora','total','usuario__imagen').order_by('-pk',orden)
                elif filtro == "descripcion":
                    todos_pedidos=Pedido.objects.filter(usuario=usuario,descripcion__contains=texto).values('pk', 'descripcion', 'contacto__nombre', 'usuario__usuario', 'estado', 'fecha', 'hora','total','usuario__imagen').order_by('-pk',orden)
                elif filtro == "estado":
                    todos_pedidos=Pedido.objects.filter(usuario=usuario,estado__contains=texto).values('pk', 'descripcion', 'contacto__nombre', 'usuario__usuario', 'estado', 'fecha', 'hora','total','usuario__imagen').order_by('-pk',orden)
        elif str(usuario.rol) == "Contacto":
            if orden == "ascendente":
                orden = ""
            elif orden == "descendente":
                orden="-"

            if filtro == "contacto":
                orden=orden+"contacto__nombre"
            elif filtro== "descripcion":
                orden=orden+"descripcion"
            elif filtro == "estado":
                orden=orden+"estado"
            
            from django.utils.timezone import localtime, now

            try:
                contacto_usuario=Contacto.objects.get(Q(activo=True),~Q(hasta__gte=localtime(now()), desde__lte=localtime(now())),Q(pk=contactos_usuarios.objects.filter(usuario=Usuario.objects.get(pk=request.session['idusuario'])).values('contacto__pk')))
            except ObjectDoesNotExist:
                contacto_usuario = None            
            todos_pedidos=None
            if contacto_usuario:
                           
                if texto == "___":
                    todos_pedidos=Pedido.objects.filter(estado="Activo",contacto=contacto_usuario).values('pk', 'descripcion', 'contacto__nombre', 'usuario__usuario', 'estado', 'fecha', 'hora','total','usuario__imagen').order_by('-pk',orden)
                else:
                    if filtro =="contacto":
                        todos_pedidos=Pedido.objects.filter(estado="Activo",contacto=contacto_usuario,contacto__nombre__contains=texto).values('pk', 'descripcion', 'contacto__nombre', 'usuario__usuario', 'estado', 'fecha', 'hora','total','usuario__imagen').order_by('-pk',orden)
                    elif filtro == "descripcion":
                        todos_pedidos=Pedido.objects.filter(estado="Activo",contacto=contacto_usuario,descripcion__contains=texto).values('pk', 'descripcion', 'contacto__nombre', 'usuario__usuario', 'estado', 'fecha', 'hora','total','usuario__imagen').order_by('-pk',orden)
                    elif filtro == "estado":
                        todos_pedidos=Pedido.objects.filter(estado="Activo",contacto=contacto_usuario,estado__contains=texto).values('pk', 'descripcion', 'contacto__nombre', 'usuario__usuario', 'estado', 'fecha', 'hora','total','usuario__imagen').order_by('-pk',orden)
        


        if todos_pedidos:
            data = json.dumps(list(todos_pedidos), cls=DjangoJSONEncoder)
        else:
            data = serializers.serialize('json', "")
        return HttpResponse(data, content_type="application/json")
        
    return render(request, 'principal/principal.html')


def recibir_pedidos_automaticos(request):
    if Estoy_Log(request):
        today = datetime.datetime.today()
        yesterday = today - datetime.timedelta(days=1)
        Pedidos = Pedido.objects.filter(estado="Activo", fecha__range=["2000-01-01", yesterday])
                    
        if Pedidos:
            n = 0
            for p in Pedidos:
                try:                    
                    Ob = Pedido.objects.get(pk=p.id)
                    Ob.estado = "Recibido"
                    Ob.save()

                    idusuario=Ob.usuario
                    monto=Ob.total
                    #obtener el monto de ese pedido y el usuario, para agregarselo a su acumulado
                    

                    monto_del_usuario=Monto.objects.filter(usuario=Ob.usuario)

                    if monto_del_usuario:
                        #Si ya tiene monto lo actualizamos
                        mdu=Monto.objects.get(usuario=Ob.usuario)
                        mdu.total=mdu.total+monto
                        mdu.save()
                    else:
                        #Si no tiene lo creamos
                        Monto.objects.create(
                            usuario=Ob.usuario,
                            total=monto
                        )
                    ##AHORA PROCEDO A ENVIAR NOTIFICACION A LOS ADMIN DE QUE EL DELIVERY LLEGÓ
                    
                    admins=Usuario.objects.filter(rol=1)
                    for admin in admins:
                        usuario=Usuario.objects.get(pk=admin.pk)
                        #Obtener las preferencias de notificacion del usuario actual
                        preferencias=PreferenciaNotificacion.objects.get(usuario=usuario)                    
                        
                        if preferencias.n_alerta:                    
                            Notificacion.objects.create(
                                tipo_notificacion="admin_pedido_llego",
                                usuario=usuario,
                                pedido=Ob
                            )                            
                        if preferencias.n_correo:
                            try:
                                mail = EmailMultiAlternatives(
                                  subject="Un pedido ha llegado",
                                  body='El pedido '+Ob.descripcion+' ha llegado,su Total es: '+str(Ob.total),
                                  from_email="Reisp Solutions <paradoxproject0@gmail.com>",
                                  to=[usuario.correo],
                                  headers={"Reply-To": "support@reispsolutions.com"}
                                )
                                mail.attach_alternative('<h1>El pedido '+Ob.descripcion+' ha llegado su Total es: '+str(Ob.total)+'</h1>', "text/html")
                                mail.send()                                
                            except Exception as a:
                                return HttpResponse("NO")

                    n += 1
                except Exception as e:
                    return HttpResponse(e)
            return HttpResponse("SI")
        else:
            return HttpResponse("NO")
        
    return HttpResponse("NO")




def equipos(request):
    if Estoy_Log(request):
        sesion = {}
        sesion = {
            'idusuario': request.session['idusuario'],
            'usuario': request.session['usuario'],
            'rol': request.session['rol']
        }
        preferencia=None
        if not request.session['idusuario'] is None:
            preferencia=PreferenciaNotificacion.objects.filter(usuario=Usuario.objects.get(pk=request.session['idusuario'])).values('n_alerta','n_correo','n_sonido')
        contexto = {
            'preferencias':preferencia,
            'sesion': sesion,
            'menu': 'SI',
            'es_admin': Soy_Admin(request)
        }
        return render(request, 'equipos/equipos.html', contexto)
    return render(request, 'principal/index.html')

def menus(request):
    if Estoy_Log(request):
        sesion = {}
        sesion = {
            'idusuario': request.session['idusuario'],
            'usuario': request.session['usuario'],
            'rol': request.session['rol']
        }
        preferencia=None
        if not request.session['idusuario'] is None:
            preferencia=PreferenciaNotificacion.objects.filter(usuario=Usuario.objects.get(pk=request.session['idusuario'])).values('n_alerta','n_correo','n_sonido')
        contexto = {
            'preferencias':preferencia,
            'menus':Menu.objects.all().values('pk','nombre','contacto__nombre'),
            'sesion': sesion,
            'menu': 'SI',
            'es_admin': Soy_Admin(request)
        }
        return render(request, 'contacto/menus.html', contexto)
    return render(request, 'principal/index.html')


def mispedidos(request):
    if Estoy_Log(request):
        sesion = {}
        sesion = {
            'idusuario': request.session['idusuario'],
            'usuario': request.session['usuario'],
            'rol': request.session['rol']
        }
        preferencia=None
        if not request.session['idusuario'] is None:
            preferencia=PreferenciaNotificacion.objects.filter(usuario=Usuario.objects.get(pk=request.session['idusuario'])).values('n_alerta','n_correo','n_sonido')
        contexto = {
            'preferencias':preferencia,
            'sesion': sesion,
            'menu': 'SI',
            'es_admin': Soy_Admin(request)
        }
        return render(request, 'pedido/mispedidos.html', contexto)
    return render(request, 'principal/index.html')

def pedidos(request):
    if Estoy_Log(request):
        sesion = {}
        sesion = {
            'idusuario': request.session['idusuario'],
            'usuario': request.session['usuario'],
            'rol': request.session['rol']
        }
        preferencia=None
        if not request.session['idusuario'] is None:
            preferencia=PreferenciaNotificacion.objects.filter(usuario=Usuario.objects.get(pk=request.session['idusuario'])).values('n_alerta','n_correo','n_sonido')
        contexto = {
            'preferencias':preferencia,
            'sesion': sesion,
            'Equipos':Equipo.objects.all(),#MANDO TODOS LOS EQUIPOS PARA EL FILTRO
            'menu': 'SI',
            'es_admin': Soy_Admin(request),
            'es_contacto':Soy_Contacto(request)
        }
        return render(request, 'pedido/pedidos.html', contexto)
    return render(request, 'principal/index.html')

def contacto(request):
    if Estoy_Log(request):
        sesion = {}
        sesion = {
            'idusuario': request.session['idusuario'],
            'usuario': request.session['usuario'],
            'rol': request.session['rol']
        }
        contexto = {}
        preferencia=None
        if not request.session['idusuario'] is None:
            preferencia=PreferenciaNotificacion.objects.filter(usuario=Usuario.objects.get(pk=request.session['idusuario'])).values('n_alerta','n_correo','n_sonido')
        contexto = {
            'preferencias':preferencia,
            'sesion': sesion,
            'menu': 'SI',
            'contactos': Contacto.objects.all(),
            'es_admin': Soy_Admin(request)
        }
        return render(request, 'contacto/contacto.html', contexto)
    return render(request, 'principal/index.html')


def sugerencia(request):
    if Estoy_Log(request):
        sesion = {}
        sesion = {
            'idusuario': request.session['idusuario'],
            'usuario': request.session['usuario'],
            'rol': request.session['rol']
        }
        contexto = {}
        preferencia=None
        if not request.session['idusuario'] is None:
            preferencia=PreferenciaNotificacion.objects.filter(usuario=Usuario.objects.get(pk=request.session['idusuario'])).values('n_alerta','n_correo','n_sonido')
        contexto = {
            'preferencias':preferencia,
            'sesion': sesion,
            'menu': 'SI',
            'sugerencias': Sugerencias.objects.all(),
            'categorias': Categoria.objects.all(),
            'es_admin': Soy_Admin(request)
        }
        return render(request, 'contacto/sugerencias.html', contexto)
    return render(request, 'principal/index.html')


def admin_sugerencia(request):
    if Estoy_Log(request):
        sesion = {}
        sesion = {
            'idusuario': request.session['idusuario'],
            'usuario': request.session['usuario'],
            'rol': request.session['rol']
        }
        contexto = {}
        preferencia=None
        if not request.session['idusuario'] is None:
            preferencia=PreferenciaNotificacion.objects.filter(usuario=Usuario.objects.get(pk=request.session['idusuario'])).values('n_alerta','n_correo','n_sonido')
        contexto = {
            'preferencias':preferencia,
            'sesion': sesion,
            'menu': 'SI',
            'sugerencias': Sugerencias.objects.all(),
            'es_admin': Soy_Admin(request)
        }
        return render(request, 'contacto/admin_sugerencia.html', contexto)
    return render(request, 'principal/index.html')



import os
from django.conf import settings
from django.http import HttpResponse
from django.template import Context
from django.template.loader import get_template
import datetime
from xhtml2pdf import pisa 


def reporte_monto_usuario(request, idusuario):
    try:
        usuario=Usuario.objects.get(pk=idusuario)
        importante=Monto.objects.filter(usuario=usuario).values('total','usuario__usuario')
        contexto = {}
        contexto = {
            'importante':importante,            
        }
        return render(request, 'principal/graficos/line_basic.html', contexto)
    except Exception as e:
        return HttpResponse("Ocurrió un error al generar el reporte"+str(e))

def reporte_mis_pedidos(request):
    
    importante = {}   
    
    importante['contactos']=Contacto.objects.all().values('pk', 'nombre', 'telefono','direccion', 'categoria__nombre', 'usuario__usuario','activo').annotate(average_rating=Avg('rating__rating'))

    c_c=0
    for cada_contacto in importante['contactos']:
        #CARGO TODOS LOS PEDIDOS DE ESE CONTACTO
        importante['contactos'][c_c]['pedidos']=Pedido.objects.filter(usuario=Usuario.objects.get(pk=request.session['idusuario']),contacto=Contacto.objects.get(pk=importante['contactos'][c_c]['pk'])).values('pk','descripcion', 'contacto__nombre','itbis','usuario__usuario', 'estado','fecha', 'hora','total')
        c_d=0
        for cada_pedido in importante['contactos'][c_c]['pedidos']:
            importante['contactos'][c_c]['pedidos'][c_d]['detalles']=DetallePedido.objects.filter(pedido=Pedido.objects.get(pk=importante['contactos'][c_c]['pedidos'][c_d]['pk'])).values('item__nombre','cantidad','precio','importe')
            c_d=c_d+1
        #Incremento el contador
        c_c=c_c+1
    return render(request, 'principal/reporte/rpt_pedidos_de_contacto.html', importante) 

def reporte_total_pedidos(request):
    
    importante = {}   
    
    importante['contactos']=Contacto.objects.all().values('pk', 'nombre', 'telefono','direccion', 'categoria__nombre', 'usuario__usuario','activo').annotate(average_rating=Avg('rating__rating'))

    c_c=0
    for cada_contacto in importante['contactos']:
        #CARGO TODOS LOS PEDIDOS DE ESE CONTACTO
        importante['contactos'][c_c]['pedidos']=Pedido.objects.filter(contacto=Contacto.objects.get(pk=importante['contactos'][c_c]['pk'])).values('pk','descripcion', 'contacto__nombre', 'usuario__usuario','itbis', 'estado','fecha', 'hora','total')
        c_d=0
        for cada_pedido in importante['contactos'][c_c]['pedidos']:
            importante['contactos'][c_c]['pedidos'][c_d]['detalles']=DetallePedido.objects.filter(pedido=Pedido.objects.get(pk=importante['contactos'][c_c]['pedidos'][c_d]['pk'])).values('item__nombre','cantidad','precio','importe')
            c_d=c_d+1
        #Incremento el contador
        c_c=c_c+1
    return render(request, 'principal/reporte/rpt_pedidos_de_contacto.html', importante)  

def reporte_pedido_contacto(request,idcontacto):
    importante = {}   
    
    importante['contactos']=Contacto.objects.filter(pk=idcontacto).values('pk', 'nombre', 'telefono','direccion', 'categoria__nombre', 'usuario__usuario','activo').annotate(average_rating=Avg('rating__rating'))

    c_c=0
    for cada_contacto in importante['contactos']:
        #CARGO TODOS LOS PEDIDOS DE ESE CONTACTO
        importante['contactos'][c_c]['pedidos']=Pedido.objects.filter(contacto=Contacto.objects.get(pk=importante['contactos'][c_c]['pk'])).values('pk','descripcion','itbis', 'contacto__nombre', 'usuario__usuario', 'estado','fecha','itbis', 'hora','total')
        c_d=0
        for cada_pedido in importante['contactos'][c_c]['pedidos']:
            importante['contactos'][c_c]['pedidos'][c_d]['detalles']=DetallePedido.objects.filter(pedido=Pedido.objects.get(pk=importante['contactos'][c_c]['pedidos'][c_d]['pk'])).values('item__nombre','cantidad','precio','importe')
            c_d=c_d+1
        #Incremento el contador
        c_c=c_c+1

    return render(request, 'principal/reporte/rpt_pedidos_de_contacto.html', importante)  

def reporte_pedido(request, idpedido):
    importante = {}       
    importante['pedido']=Pedido.objects.filter(pk=idpedido).values('pk','descripcion', 'contacto__nombre', 'usuario__usuario','itbis', 'estado','fecha', 'hora','total')
    importante['detalles']=DetallePedido.objects.filter(pedido=Pedido.objects.get(pk=idpedido)).values('item__nombre','cantidad','precio','importe')
    return render(request,'principal/reporte/reporte_pedido.html',importante)
    
    
