from fabric.api import *
import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Hosts to deploy onto
env.hosts = ['127.0.0.1', '10.0.0.10']

# Where your project code lives on the server
env.project_root = BASE_DIR+'/proyecto'

def deploy_static():
    with cd(env.project_root):
        run('./manage.py collectstatic -v0 --noinput')

def hello():
    print("Hello world!")