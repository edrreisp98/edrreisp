
from django.conf.urls import url, include
from django.contrib import admin

from django.conf import settings
from django.conf.urls.static import static


from django.contrib.staticfiles.views import serve 

urlpatterns = [
    # URL que me dirige al panel de administracion
    url(r'^admin/', admin.site.urls),

    # URL que me dirige a la pagina principal
    url(r'', include('principal.urls')),

    # URL que me dirige a la pagina administracion
    url(r'administracion/', include('administracion.urls')),

] 
if settings.DEBUG:
	urlpatterns+=static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
	urlpatterns+=static(settings.STATIC_URL,document_root=settings.STATIC_ROOT) 

