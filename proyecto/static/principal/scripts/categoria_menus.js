$(document).on('ready',function(){
	$('#contenedorFormulario').hide();


	$('#btnCancelar').on('click',function(){
        $('#spinner_registro_usuario').html('');
        $('#btnAccion').html('Guardar');
        $('#btnAccion').addClass('btn-success');
        $('#btnAccion').removeClass('btn-warning');
        limpiar();
    });

    var n=0;
    $('#abrirForm').on('click',function(){
        if(n%2==0){
            $('#contenedorFormulario').fadeIn();
            $(this).html('Cerrar');
            $(this).removeClass('btn-success');
            $(this).addClass('btn-primary');
            $('#btnCancelar').click();
        }else{
            $('#contenedorFormulario').fadeOut();
            $(this).html('Agregar Nueva');
            $(this).removeClass('btn-primary');
            $(this).addClass('btn-success');
            $('#btnCancelar').click();
        }
        n++;
    });

    limpiar = function(){
        $('#categoria').val('');
        $('#divCategoria').removeClass('has-error');        
    };

    borrar_categoria_menu = function(id){
        alertify.set({ buttonReverse: true });
        alertify.set({ labels: {
            ok     : "Si",
            cancel : "No"
        } });
        alertify.confirm("Está seguro de eliminar esta categoría?", function (e) {
            if (e) {
                $('#spinner_registro_usuario').html('<h4>Eliminando</h4><i class="fa fa-spinner fa-spin fa-3x"></i>');
                $.ajax(
                    {
                        type:'GET',
                        url:'/eliminar_categoria_menu/'+id
                    }
                )
                .done(function(respuesta){
                    if(respuesta=="SI"){
                        alertify.log("Categoría de menú eliminada");
                        mostrar();
                    }else if(respuesta=="NO"){
                        alertify.error("Ocurrió un error al eliminar la categoría");
                    }else{
                        alertify.error("Error: "+respuesta)
                    }
                    $('#spinner_registro_usuario').html('');
                });
            }
        });
    };

    $('#form_registro_categorias_menu').on('submit',function(e){
		e.preventDefault();
        var texto = $('#btnAccion').html();
        var url="/registro_categoria_menu";
        if(texto!="Guardar"){
        	url="modifica_categoria_menu";
        }
        if($('#categoria').val()!=""){
        	$('#divCategoria').removeClass('has-error');
        	$.ajax({
        		type:'POST',
        		url:url,
        		data:
        		{
        			idcategoria:$('#categoria2').val(),
        			categoria:$('#categoria').val(),
        			csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
        		},
                beforeSend:function()
                {
                    $('#spinner_registro_usuario').html('<i class="fa fa-spinner fa-spin fa-3x"></i>');
                }
        	}).done(function(respuesta){
        		$('#spinner_registro_usuario').html('');
                if(respuesta=="SI"){
                    if(texto=="Guardar"){
                        alertify.log('Categoría de menú registrada');
                    }else{
                        alertify.log('Categoría de menú editada');
                    }                    
                    limpiar();
                    mostrar();
                    $('#contenedorFormulario').fadeOut();
                    $('#abrirForm').html('Agregar Nueva');
                    $('#abrirForm').removeClass('btn-primary');
                    $('#abrirForm').addClass('btn-success');
                    n++;
                }else if(respuesta=="Error1"){
                    if(texto=="Guardar"){
                        alertify.error('Ocurrió un error en la creación de la categoría');
                    }else{
                        alertify.error('Ocurrió un error en la edición de la categoría');
                    }                     
                }else if(respuesta=="Error2"){
                    alertify.error('Datos enviados por el método incorrecto');
                }else{
                    alertify.error("Error: "+respuesta)
                }
        	});
        }else{
        	var errores="";
            if($('#categoria').val()==""){
                $('#divCategoria').addClass('has-error');
                if(errores!=""){errores+="<br> • Debe introducir la Categoría del Menú";}
                else
                    {errores+="• Debe introducir la Categoría del Menú";}
            }else{
                $('#divCategoria').removeClass('has-error');
            }
            if(errores!=""){
                alertify.error(errores);
            }            
        }
    }); 

    modificar_categoria_menu = function(id){
        $('#divCategoria').removeClass('has-error');
        $('#spinner_registro_usuario').html('<h4>Obteniendo datos</h4><i class="fa fa-spinner fa-spin fa-3x"></i>');
        $.ajax(
            {
                type:'GET',
                url:'/obtener_datos_categoria_menu/'+id,
                success:function(data)
                {
                    for(var i=0;i<data.length;i++){
                        $('#categoria').val(data[i]['fields']['nombre']);
                        $('#categoria2').val(data[i]['pk']);
                    }
                    $('#spinner_registro_usuario').html('');
                }
            }
        );
        $('#btnAccion').html('Guardar Cambios');
        $('#btnAccion').removeClass('btn-success');
        $('#btnAccion').addClass('btn-warning');
        
        $('#contenedorFormulario').fadeIn();
        $('#abrirForm').html('Cerrar');
        $('#abrirForm').removeClass('btn-success');
        $('#abrirForm').addClass('btn-primary');
        n++;
    };

    mostrar=function(){
        $.ajax({
            type:'GET',
            url:'/get_categories_menu'
        }).done(function(respuesta){
            var DATA=[];
            for(var i=0;i<respuesta.length;i++){
                 
                fila={};
                fila[0]=respuesta[i]['pk'];
                fila[1]=respuesta[i]['nombre'];
                fila[2]="<a href='#' onclick='modificar_categoria_menu("+respuesta[i]['pk']+")'><span title='Modificar' style='color:#ffc455; font-size:20px;' class='glyphicon glyphicon-pencil'></span> </a><a href='#' onclick='borrar_categoria_menu("+respuesta[i]['pk']+")'><span title='Eliminar' style='color:#c72439; font-size:20px;' class='glyphicon glyphicon-trash'></span> </a>";
                DATA.push(fila);
            }
            var oTable=$('#tblCategoriasMenu').dataTable();
            oTable.fnClearTable();
            if(DATA.length>0){
                oTable.fnAddData(DATA);
                oTable.fnDraw();
            }else{
                oTable.fnDraw();
            }
        });
    }
    mostrar();

    
});