$(document).on('ready',function(){
    $('#divCambiarFoto').hide();
    $('#contacto').val('0');
    $("#contacto").prop('disabled', true);
    $('#rol').val('0');
    $('#contenedorFormulario').hide();
    activar_usuario = function(id){
        alertify.set({ buttonReverse: true });
        alertify.set({ labels: {
            ok     : "Si",
            cancel : "No"
        } });
        alertify.confirm("Está seguro de activar a ese Usuario?", function (e) {
            if (e) {
                $('#spinner_registro_usuario').html('<h4>Activando</h4><i class="fa fa-spinner fa-spin fa-3x"></i>');
                $.ajax(
                    {
                        type:'GET',
                        url:'/activar_usuario/'+id
                    }
                )
                .done(function(respuesta){
                    if(respuesta=="SI"){
                        alertify.log("Usuario activado correctamente");
                        pagina_actual=1;
                        mostrar();
                    }else if(respuesta=="NO"){
                        alertify.error("Ocurrió un error al activar el usuario");
                    }else{
                        alertify.error("Error: "+respuesta)
                    }
                    $('#spinner_registro_usuario').html('');
                });
            }
        });
    };

    borrar_usuario = function(id){
        alertify.set({ buttonReverse: true });
        alertify.set({ labels: {
            ok     : "Si",
            cancel : "No"
        } });
        alertify.confirm("Está seguro de eliminar a este Usuario?", function (e) {
            if (e) {
                $('#spinner_registro_usuario').html('<h4>Eliminando</h4><i class="fa fa-spinner fa-spin fa-3x"></i>');
                $.ajax(
                    {
                        type:'GET',
                        url:'/eliminar_usuario/'+id
                    }
                )
                .done(function(respuesta){
                    if(respuesta=="SI"){
                        alertify.log("Usuario eliminado correctamente");
                        pagina_actual=1;
                        mostrar();
                        cargar_contactos();
                    }else if(respuesta=="NO"){
                        alertify.error("Ocurrió un error al eliminar el usuario");
                    }else{
                        alertify.error("Error: "+respuesta)
                    }
                    $('#spinner_registro_usuario').html('');
                });
            }
        });
    };
    var existe_usuario=0;
    $('#usuario').on('keyup',function () {
        if($(this).val()!="")
        {
            $.ajax(
                {
                    type:'GET',
                    url:'/comprobarUsuario/'+$(this).val(),
                    beforeSend:function()
                    {},
                    success:function()
                    {}
                }
            )
           .done(function(respuesta){
               if(respuesta=="SI"){
                    existe_usuario=1;
                    $('#divUsuario').removeClass('has-error');
                    $('#divUsuario').removeClass('has-warning');
                    $('#divUsuario').addClass('has-warning');
                }else if(respuesta=="NO"){
                    existe_usuario=0;
                    $('#divUsuario').removeClass('has-error');
                    $('#divUsuario').removeClass('has-warning');
                    $('#divUsuario').addClass('has-success');
               }else{
                    alertify.error("Error: "+respuesta)
               }
               
           })
        }
        else
        {
            $('#divUsuario').removeClass('has-error');
            $('#divUsuario').removeClass('has-warning');
            $('#divUsuario').removeClass('has-warning');
            $('#spinner_register').html('');
        }
    });
    modificar_usuario = function(id){
        $('#divCambiarFoto').show();
        $('#divNombre,#divUsuario,#divClave,#divClave2,#divCorreo,#divRol,#divDireccion').removeClass('has-error');
        $('#divUsuario').removeClass('has-warning');
        $('#spinner_registro_usuario').html('<h4>Obteniendo datos</h4><i class="fa fa-spinner fa-spin fa-3x"></i>');
        $.ajax(
            {
                type:'GET',
                url:'/obtener_datos_usuario/'+id,
                success:function(data)
                {
                    var ruta;
                    for(var i=0;i<data.length;i++){
                        $('#nombre').val(data[i]['nombre']);
                        $('#usuario').val(data[i]['usuario']);
                        $('#correo').val(data[i]['correo']);
                        $('#rol').val(data[i]['rol']);
                        $('#direccion').val(data[i]['direccion']);
                        //Si es rol contacto
                        if(data[i]['rol']==5){
                            $('#contacto').prop('disabled',true);                           
                            $('#rol').prop('disabled',true);                             
                        }

                        $('#usuario2').val(data[i]['pk']);
                        ruta="/media/"+data[i]['imagen'].substring(2);
                    }
                    $('#spinner_registro_usuario').html('');
                    if(ruta!="/media/"){
                        $('#fotoUsuarioEditar').attr('src',ruta);
                        $('#fotoUsuarioEditar').attr('alt',"");
                    }else{
                        $('#fotoUsuarioEditar').attr('alt',"El usuario no tiene foto");
                        $('#fotoUsuarioEditar').attr('src',"");
                    }
                }
            }
        );
        $('#btnAccion').html('Guardar Cambios');
        $('#btnAccion').removeClass('btn-success');
        $('#btnAccion').addClass('btn-warning');
        //la contraseña no puede ser cambiada por esta via
        $('#clave,#clave2').prop('disabled', true);

        $('#contenedorFormulario').fadeIn();
        $('#abrirForm').html('Cerrar');
        $('#abrirForm').removeClass('btn-success');
        $('#abrirForm').addClass('btn-primary');
        n++;
    };
    $('#btnCancelar').on('click',function(){
        $('#spinner_registro_usuario').html('');
        $('#btnAccion').html('Guardar');
        $('#btnAccion').addClass('btn-success');
        $('#btnAccion').removeClass('btn-warning');
        $('#clave,#clave2').prop('disabled', false);
        $('#fotoUsuarioEditar').attr('src',"");
        $('#fotoUsuarioEditar').attr('alt',"");
        $('#divCambiarFoto').hide();
        limpiar();
    });


    $('#rol').on('change',function(){
        if($(this).val()==5 || $('#rol option:selected').text()=="Contacto"){
            $('#contacto').val(0);
            $("#contacto").prop('disabled', false);

        }else{
            $('#contacto').val(0);
            $("#contacto").prop('disabled', true);
        }
    });
    $('#form_registro_usuarios').on('submit',function(e){
        e.preventDefault();
        var texto = $('#btnAccion').html();
        var codigo=Math.floor(Math.random() * 999999999) + 1 ;
        if (texto=="Guardar"){
            if($('#usuario').val()!="" &&
               $('#clave').val()!="" &&
               $('#clave2').val()!="" &&
               $('#correo').val()!="" &&
               $('#nombre').val()!="" &&
               $('#direccion').val()!="" &&
               $('#rol').val()!=0 &&
                ($('#clave').val()==$('#clave2').val()) &&                
                !$('#divUsuario').hasClass('has-warning') &&
                existe_usuario==0)
            {
                var guardar=true;
                if($(this).val()==5 || $('#rol option:selected').text()=="Contacto"){
                    //Si es contacto hago una validacion aparte
                    if($('#contacto').val()!=0){
                        guardar=true;
                    }else{
                        guardar=false;
                        //VARIABLE DE ERRORES
                        var errores="";
                        if($('#contacto').val()=="0"){
                            $('#divContacto').addClass('has-error');
                            if(errores!=""){errores+="<br> • Debe seleccionar el establecimiento asociado";}
                            else
                                {errores+="• Debe seleccionar el establecimiento asociado";}
                        }else{
                            $('#divContacto').removeClass('has-error');
                        }

                        if(errores!=""){
                          alertify.error(errores);
                        }
                    }
                }
                if(guardar){
                    $('#divNombre,#divUsuario,#divClave,#divClave2,#divCorreo,#divRol,#divContacto,#divDireccion').removeClass('has-error');
                    $('#divUsuario').removeClass('has-warning');

                    //CODIGO NUEVO
                    formdata=new FormData();
                    var file = $('#imagen')[0].files[0];
                    if(formdata){
                        formdata.append("nombre",$('#nombre').val());
                        formdata.append("usuario",$('#usuario').val()); 
                        formdata.append("correo",$('#correo').val()); 
                        formdata.append("clave",$('#clave').val()); 
                        formdata.append("codigo",codigo);     
                        formdata.append("rol",$('#rol').val());   
                        formdata.append("contacto",$('#contacto').val());   
                        formdata.append("imagen",file);
                        formdata.append("direccion",$('#direccion').val());
                        formdata.append("csrfmiddlewaretoken",$('input[name=csrfmiddlewaretoken]').val());                         
                    }

                    //FIN
                    $.ajax(
                        {
                            url:'/registro_usuario',
                            data:formdata,
                            processData: false,
                            contentType: false,
                            type:'POST',
                            beforeSend:function()
                            {
                                $('#spinner_registro_usuario').html('<i class="fa fa-spinner fa-spin fa-3x"></i>');
                            }
                            
                        }
                    )
                    .done(function(respuesta){
                        $('#spinner_registro_usuario').html('');
                        if(respuesta=="SI"){
                            alertify.log('Usuario registrado');
                            limpiar();
                            pagina_actual=1;
                            mostrar();
                            cargar_contactos();
                            $('#contenedorFormulario').fadeOut();
                            $('#abrirForm').html('Agregar Nuevo');
                            $('#abrirForm').removeClass('btn-primary');
                            $('#abrirForm').addClass('btn-success');
                            n++;
                        }else if(respuesta=="Error1"){
                            alertify.error('Ocurrió un error en la creacion del usuario');
                        }else if(respuesta=="Error2"){
                            alertify.error('Datos enviados por el método incorrecto');
                        }else{
                            alertify.error("Error: "+respuesta)
                        }
                    });
                }

            }else{
                //VARIABLE DE ERRORES
                var errores="";
                if($('#direccion').val()==""){
                    $('#divDireccion').addClass('has-error');
                    if(errores!=""){errores+="<br> • Debe introducir la Dirección";}
                    else
                        {errores+="• Debe introducir la Dirección";}
                }else{
                    $('#divDireccion').removeClass('has-error');
                }

                if($('#nombre').val()==""){
                    $('#divNombre').addClass('has-error');
                    if(errores!=""){errores+="<br> • Debe introducir el Nombre";}
                    else
                        {errores+="• Debe introducir el Nombre";}
                }else{
                    $('#divNombre').removeClass('has-error');
                }

                if($('#usuario').val()==""){
                    $('#divUsuario').addClass('has-error');
                    if(errores!=""){errores+="<br> • Debe introducir el Usuario";}
                    else
                        {errores+="• Debe introducir el Usuario";}
                }else{
                    $('#divUsuario').removeClass('has-error');
                }

                if($('#rol').val()==0){
                    $('#divRol').addClass('has-error');
                    if(errores!=""){errores+="<br> • Seleccione un Rol";}
                    else
                        {errores+="• Seleccione un Rol";}
                }else{
                    $('#divRol').removeClass('has-error');
                }
                //Valido la clave
                if($('#clave').val()==""){
                    $('#divClave').addClass('has-error');
                    if(errores!=""){errores+="<br> • Debe introducir la Clave";}
                    else
                        {errores+="• Debe introducir la Clave";}
                }else{
                     $('#divClave').removeClass('has-error');
                }
                //Valido la repiticion de la clave
                if($('#clave2').val()==""){
                    $('#divClave2').addClass('has-error');
                    if(errores!=""){errores+="<br> • Debe repetir la Clave";}
                    else
                        {errores+="• Debe repetir la Clave";}
                }else{
                     $('#divClave2').removeClass('has-error');
                }
                var c1=$('#clave').val(),
                c2=$('#clave2').val();
                //Si alguna clave esta escrita, compruebo su parentesco
                if(c1!="" && c2!=""){

                    if(c1!=c2){
                        $('#divClave,#divClave2').addClass('has-error');
                        if(errores!=""){errores+="<br> • Las contraseñas no coinciden";}
                        else
                            {errores+="• Las contraseñas no coinciden";}                    
                    }else{
                        $('#divClave,#divClave2').removeClass('has-error');
                    }
                }

                if($('#correo').val()==""){
                    $('#divCorreo').addClass('has-error');
                    if(errores!=""){errores+="<br> • Debe introducir el Correo";}
                        else
                            {errores+="• Debe introducir el Correo";}        
                }else{
                    $('#divCorreo').removeClass('has-error');                     
                }   
                             

                if($('#divUsuario').hasClass('has-warning') || existe_usuario==1){
                    if(errores!=""){errores+="<br> • Este usuario ya existe";}
                        else
                            {errores+="• Este usuario ya existe";}        
                }

                if(errores!=""){
                  alertify.error(errores);
                }
                
            }
        }else if(texto=="Guardar Cambios"){
            if($('#usuario2').val()!="" &&
               $('#usuario').val()!="" &&
               $('#correo').val()!="" &&
               $('#nombre').val()!="" &&
               $('#rol').val()!=0 &&
               $('#direccion').val()!="" &&
              !$('#divUsuario').hasClass('has-warning'))
            {
                var editar=true;
                if(editar){
                    $('#divNombre,#divUsuario,#divClave,#divClave2,#divCorreo,#divRol,#divContacto,#divDireccion').removeClass('has-error');
                    $('#divUsuario').removeClass('has-warning');
                    //CODIGO NUEVO
                    formdata=new FormData();
                    var file = $('#imagen')[0].files[0];
                    if(formdata){
                        formdata.append("nombre",$('#nombre').val());
                        formdata.append("usuario",$('#usuario').val()); 
                        formdata.append("correo",$('#correo').val()); 
                        formdata.append("idusuario",$('#usuario2').val());     
                        formdata.append("rol",$('#rol').val());   
                        formdata.append("contacto",$('#contacto').val());
                        formdata.append("direccion",$('#direccion').val());

                        if($('#chkCambiarfoto').is(":checked")){
                            formdata.append("imagen",file);
                        }                       

                        formdata.append("csrfmiddlewaretoken",$('input[name=csrfmiddlewaretoken]').val());                         
                    }


                    //FIN
                    $.ajax(
                        {
                            type:'POST',
                            url:'/modifica_usuario',
                            data:formdata,
                            processData: false,
                            contentType: false,
                            beforeSend:function()
                            {
                                $('#spinner_registro_usuario').html('<i class="fa fa-spinner fa-spin fa-3x"></i>');
                            }
                        }
                    )
                    .done(function(respuesta){
                        $('#spinner_registro_usuario').html('');
                        if(respuesta=="SI"){
                            alertify.log('Usuario Modificado');
                            limpiar();
                            pagina_actual=1;
                            mostrar();
                            cargar_contactos();
                            $('#btncancelar').click();
                            $('#contenedorFormulario').fadeOut();
                            $('#abrirForm').html('Agregar Nuevo');
                            $('#abrirForm').removeClass('btn-primary');
                            $('#abrirForm').addClass('btn-success');
                            n++;
                        }else if(respuesta=="Error1"){
                            alertify.error('Ocurrió un error en la modificación del usuario');
                        }else if(respuesta=="Error2"){
                            alertify.error('Datos enviados por el método incorrecto');
                        }else{
                            alertify.error("Error: "+respuesta)
                        }
                    });
                }
                
            }else{
                //VARIABLE DE ERRORES
                var errores="";
                if($('#direccion').val()==""){
                    $('#divDireccion').addClass('has-error');
                    if(errores!=""){errores+="<br> • Debe introducir la Dirección";}
                    else
                        {errores+="• Debe introducir la Dirección";}
                }else{
                    $('#divDireccion').removeClass('has-error');
                }

                if($('#nombre').val()==""){
                    $('#divNombre').addClass('has-error');
                    if(errores!=""){errores+="<br> • Debe introducir el Nombre";}
                    else
                        {errores+="• Debe introducir el Nombre";}
                }else{
                    $('#divNombre').removeClass('has-error');
                }

                if($('#usuario').val()==""){
                    $('#divUsuario').addClass('has-error');
                    if(errores!=""){errores+="<br> • Debe introducir el Usuario";}
                    else
                        {errores+="• Debe introducir el Usuario";}
                }else{
                    $('#divUsuario').removeClass('has-error');
                }

                if($('#rol').val()==0){
                    $('#divRol').addClass('has-error');
                    if(errores!=""){errores+="<br> • Seleccione un Rol";}
                    else
                        {errores+="• Seleccione un Rol";}
                }else{
                    $('#divRol').removeClass('has-error');
                }                

                if($('#divUsuario').hasClass('has-warning')){
                    if(errores!=""){errores+="<br> • Este usuario ya existe";}
                        else
                            {errores+="• Este usuario ya existe";}        
                }

                if($('#correo').val()==""){
                    $('#divCorreo').addClass('has-error');
                    if(errores!=""){errores+="<br> • Debe introducir el Correo";}
                        else
                            {errores+="• Debe introducir el Correo";}        
                }else{
                    $('#divCorreo').removeClass('has-error');                     
                } 

                if(errores!=""){
                  alertify.error(errores);
                }
            }
        }
    });
    var n=0;
    $('#abrirForm').on('click',function(){
        if(n%2==0){
            $('#contenedorFormulario').fadeIn();
            $(this).html('Cerrar');
            $(this).removeClass('btn-success');
            $(this).addClass('btn-primary');
            $('#btnCancelar').click();
        }else{
            $('#contenedorFormulario').fadeOut();
            $(this).html('Agregar Nuevo');
            $(this).removeClass('btn-primary');
            $(this).addClass('btn-success');
            $('#btnCancelar').click();
        }
        n++;
    });
    limpiar = function(){
        $('#nombre,#usuario,#usuario2,#correo,#clave,#clave2,#direccion').val('');
        $('#rol,#contacto').val(0);
        $('#divNombre,#divUsuario,#divClave,#divClave2,#divCorreo,#divRol,#divContacto,#divDireccion').removeClass('has-error');
        $('#divUsuario').removeClass('has-warning');
        $('#divUsuario').removeClass('has-success');
        $('#imagen').val('');
        $('#chkCambiarfoto').prop( "checked", false );
        $('#contacto').prop( "disabled", true );
        $('#rol').prop('disabled',false);
    };

    ver_foto=function(idimagen){
        var ruta_foto=$('#fotoid'+idimagen).attr('ruta_foto');
        $('#imagenModal').attr('src',ruta_foto);
        $('#btnverFoto').click();
    }
    
    reporte_monto_usuario=function(id){
        window.open('/reporte_monto_usuario/'+id);
    }
    ver_equipos=function(id){
        $.ajax({
            type:'GET',
            url:'/get_my_teams/'+id
        }).done(function(respuesta){
            var DATA=[];
            for(var i=0;i<respuesta.length;i++){
                item={};

                var usuario_creacion=respuesta[i]['equipo__usuario_creacion'];
                var usuario=respuesta[i]['usuario__usuario'];
                if(usuario==usuario_creacion){
                    item[0]="<span class='label-success'>Creador</span><p style='color:#445F7E;'>"+respuesta[i]['equipo__nombre']+"</p>";
                }else{
                    item[0]="<span class='label-info'>Miembro</span><p style='color:#445F7E;'>"+respuesta[i]['equipo__nombre']+"</p>";
                }
                
                DATA.push(item);
            }
            if(DATA.length>0){
                $('#btnModalEquipos').click();
                var oTable = $('#tblEquipos').dataTable();
                oTable.fnClearTable();
                oTable.fnAddData(DATA);
                oTable.fnDraw();
            }else{
                alertify.log("Este usuario no está en ningun equipo");
            }
        });
    }
    cargar_contactos=function(){
        var datos="";
        $.ajax({
            type:'GET',
            url:'/cargar_contactos_libres'
        }).done(function(respuesta){
            for(var i=0;i<respuesta.length;i++){
                var id=respuesta[i]['pk'];
                var contacto=respuesta[i]['nombre'];
                datos+="<option value='"+id+"'>"+contacto+"</option>";
            }
            $('#contacto').html("<option value='0'>- Seleccione un establecimiento -</option>");    
            $('#contacto').append(datos);                
        });
    };
    cargar_contactos();
    mostrar=function(){
        $.ajax({
            type:'GET',
            url:'/get_users'
        }).done(function(respuesta){
            var DATA=[];
            for(var i=0;i<respuesta.length;i++){
                ruta_foto="/media/"+respuesta[i]['imagen'].substring(2);                            
                if(ruta_foto=="/media/"){
                    ruta_foto="/media/nofoto.png";
                }

                var id = respuesta[i]['pk'];
                var codigo_Poderoso= "<a href='#' onclick='modificar_usuario("+id+")'><span title='Modificar' style='color:#ffc455; font-size:20px;' class='glyphicon glyphicon-pencil'></span> </a>";
                codigo_Poderoso +="<a href='#' onclick='borrar_usuario("+id+")'><span title='Eliminar' style='color:#c72439; font-size:20px;' class='glyphicon glyphicon-trash'></span> </a>";
                var activar="";
                if($('#idUser').html()==id){
                    codigo_Poderoso="<h5>No disponible</h5>";
                    activar="";
                }
                var activo = respuesta[i]['activo'];
                if (activo){
                    activo="<i style='color:#70e663' class='fa fa-check-circle' aria-hidden='true'></i>";
                }
                else{
                    activo="<i style='color:#c5454e' class='fa fa-check-circle' aria-hidden='true'></i>";
                    activar="<a href='#' onclick='activar_usuario("+id+")'><i style='color:#214165;' title='Activar' class='fa fa-toggle-on' aria-hidden='true'></i></a>";
                }
                var equipo="";
                if(respuesta[i]['rol__rol']=="Firefighter"){
                    equipo="<td class='text-center'><a class='btn btn-primary' onclick='ver_equipos("+respuesta[i]['pk']+")'>Ver Equipos</a></td>";
                }else if(respuesta[i]['rol__rol']=="Developer"){
                    equipo="<td class='text-center'><a class='btn btn-success' onclick='ver_equipos("+respuesta[i]['pk']+")'>Ver Equipo</a></td>";
                }else{
                    equipo="<td class='text-center'><h4>No disponible</h4></td>";
                }
                fila={};
                fila[0]=respuesta[i]['pk'];
                fila[1]="<center><img width=50px src='"+ruta_foto+"' id='fotoid"+respuesta[i]['pk']+"' onclick='ver_foto("+respuesta[i]['pk']+")' ruta_foto='"+ruta_foto+"' class='img-responsive' alt='Cinque Terre'></center>";
                fila[2]=respuesta[i]['nombre'];
                fila[3]=respuesta[i]['usuario'];
                fila[4]=respuesta[i]['correo'];
                fila[5]=equipo;
                fila[6]=respuesta[i]['rol__rol'];
                fila[7]=activo;
                fila[8]=respuesta[i]['direccion'];
                fila[9]=codigo_Poderoso+activar+"<a href='#' onclick='reporte_monto_usuario("+id+")'><span title='Reporte de Monto' style='color:#66D264; font-size:20px;' class='glyphicon glyphicon-euro'></span> </a>";

                DATA.push(fila);
            }
            var oTable=$('#tblUsuarios').dataTable();
            oTable.fnClearTable();
            if(DATA.length>0){
                oTable.fnAddData(DATA);
                oTable.fnDraw();
            }else{
                oTable.fnDraw();
            }
        });
    };
    
    mostrar();

});
