$(document).on('ready',function(){

    $('#contenedorFormulario').hide();
    borrar_categoria = function(id){
        alertify.set({ buttonReverse: true });
        alertify.set({ labels: {
            ok     : "Si",
            cancel : "No"
        } });
        alertify.confirm("Está seguro de eliminar esta categoría?", function (e) {
            if (e) {
                $('#spinner_registro_categoria').html('<h4>Eliminando</h4><i class="fa fa-spinner fa-spin fa-3x"></i>');
                $.ajax(
                    {
                        type:'GET',
                        url:'administracion/eliminar_categoria/'+id
                    }
                )
                .done(function(respuesta){
                    if(respuesta=="SI"){
                        alertify.log("Categoría eliminada correctamente");
                        mostrar();
                    }else if(respuesta=="NO"){
                        alertify.error("Ocurrió un error al eliminar la categoría");
                    }else if(respuesta=="TC"){
                        alertify.alert("Esta categoría contiene contactos registrados");
                    }
                    else{
                        alertify.error("Error: "+respuesta);
                    }
                    $('#spinner_registro_categoria').html('');
                });
            }
        });
    };
    modificar_categoria = function(id){
      $('#spinner_registro_categoria').html('<h4>Obteniendo datos</h4><i class="fa fa-spinner fa-spin fa-3x"></i>');
        $.ajax(
            {
                type:'GET',
                url:'administracion/obtener_datos_categoria/'+id,
                success:function(data)
                {
                    for(var i=0;i<data.length;i++){
                        $('#categoria').val(data[i]['fields']['nombre']);
                        $('#categoria2').val(data[i]['pk']);
                    }
                    $('#spinner_registro_categoria').html('');
                }
            }
        );
        $('#btnAccion').html('Guardar Cambios');
        $('#btnAccion').removeClass('btn-success');
        $('#btnAccion').addClass('btn-warning');

        $('#contenedorFormulario').fadeIn();
        $('#abrirForm').html('Cerrar');
        $('#abrirForm').removeClass('btn-success');
        $('#abrirForm').addClass('btn-primary');
        n++;
    };
    $('#btnCancelar').on('click',function(){
        $('#spinner_registro_categoria').html('');
        $('#btnAccion').html('Guardar');
        $('#btnAccion').addClass('btn-success');
        $('#btnAccion').removeClass('btn-warning');
        limpiar();
    });
    var n=0;
    $('#abrirForm').on('click',function(){
        if(n%2==0){
            $('#contenedorFormulario').fadeIn();
            $(this).html('Cerrar');
            $(this).removeClass('btn-success');
            $(this).addClass('btn-primary');
            $('#btnCancelar').click();
        }else{
            $('#contenedorFormulario').fadeOut();
            $(this).html('Agregar Nuevo');
            $(this).removeClass('btn-primary');
            $(this).addClass('btn-success');
            $('#btnCancelar').click();
        }
        n++;
    });
    limpiar = function(){
        $('#categoria,#categoria2').val('');
        $('#divCategoria').removeClass('has-error');
    };
    $('#form_registro_categorias').on('submit',function(e){
        e.preventDefault();
        var texto = $('#btnAccion').html();
        if (texto=="Guardar"){
            if($('#categoria').val()!="")
            {
                $('#divCategoria').removeClass('has-error');
                $.ajax(
                    {
                        type:'POST',
                        url:'administracion/registro_categoria',
                        data:
                        {
                            categoria:$('#categoria').val(),
                            csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
                        },
                        beforeSend:function()
                        {
                            $('#spinner_registro_categoria').html('<i class="fa fa-spinner fa-spin fa-3x"></i>');
                        }
                    }
                )
                .done(function(respuesta){
                    $('#spinner_registro_categoria').html('');
                    if(respuesta=="SI"){
                        alertify.log('Categoría registrada');
                        limpiar();
                        mostrar();
                        $('#contenedorFormulario').fadeOut();
                        $('#abrirForm').html('Agregar Nueva');
                        $('#abrirForm').removeClass('btn-primary');
                        $('#abrirForm').addClass('btn-success');
                        n++;
                    }else if(respuesta=="Error1"){
                        alertify.error('Ocurrió un error en la creacion de la categoría');
                    }else if(respuesta=="Error2"){
                        alertify.error('Datos enviados por el método incorrecto');
                    }else{
                        alertify.error("Error: "+respuesta);
                    }
                });
            }else{
                if($('#categoria').val()==""){
                    $('#divCategoria').addClass('has-error');
                    alertify.error("• Debe introducir la Categoría");
                }else{
                    $('#divCategoria').removeClass('has-error');
                    alertify.error("• Debe completar los campos");
                }              
               
            }
        }else if(texto=="Guardar Cambios"){
            if($('#categoria2').val()!="" &&
               $('#categoria').val()!="")
            {
                $('#divCategoria').removeClass('has-error');
                $.ajax(
                    {
                        type:'POST',
                        url:'administracion/modifica_categoria',
                        data:
                        {
                            idcategoria:$('#categoria2').val(),
                            categoria:$('#categoria').val(),
                            csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
                        },
                        beforeSend:function()
                        {
                            $('#spinner_registro_categoria').html('<i class="fa fa-spinner fa-spin fa-3x"></i>');
                        }
                    }
                )
                .done(function(respuesta){
                    $('#spinner_registro_categoria').html('');
                    if(respuesta=="SI"){
                        alertify.log('Categoría Modificada');
                        limpiar();
                        mostrar();
                        $('#btncancelar').click();
                        $('#contenedorFormulario').fadeOut();
                        $('#abrirForm').html('Agregar Nueva');
                        $('#abrirForm').removeClass('btn-primary');
                        $('#abrirForm').addClass('btn-success');
                        n++;
                    }else if(respuesta=="Error1"){
                        alertify.error('Ocurrió un error en la modificación de la categoría');
                    }else if(respuesta=="Error2"){
                        alertify.error('Datos enviados por el método incorrecto');
                    }else{
                        alertify.error("Error: "+respuesta);
                    }
                });
            }else{
               if($('#categoria').val()==""){
                    $('#divCategoria').addClass('has-error');
                    alertify.error("• Debe introducir la Categoría");
                }else{
                    $('#divCategoria').removeClass('has-error');
                    alertify.error("• Debe completar los campos");
                }   
            }
        }
    });
     ver_foto=function(idimagen){
        var ruta_foto=$('#fotoid'+idimagen).attr('ruta_foto');
        $('#imagenModal').attr('src',ruta_foto);
        $('#btnverFoto').click();
    }
    mostrar=function(){
        $.ajax({
            type:'GET',
            url:'/administracion/get_categories'
        }).done(function(respuesta){
            var DATA=[];
            for(var i=0;i<respuesta.length;i++){
                ruta_foto="/media/"+respuesta[i]['usuario__imagen'].substring(2);
                            
                if(ruta_foto=="/media/"){
                    ruta_foto="/media/nofoto.png";
                }
                var id = respuesta[i]['pk'];
                fila={};
                fila[0]=respuesta[i]['pk'];
                fila[1]=respuesta[i]['nombre'];
                fila[2]=respuesta[i]['usuario__usuario'];
                fila[3]="<center><img width=50px src='"+ruta_foto+"' id='fotoid"+respuesta[i]['pk']+"' onclick='ver_foto("+respuesta[i]['pk']+")' ruta_foto='"+ruta_foto+"' class='img-responsive' alt='Cinque Terre'></center>";
                fila[4]="<a href='#' onclick='modificar_categoria("+id+")'><span title='Modificar' data-toggle='tooltip' style='color:#ffc455; font-size:20px;' class='glyphicon glyphicon-pencil'></span> </a><a href='#' onclick='borrar_categoria("+id+")'><span title='Eliminar' data-toggle='tooltip' style='color:#c72439; font-size:20px;' class='glyphicon glyphicon-trash'></span> </a>";
                DATA.push(fila);
            }
            var oTable=$('#tblCategorias').dataTable();
            oTable.fnClearTable();
            if(DATA.length>0){
                oTable.fnAddData(DATA);
                oTable.fnDraw();
            }else{
                oTable.fnDraw();
            }
        });
    }
    mostrar();
});
