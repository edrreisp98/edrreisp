$(document).on('ready',function(){
  $('#contenedorFormulario').hide();
  borrar_contacto=function(id){
    alertify.set({ buttonReverse: true});
    alertify.set({ labels: {
        ok     : "Si",
        cancel : "No"
    } });
    alertify.confirm("Está seguro de eliminar este contacto?", function (e) {
        if (e) {
            $('#spinner_registro_contacto').html('<h4>Eliminando</h4><i class="fa fa-spinner fa-spin fa-3x"></i>');
            $.ajax(
                {
                    type:'GET',
                    url:'administracion/eliminar_contacto/'+id
                }
            )
            .done(function(respuesta){
                if(respuesta=="SI"){
                    alertify.log("Establecimiento eliminado correctamente");
                    mostrar();
                }else if(respuesta=="NO"){
                    alertify.error("Ocurrió un error al eliminar el Establecimiento");
                }else{
                    alertify.error("Error: "+ respuesta);
                }
                $('#spinner_registro_contacto').html('');
            });
        }
    });
  };

  modificar_contacto = function(id){
      $('#spinner_registro_contacto').html('<h4>Obteniendo datos</h4><i class="fa fa-spinner fa-spin fa-3x"></i>');
      $('#divContacto,#divTelefono,#divDireccion,#divCategoria').removeClass('has-error');
      $.ajax(
          {
            type:'GET',
            url:'administracion/obtener_datos_contacto/'+id,
            success:function(data)
            {
              for(var i=0;i<data.length;i++){
                $('#contacto2').val(data[i]['pk']);
                $('#contacto').val(data[i]['fields']['nombre']);
                $('#telefono').val(data[i]['fields']['telefono']);
                $('#direccion').val(data[i]['fields']['direccion']);
                $('#categoria').val(data[i]['fields']['categoria']);
                $('#desde').val(data[i]['fields']['desde']);
                $('#hasta').val(data[i]['fields']['hasta']);
              }
              $('#spinner_registro_contacto').html('');
            }
          }
      );
      $('#btnAccion').html('Guardar Cambios');
      $('#btnAccion').removeClass('btn-success');
      $('#btnAccion').addClass('btn-warning');

      $('#contenedorFormulario').fadeIn();
      $('#abrirForm').html('Cerrar');
      $('#abrirForm').removeClass('btn-success');
      $('#abrirForm').addClass('btn-primary');
      n++;
  };
  $('#btnCancelar').on('click',function(){
      $('#spinner_registro_contacto').html('');
      $('#btnAccion').html('Guardar');
      $('#btnAccion').addClass('btn-success');
      $('#btnAccion').removeClass('btn-warning');
      limpiar();
  });
  var n=0;
  $('#abrirForm').on('click',function(){
      if(n%2==0){
          $('#contenedorFormulario').fadeIn();
          $(this).html('Cerrar');
          $(this).removeClass('btn-success');
          $(this).addClass('btn-primary');
          $('#btnCancelar').click();
      }else{
          $('#contenedorFormulario').fadeOut();
          $(this).html('Agregar Nuevo');
          $(this).removeClass('btn-primary');
          $(this).addClass('btn-success');
          $('#btnCancelar').click();
      }
      n++;
  });
  limpiar = function(){
    $('#contacto,#contacto2,#telefono,#direccion,#desde,#hasta').val('');
    $('#categoria').val('0');
    $('#divContacto,#divTelefono,#divDireccion,#divCategoria,#divDesde,#divHasta').removeClass('has-error');
    
  };
  $('#form_registro_contactos').on('submit',function(e){
    e.preventDefault();
    var texto=$('#btnAccion').html();
    if(texto=="Guardar"){
      if($('#contacto').val()!="" &&
         $('#telefono').val()!="" &&
         $('#categoria').val()!="" &&
         $('#direccion').val()!="" &&
         $('#desde').val()!="" &&
         $('#hasta').val()!="" &&
         ($('#desde').val()<$('#hasta').val()) &&
         ($('#desde').val()!=$('#hasta').val()) &&
       $('#categoria').val()!="0"){
          $('#divContacto,#divTelefono,#divDireccion,#divCategoria,#divDesde,#divHasta').removeClass('has-error');
           $.ajax(
             {
               type:'POST',
               url:'administracion/registro_contacto',
               data:
               {
                 contacto:$('#contacto').val(),
                 telefono:$('#telefono').val(),
                 direccion:$('#direccion').val(),
                 categoria:$('#categoria').val(),
                 desde:$('#desde').val(),
                 hasta:$('#hasta').val(),
                 csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
               },
               beforeSend:function()
               {
                 $('#spinner_registro_contacto').html('<i class="fa fa-spinner fa-spin fa-3x"></i>');
               }
             }
           )
           .done(function(respuesta){
             $('#spinner_registro_contacto').html('');
             if(respuesta=="SI"){
               alertify.log('Contacto registrado');
               limpiar();
               mostrar();
               $('#contenedorFormulario').fadeOut();
               $('#abrirForm').html('Agregar Nuevo');
               $('#abrirForm').removeClass('btn-primary');
               $('#abrirForm').addClass('btn-success');
               n++;
             }else if (respuesta=="Error1") {
               alertify.error('Ocurrió un error en la creacion del contacto');
             }else if (respuesta=="Error2") {
               alertify.error('Datos enviados por el método incorrecto');
             }else{
                alertify.error("Error: "+ respuesta);
             }
           });
         }else{
            //VARIABLE DE ERRORES
            var errores="";

            if($('#desde').val()!="" && $('#hasta').val()!=""){

              if($('#desde').val()>$('#hasta').val()){
                $('#divDesde,#divHasta').addClass('has-error');              
                if(errores!=""){errores+="<br> • La fecha inicial debe ser mayor que la final";}
                else
                  {errores+="• La fecha inicial debe ser mayor que la final";}
              }else if($('#desde').val()==$('#hasta').val()){
                $('#divDesde,#divHasta').addClass('has-error');              
                if(errores!=""){errores+="<br> • Las fechas no pueden ser iguales";}
                else
                  {errores+="• Las fechas no pueden ser iguales";}
              }else{
                $('#divDesde,#divHasta').removeClass('has-error');
              }

            }else{//Si estan vacios verifico cual es
              if($('#desde').val()==""){
                $('#divDesde').addClass('has-error');              
                if(errores!=""){errores+="<br> • Debe Introducir Hora Inicial";}
                else
                  {errores+="• Debe Introducir Hora Inicial";}
              }else{
                $('#divDesde').removeClass('has-error');
              }

              if($('#hasta').val()==""){
                $('#divHasta').addClass('has-error');              
                if(errores!=""){errores+="<br> • Debe Introducir Hora Final";}
                else
                  {errores+="• Debe Introducir Hora Final";}
              }else{
                $('#divHasta').removeClass('has-error');
              }
            }           

            

            //Valida las alertas
            if($('#categoria').val()=="0"){
              $('#divCategoria').addClass('has-error');
              if(errores!=""){errores+="<br> • Seleccione una categoría";}
              else
                {errores+="• Seleccione una categoría";}
            }else{
              $('#divCategoria').removeClass('has-error');
            }
            if($('#contacto').val()==""){
              $('#divContacto').addClass('has-error');              
              if(errores!=""){errores+="<br> • Debe Introducir el Establecimiento";}
              else
                {errores+="• Debe Introducir el Establecimiento";}
            }else{
              $('#divContacto').removeClass('has-error');
            }
            if($('#telefono').val()==""){
              $('#divTelefono').addClass('has-error');
              if(errores!="") {errores+="<br> • Debe Introducir el Teléfono";}
              else
                {errores+="• Debe Introducir el Teléfono";}            
            }else{
              $('#divTelefono').removeClass('has-error');
            }

            if($('#direccion').val()==""){
              $('#divDireccion').addClass('has-error');
              if(errores!=""){errores+="<br> • Debe Introducir la Dirección";}
              else
                {errores+="• Debe Introducir la Dirección";}
            }else{
              $('#divDireccion').removeClass('has-error');
            }

            //Valida los campos
            if(errores!=""){
              alertify.error(errores);
            }
         }
    }else if(texto=="Guardar Cambios"){
      if($('#contacto2').val()!="" &&
         $('#contacto').val()!="" &&
         $('#telefono').val()!="" &&
         $('#categoria').val()!="" &&
         $('#direccion').val()!=""&&
         $('#desde').val()!="" &&
         $('#hasta').val()!="" &&
         ($('#desde').val()<$('#hasta').val()) &&
         ($('#desde').val()!=$('#hasta').val()) &&
       $('#categoria').val()!="0")
         {
          $('#divContacto,#divTelefono,#divDireccion,#divCategoria,#divDesde,#divHasta').removeClass('has-error');
           $.ajax(
             {
               type:'POST',
               url:'administracion/modifica_contacto',
               data:
               {
                 contacto2:$('#contacto2').val(),
                 contacto:$('#contacto').val(),
                 telefono:$('#telefono').val(),
                 direccion:$('#direccion').val(),
                 categoria:$('#categoria').val(),
                 desde:$('#desde').val(),
                 hasta:$('#hasta').val(),
                 csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
                },
                beforeSend:function()
                {
                  $('#spinner_registro_contacto').html('<i class="fa fa-spinner fa-spin fa-3x"></i>');
                }
            }
           )
           .done(function(respuesta){
               $('#spinner_registro_contacto').html('');
               if(respuesta=="SI"){
                   alertify.log('Establecimiento Modificado');
                   limpiar();
                   mostrar();
                   $('#btncancelar').click();
                   $('#contenedorFormulario').fadeOut();
                   $('#abrirForm').html('Agregar Nuevo');
                   $('#abrirForm').removeClass('btn-primary');
                   $('#abrirForm').addClass('btn-success');
                   n++;
               }else if(respuesta=="Error1"){
                   alertify.error('Ocurrió un error en la modificación del Establecimiento');
               }else if(respuesta=="Error2"){
                   alertify.error('Datos enviados por el método incorrecto');
               }else{
                   alertify.error("Error: "+ respuesta);
               }
           });
         }else{
           //VARIABLE DE ERRORES
            var errores="";

            
            if($('#desde').val()!="" && $('#hasta').val()!=""){

              if($('#desde').val()>$('#hasta').val()){
                $('#divDesde,#divHasta').addClass('has-error');              
                if(errores!=""){errores+="<br> • La fecha inicial debe ser mayor que la final";}
                else
                  {errores+="• La fecha inicial debe ser mayor que la final";}
              }else if($('#desde').val()==$('#hasta').val()){
                $('#divDesde,#divHasta').addClass('has-error');              
                if(errores!=""){errores+="<br> • Las fechas no pueden ser iguales";}
                else
                  {errores+="• Las fechas no pueden ser iguales";}
              }else{
                $('#divDesde,#divHasta').removeClass('has-error');
              }

            }else{//Si estan vacios verifico cual es
              if($('#desde').val()==""){
                $('#divDesde').addClass('has-error');              
                if(errores!=""){errores+="<br> • Debe Introducir Hora Inicial";}
                else
                  {errores+="• Debe Introducir Hora Inicial";}
              }else{
                $('#divDesde').removeClass('has-error');
              }

              if($('#hasta').val()==""){
                $('#divHasta').addClass('has-error');              
                if(errores!=""){errores+="<br> • Debe Introducir Hora Final";}
                else
                  {errores+="• Debe Introducir Hora Final";}
              }else{
                $('#divHasta').removeClass('has-error');
              }
            } 

            //Valida las alertas
            if($('#categoria').val()=="0"){
              $('#divCategoria').addClass('has-error');
              if(errores!=""){errores+="<br> • Seleccione una categoría";}
              else
                {errores+="• Seleccione una categoría";}
            }else{
              $('#divCategoria').removeClass('has-error');
            }
            if($('#contacto').val()==""){
              $('#divContacto').addClass('has-error');              
              if(errores!=""){errores+="<br> • Debe Introducir el Establecimiento";}
              else
                {errores+="• Debe Introducir el Establecimiento";}
            }else{
              $('#divContacto').removeClass('has-error');
            }
            if($('#telefono').val()==""){
              $('#divTelefono').addClass('has-error');
              if(errores!="") {errores+="<br> • Debe Introducir el Teléfono";}
              else
                {errores+="• Debe Introducir el Teléfono";}            
            }else{
              $('#divTelefono').removeClass('has-error');
            }

            if($('#direccion').val()==""){
              $('#divDireccion').addClass('has-error');
              if(errores!=""){errores+="<br> • Debe Introducir la Dirección";}
              else
                {errores+="• Debe Introducir la Dirección";}
            }else{
              $('#divDireccion').removeClass('has-error');
            }

            //Valida los campos
            if(errores!=""){
              alertify.error(errores);
            }

         }
    }
  });
  desactivar_contacto=function(id){
    alertify.set({ buttonReverse: true});
    alertify.set({ labels: {
        ok     : "Si",
        cancel : "No"
    } });
    alertify.confirm("Está seguro de desactivar este establecimiento?", function (e) {
        if (e) {
            $('#spinner_registro_contacto').html('<h4>Desactivando</h4><i class="fa fa-spinner fa-spin fa-3x"></i>');
            $.ajax(
                {
                    type:'GET',
                    url:'administracion/deactivate_contact/'+id
                }
            )
            .done(function(respuesta){
                if(respuesta=="SI"){
                    alertify.log("Establecimiento desactivado correctamente");
                    mostrar();
                }else if(respuesta=="NO"){
                    alertify.error("Ocurrió un error al desactivar el Establecimiento");
                }else{
                    alertify.error("Error: "+ respuesta);
                }
                $('#spinner_registro_contacto').html('');
            });
        }
    });
  };
  activar_contacto=function(id){
    alertify.set({ buttonReverse: true});
    alertify.set({ labels: {
        ok     : "Si",
        cancel : "No"
    } });
    alertify.confirm("Está seguro de activar este establecimiento?", function (e) {
        if (e) {
            $('#spinner_registro_contacto').html('<h4>Activando</h4><i class="fa fa-spinner fa-spin fa-3x"></i>');
            $.ajax(
                {
                    type:'GET',
                    url:'administracion/activar_contacto/'+id
                }
            )
            .done(function(respuesta){
                if(respuesta=="SI"){
                    alertify.log("Establecimiento activado correctamente");
                    mostrar();
                }else if(respuesta=="NO"){
                    alertify.error("Ocurrió un error al activar el Establecimiento");
                }else{
                    alertify.error("Error: "+ respuesta);
                }
                $('#spinner_registro_contacto').html('');
            });
        }
    });
  };
  ver_foto=function(idimagen){
      var ruta_foto=$('#fotoid'+idimagen).attr('ruta_foto');
      $('#imagenModal').attr('src',ruta_foto);
      $('#btnverFoto').click();
  }
  reporte_pedidos=function(idcontacto){
    alertify.set({ buttonReverse: true});
    alertify.set({ labels: {
        ok     : "Si",
        cancel : "No"
    } });
    alertify.confirm("Está seguro de generar un reporte de los pedidos de este Establecimiento?", function (e) {
        if (e) {
            window.open('/reporte_pedido_contacto/'+idcontacto);                 
        }
    });
  }

  mostrar=function(){
    $.ajax({
        type:'GET',
        url:'/administracion/get_contacts'
    }).done(function(respuesta){
        var DATA=[];
        for(var i=0;i<respuesta.length;i++){
          var id = respuesta[i]['pk'];                    
          ruta_foto="/media/"+respuesta[i]['usuario__imagen'].substring(2);
                  
          if(ruta_foto=="/media/"){
              ruta_foto="/media/nofoto.png";
          }


          var rate=0;
          if(respuesta[i]['average_rating']!=null){
            rate=parseFloat(respuesta[i]['average_rating']).toFixed(2);
          }
          var activar="<a href='#' onclick='activar_contacto("+respuesta[i]['pk']+")'><i style='color:#214165;' title='Activar' class='fa fa-toggle-on' aria-hidden='true'></i></a>";

          var acciones="<a href='#' onclick='modificar_contacto("+id+")'><span title='Modificar' style='color:#ffc455; font-size:20px;' class='glyphicon glyphicon-pencil'></span> </a>"+
                      "<a href='#' onclick='borrar_contacto("+id+")'><span title='Eliminar' style='color:#c72439; font-size:20px;' class='glyphicon glyphicon-trash'></span> </a>";

          var activo="<i style='color: #e04e5a;' class='fa fa-ban fa-2x' aria-hidden='true'></i>";
          if(respuesta[i]['activo']==true){
            activo="<i style='color: #27B04B;' class='fa fa-check-circle-o fa-2x' aria-hidden='true'></i>";
            acciones+="<a href='#' onclick='desactivar_contacto("+respuesta[i]['pk']+")'><i style='color:#C70039;' title='Desactivar' class='fa fa-toggle-on' aria-hidden='true'></i></a>";
          }else{
            acciones+="<a href='#' onclick='activar_contacto("+respuesta[i]['pk']+")'><i style='color:#22DA52;' title='Activar' class='fa fa-toggle-on' aria-hidden='true'></i></a>";
          }
          acciones+="<a href='#' onclick='reporte_pedidos("+respuesta[i]['pk']+")'><span title='Reporte de Pedidos' style='color:#900C3F; font-size:20px;' class='glyphicon glyphicon-list-alt'></span></a>";
          
          var disponible="<i class='fa fa-frown-o fa-3x' style='color:#DE5B50;' aria-hidden='true'></i>";


          var now     = new Date(); 
          var hour    = now.getHours();
          var minute  = now.getMinutes();
          var second  = now.getSeconds(); 
          if(hour.toString().length == 1) {
              var hour = '0'+hour;
          }
          if(minute.toString().length == 1) {
              var minute = '0'+minute;
          }
          if(second.toString().length == 1) {
              var second = '0'+second;
          }   
          var hora = hour+':'+minute+':'+second;   
          

          if(hora>=respuesta[i]['desde'] && hora<=respuesta[i]['hasta']){
            disponible="<i class='fa fa-smile-o fa-3x' style='color:#1ABB48;' aria-hidden='true'></i>";
          }
          fila={};
          fila[0]=respuesta[i]['pk'];
          fila[1]=respuesta[i]['nombre'];
          fila[2]=respuesta[i]['desde'];
          fila[3]=respuesta[i]['hasta'];
          fila[4]=disponible;
          fila[5]=rate;
          fila[6]=respuesta[i]['telefono'];
          fila[7]=respuesta[i]['direccion'];
          fila[8]=respuesta[i]['categoria__nombre'];
          fila[9]=respuesta[i]['usuario__usuario'];
          fila[10]="<center><img width=50px src='"+ruta_foto+"' id='fotoid"+respuesta[i]['pk']+"' onclick='ver_foto("+respuesta[i]['pk']+")' ruta_foto='"+ruta_foto+"' class='img-responsive' alt='Cinque Terre'></center>";
          fila[11]=activo;
          fila[12]=acciones;
          DATA.push(fila);
        }
        var oTable=$('#tblContactos').dataTable();
        oTable.fnClearTable();
        if(DATA.length>0){
            oTable.fnAddData(DATA);
            oTable.fnDraw();
        }else{
            oTable.fnDraw();
        }
    });
  }
  mostrar();


});
