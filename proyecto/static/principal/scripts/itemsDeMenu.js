$(document).on('ready',function(){
	$('#contenedorFormulario').hide();


	$('#btnCancelar').on('click',function(){
        $('#spinner_registro_usuario').html('');
        $('#btnAccion').html('Guardar');
        $('#btnAccion').addClass('btn-success');
        $('#btnAccion').removeClass('btn-warning');
        limpiar();
    });

    var n=0;
    $('#abrirForm').on('click',function(){
        if(n%2==0){
            $('#contenedorFormulario').fadeIn();
            $(this).html('Cerrar');
            $(this).removeClass('btn-success');
            $(this).addClass('btn-primary');
            $('#btnCancelar').click();
        }else{
            $('#contenedorFormulario').fadeOut();
            $(this).html('Agregar Nuevo');
            $(this).removeClass('btn-primary');
            $(this).addClass('btn-success');
            $('#btnCancelar').click();
        }
        n++;
    });

    limpiar = function(){
        $('#item,#precio').val('');
        $('#categoria_menu').val(0);
        $('#divItem').removeClass('has-error');        
    };

    borrar_item_menu = function(id){
        alertify.set({ buttonReverse: true });
        alertify.set({ labels: {
            ok     : "Si",
            cancel : "No"
        } });
        alertify.confirm("Está seguro de eliminar este Item?", function (e) {
            if (e) {
                $('#spinner_registro_usuario').html('<h4>Eliminando</h4><i class="fa fa-spinner fa-spin fa-3x"></i>');
                $.ajax(
                    {
                        type:'GET',
                        url:'/eliminar_item_menu/'+id
                    }
                )
                .done(function(respuesta){
                    if(respuesta=="SI"){
                        alertify.log("Item de menú eliminado");
                        mostrar();
                    }else if(respuesta=="NO"){
                        alertify.error("Ocurrió un error al eliminar el item");
                    }else{
                        alertify.error("Error: "+respuesta)
                    }
                    $('#spinner_registro_usuario').html('');
                });
            }
        });
    };

    $('#form_registro_items').on('submit',function(e){
    	e.preventDefault();
    	var texto=$('#btnAccion').html();
    	var url="/registro_item_menu";
    	if(texto!="Guardar"){
    		url="/modifica_item_menu";
    	}

    	if($('#item').val()!=""){
    		$('#divItem').removeClass('has-error');
    		$.ajax({
    			type:'POST',
    			url:url,
    			data:
    			{
    				id:$('#item2').val(),
    				item:$('#item').val(),
    				categoria:$('#categoria_menu').val(),
    				precio:$('#precio').val(),
    				menu:$('#idVerMenu').html(),
    				csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
    			},
                beforeSend:function()
                {
                    $('#spinner_registro_usuario').html('<i class="fa fa-spinner fa-spin fa-3x"></i>');
                }
    		}).done(function(respuesta){
    			$('#spinner_registro_usuario').html('');
                if(respuesta=="SI"){
                    if(texto=="Guardar"){
                        alertify.log('Item de menú registrado');
                    }else{
                        alertify.log('Item de menú editado');
                    }                    
                    limpiar();
                    mostrar();
                    $('#contenedorFormulario').fadeOut();
                    $('#abrirForm').html('Agregar Nuevo');
                    $('#abrirForm').removeClass('btn-primary');
                    $('#abrirForm').addClass('btn-success');
                    n++;
                }else if(respuesta=="Error1"){
                    if(texto=="Guardar"){
                        alertify.error('Ocurrió un error en la creación del ítem');
                    }else{
                        alertify.error('Ocurrió un error en la edición del ítem');
                    }                     
                }else if(respuesta=="Error2"){
                    alertify.error('Datos enviados por el método incorrecto');
                }else{
                    alertify.error("Error: "+respuesta)
                }
    		});
    	}else{
    		var errores="";
            if($('#item').val()==""){
                $('#divItem').addClass('has-error');
                if(errores!=""){errores+="<br> • Debe introducir el Item del Menú";}
                else
                    {errores+="• Debe introducir el Item del Menú";}
            }else{
                $('#divItem').removeClass('has-error');
            }
            if(errores!=""){
                alertify.error(errores);
            }    
    	}
    });

    modificar_item_menu = function(id){
        $('#divItem').removeClass('has-error');
        $('#spinner_registro_usuario').html('<h4>Obteniendo datos</h4><i class="fa fa-spinner fa-spin fa-3x"></i>');
        $.ajax(
            {
                type:'GET',
                url:'/obtener_datos_item_menu/'+id,
                success:function(data)
                {
                    for(var i=0;i<data.length;i++){
                        $('#item').val(data[i]['fields']['nombre']);
                        $('#item2').val(data[i]['pk']);
                        $('#precio').val(data[i]['fields']['precio'])
                        $('#categoria_menu').val(data[i]['fields']['CatMenu'])
                    }
                    $('#spinner_registro_usuario').html('');
                }
            }
        );
        $('#btnAccion').html('Guardar Cambios');
        $('#btnAccion').removeClass('btn-success');
        $('#btnAccion').addClass('btn-warning');
        
        $('#contenedorFormulario').fadeIn();
        $('#abrirForm').html('Cerrar');
        $('#abrirForm').removeClass('btn-success');
        $('#abrirForm').addClass('btn-primary');
        n++;
    };
    mostrar=function(){
        $.ajax({
            type:'GET',
            url:'/get_items_menu/'+$('#idVerMenu').html()
        }).done(function(respuesta){
            var DATA=[];
            for(var i=0;i<respuesta.length;i++){
                
                fila={};
               
                fila[0]=respuesta[i]['pk'];
                fila[1]=respuesta[i]['nombre'];
                fila[2]=respuesta[i]['CatMenu__nombre'];
                fila[3]=respuesta[i]['precio'];
                fila[4]="<a href='#' onclick='modificar_item_menu("+respuesta[i]['pk']+")'><span title='Modificar' style='color:#ffc455; font-size:20px;' class='glyphicon glyphicon-pencil'></span> </a><a href='#' onclick='borrar_item_menu("+respuesta[i]['pk']+")'><span title='Eliminar' style='color:#c72439; font-size:20px;' class='glyphicon glyphicon-trash'></span> </a>";
                                        
                DATA.push(fila);
            }
            var oTable=$('#tblItems').dataTable();
            oTable.fnClearTable();
            if(DATA.length>0){
                oTable.fnAddData(DATA);
                oTable.fnDraw();
            }else{
                oTable.fnDraw();
            }
        });
    }
    mostrar();
    
});