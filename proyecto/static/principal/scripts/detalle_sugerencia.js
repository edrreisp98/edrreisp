$(document).on('ready',function(){
    aceptar=function(id){
        alertify.set({ buttonReverse: true });
        alertify.set({ labels: {
            ok     : "Si",
            cancel : "No"
        } });
        alertify.confirm("Está seguro de aceptar esta sugerencia?", function (e) {
            if (e) {
                $.ajax(
                    {
                        type:'GET',
                        url:'/administracion/aceptar_sugerencia/'+id
                    }
                )
                .done(function(respuesta){
                    if(respuesta=="SI"){
                        alertify.log("Sugerencia aceptada");
                        location.assign("/principal");
                    }else if(respuesta=="Error1"){
                        alertify.error("Ocurrió un error al aplicar la sugerencia");
                    }else{
                        alertify.error("Error: "+ respuesta);                       
                    }
                });
            }
        });
    };
    rechazar=function(id){
      alertify.set({ buttonReverse: true });
            alertify.set({ labels: {
                ok     : "Si",
                cancel : "No"
            } });
            alertify.confirm("Está seguro de rechazar esta sugerencia?", function (e) {
                if (e) {
                    $.ajax(
                        {
                            type:'GET',
                            url:'/eliminar_sugerencia/'+id
                        }
                    )
                    .done(function(respuesta){
                         if(respuesta=="SI"){
                            alertify.log("Sugerencia rechazada");
                            location.assign("/principal");
                        }else if(respuesta=="NO"){
                            alertify.error("Ocurrió un error al rechazar la sugerencia");
                        }else{
                             alertify.error("Error: "+respuesta);
                         }
                    });
                }
            });
    };
    ver_foto=function(idimagen){
        var ruta_foto=$('#fotoid'+idimagen).attr('ruta_foto');
        $('#imagenModal').attr('src',ruta_foto);
        $('#btnverFoto').click();
    }
    verSugerencia=function(id){
        var tabla="";
        $.ajax(
            {
                type:'GET',
                url:'/ver_sugerencia/'+id,
                success:function(data)
                {
                    if (data.length==0){
                        $('#btnAceptar').hide();
                        $('#btnRechazar').hide();
                        $('#mensajito').hide();
                        $('#mensajito2').show();
                    }else{
                        $('#mensajito2').hide();
                    }
                    var DATA=[];
                    for (var i=0;i<data.length;i++){
                        var produccion=false;
                        if(produccion){                                
                            ruta_foto="/media/"+data[i]['usuario__imagen'].substring(2);
                        
                            if(ruta_foto=="/media/"){
                                ruta_foto="/media/nofoto.png";
                            }
                        }else{
                            ruta_foto="/static/images/"+data[i]['usuario__imagen'].substring(2);
                        
                            if(ruta_foto=="/static/images/"){
                                ruta_foto="/static/images/nofoto.png";
                            }
                        }
                        

                        var aplicada=data[i]['aplicada'];
                        if (aplicada==true){
                            $('#btnAceptar').hide();
                            $('#btnRechazar').hide();
                            $('#mensajito').show();
                        }else{
                            $('#btnAceptar').show();
                            $('#btnRechazar').show();
                            $('#mensajito').hide();
                        }

                        fila={};
                        fila[0]=data[i]['pk'];
                        fila[1]=data[i]['nombre'];
                        fila[2]=data[i]['telefono'];
                        fila[3]=data[i]['direccion'];
                        fila[4]=data[i]['categoria__nombre'];
                        fila[5]=data[i]['usuario__usuario'];
                        fila[6]="<center><img width=50px src='"+ruta_foto+"' id='fotoid"+data[i]['pk']+"' onclick='ver_foto("+data[i]['pk']+")' ruta_foto='"+ruta_foto+"' class='img-responsive' alt='Cinque Terre'></center>";
                        DATA.push(fila);
                    }
                    var oTable=$('#tblSugerencia').dataTable();
                    oTable.fnClearTable();
                    if(DATA.length>0){
                        oTable.fnAddData(DATA);
                        oTable.fnDraw();
                    }else{
                        oTable.fnDraw();
                    }

                }
            }
        );
    };
    verSugerencia($('#idSugerencia').html());

});