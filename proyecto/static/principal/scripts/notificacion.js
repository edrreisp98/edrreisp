$(document).on('ready',function(){
  
  notificar=function(id){
    alertify.set({ buttonReverse: true});
    alertify.set({ labels: {
        ok     : "Si",
        cancel : "No"
    } });
    alertify.confirm("Está seguro de notificar el usuario que su pedido llego?", function (e) {
        if (e) {
            $.ajax(
                {
                    type:'GET',
                    url:'/notificame_este_pedido/'+id
                }
            )
            .done(function(respuesta){
                if(respuesta=="SI"){
                  $('#totalNotificaciones').html(0);
                  //Solo los administradores pueden ver las notificaciones de sugerencia
                  $('#contenidoNotificaciones').html('');
                  if($('#txtRol').val()=="Administrador"){
                    cargarNotificacionSugerencias();
                    cargar_notificacion_admins_pedidos_nuevos();    
                    cargar_notificacion_admins_pedidos_llegados();
                  }
                  cargarNotificacionAlertas();
                  cargarNotificacionRecoger();
                  
                  //$('#audio_fb')[0].play();
                  $('#contenidoNotificaciones').append("<li><a href='/todas_notificaciones' class='view'>Ver todas las notificaciones</a></li>");                    
                }else if(respuesta=="NO"){
                    alertify.error("Ocurrió un error al notificar el pedido");
                }else{
                    alertify.error("Error: "+ respuesta);
                }
            });
        }
    });
  }
  marcar_como_leida=function(id){
    alertify.set({ buttonReverse: true});
    alertify.set({ labels: {
        ok     : "Si",
        cancel : "No"
    } });
    alertify.confirm("Está seguro de marcar como leída esta notificación?", function (e) {
        if (e) {
            $.ajax(
                {
                    type:'GET',
                    url:'/marcar_como_leida/'+id
                }
            )
            .done(function(respuesta){
                if(respuesta=="SI"){
                  $('#totalNotificaciones').html(0);
                  //Solo los administradores pueden ver las notificaciones de sugerencia
                  $('#contenidoNotificaciones').html('');
                  if($('#txtRol').val()=="Administrador"){
                    cargarNotificacionSugerencias();
                    cargar_notificacion_admins_pedidos_nuevos();    
                    cargar_notificacion_admins_pedidos_llegados();
                    cargar_notificacion_admins_cierre_contactos();
                  }
                  cargarNotificacionAlertas();
                  cargarNotificacionRecoger();
                  cargar_notificacion_users_pedidos_llegados();
                  cargar_notificacion_users_pedidos_despachados();
                  cargar_notificacion_users_casi_cierre_contactos();
                  
                  //$('#audio_fb')[0].play();
                  $('#contenidoNotificaciones').append("<li><a href='/todas_notificaciones' class='view'>Ver todas las notificaciones</a></li>");                    
                }else if(respuesta=="NO"){
                    alertify.error("Ocurrió un error al marcar como leída la notificación");
                }else{
                    alertify.error("Error: "+ respuesta);
                }
            });
        }
    });
  };

  
  cargarNotificacionRecoger=function(){
    var tabla="";
    $.ajax({
      type:'GET',
      url:'obtener_notificaciones_recoger_pedido',
      success:function(data)
      {
        var n_actuales=parseInt($('#totalNotificaciones').html() || 0);
        for (var i=0;i<data.length;i++){
          $('#totalNotificaciones').html(parseInt(data.length)+n_actuales);

          var id=data[i]['pk'];
          var pedido=data[i]['pedido__descripcion'];
          var usuario=data[i]['usuario__usuario'];
          var total=data[i]['pedido__total'];
          var fecha=data[i]['fecha'];
          var hora=data[i]['hora'];

          tabla+="<li><a href='#'>";   
            tabla+="<div class='user-new'>";
              tabla+="<p>Recoger Pedido</p>";
              tabla+="<span>"+pedido+"</span>";
            tabla+="</div>";
            tabla+="<div style='position:relative; left:-15px;' class='user-new-left'>";
              tabla+="<span>RD$ "+total+"</span>";
              tabla+="<i style='color:#22970D;' class='fa fa-check-circle'></i>";
              tabla+="<i onclick='marcar_como_leida("+id+")' style='color:#D26464;' title='Marcar como leída' class='fa fa-check-circle-o'></i>";
            tabla+="</div>";
            tabla+="<div class='clearfix'> </div>";
          tabla+="</a></li>";             
                   
        }
      }
    }).done(function(){
      $('#contenidoNotificaciones').append(tabla)
    });
  };

  cargarNotificacionAlertas=function (){
    var tabla="";
    $.ajax({
      type:'GET',
      url:'/obtener_alertas_pedido',
      success:function(data)
      {
        var n_actuales=parseInt($('#totalNotificaciones').html() || 0);
        for (var i=0;i<data.length;i++){
          $('#totalNotificaciones').html(parseInt(data.length)+n_actuales);

          var id=data[i]['pk'];
          var pedido=data[i]['pedido__descripcion'];
          var usuario=data[i]['usuario__usuario'];
          var total=data[i]['pedido__total'];

          tabla+="<li><a href='#'>";   
            tabla+="<div class='user-new'>";
              tabla+="<p>Su pedido ha llegado</p>";
              tabla+="<span>"+pedido+"</span>";
            tabla+="</div>";
            tabla+="<div style='position:relative; left:-15px;' class='user-new-left'>";
              tabla+="<span>RD$ "+total+"</span>";
              tabla+="<i class='fa fa-info'></i>";
              tabla+="<i onclick='marcar_como_leida("+id+")' style='color:#D26464;' title='Marcar como leída' class='fa fa-check-circle-o'></i>";
            tabla+="</div>";
            tabla+="<div class='clearfix'> </div>";
          tabla+="</a></li>";             
                   
        }
      }
    }).done(function(){
      $('#contenidoNotificaciones').append(tabla)
    });
  };


  
  cargarNotificacionSugerencias=function (){
    var tabla="";
    $.ajax({
      type:'GET',
      url:'/obtener_sugerencias_notificacion',
      success:function(data)
      {
        var n_actuales=parseInt($('#totalNotificaciones').html() || 0);
        for (var i=0;i<data.length;i++){
          $('#totalNotificaciones').html(parseInt(data.length)+n_actuales);

          var id=data[i]['pk'];
          var nombre=data[i]['nombre'];
          var fecha=data[i]['fecha'];
                   
          tabla+="<li><a href='/vista_detalle/"+id+"'>";   
            tabla+="<div class='user-new'>";
              tabla+="<p>Tiene una sugerencia</p>";
              tabla+="<p>"+nombre+"</p>";
              tabla+="<span>"+fecha+"</span>";
            tabla+="</div>";
            tabla+="<div style='position:relative; left:-15px;' class='user-new-left'>";
              tabla+="<i class='fa fa-user-plus'></i>";
            tabla+="</div>";
            tabla+="<div class='clearfix'> </div>";
          tabla+="</a></li>"; 
          
        }
        
      }
    }).done(function(){
      $('#contenidoNotificaciones').append(tabla)
    });
  };
  cargar_notificacion_users_pedidos_despachados=function(){
    var tabla="";
    $.ajax({
      type:'GET',
      url:'/obtener_notificacion_users_pedido_despachado',
      success:function(data)
      {
        var n_actuales=parseInt($('#totalNotificaciones').html() || 0);
        for (var i=0;i<data.length;i++){
          $('#totalNotificaciones').html(parseInt(data.length)+n_actuales);

          var id=data[i]['pk'];
          var id_pedido=data[i]['pedido__pk'];
          var pedido=data[i]['pedido__descripcion'];
          var usuario=data[i]['usuario__usuario'];
          var total=data[i]['pedido__total'];


          var contacto=data[i]['contacto__nombre'];
          tabla+="<li><a href='#'>";   
            tabla+="<div class='user-new'>";
              tabla+="<p>"+contacto+" a despachado su Pedido</p>";
              tabla+="<span>"+pedido+"</span>";
            tabla+="</div>";
            tabla+="<div style='position:relative; left:-15px;' class='user-new-left'>";
              tabla+="<span>RD$ "+total+"</span>";
              tabla+="<i class='fa fa-user-plus'></i>";
              tabla+="<i onclick='marcar_como_leida("+id+")' style='color:#D26464;' title='Marcar como leída' class='fa fa-check-circle-o'></i>";
            tabla+="</div>";
            tabla+="<div class='clearfix'> </div>";
          tabla+="</a></li>";             
                   
        }
      }
    }).done(function(){
      $('#contenidoNotificaciones').append(tabla)
    }); 
  }
  cargar_notificacion_users_pedidos_llegados=function(){
    var tabla="";
    $.ajax({
      type:'GET',
      url:'/obtener_notificacion_users_pedido_llego',
      success:function(data)
      {
        var n_actuales=parseInt($('#totalNotificaciones').html() || 0);
        for (var i=0;i<data.length;i++){
          $('#totalNotificaciones').html(parseInt(data.length)+n_actuales);

          var id=data[i]['pk'];
          var id_pedido=data[i]['pedido__pk'];
          var pedido=data[i]['pedido__descripcion'];
          var usuario=data[i]['usuario__usuario'];
          var total=data[i]['pedido__total'];

          tabla+="<li><a href='#'>";   
            tabla+="<div class='user-new'>";
              tabla+="<p>Su Pedido ha llegado</p>";
              tabla+="<span>"+pedido+"</span>";
            tabla+="</div>";
            tabla+="<div style='position:relative; left:-15px;' class='user-new-left'>";
              tabla+="<span>RD$ "+total+"</span>";
              tabla+="<i class='fa fa-user-plus'></i>";
              tabla+="<i onclick='marcar_como_leida("+id+")' style='color:#D26464;' title='Marcar como leída' class='fa fa-check-circle-o'></i>";
            tabla+="</div>";
            tabla+="<div class='clearfix'> </div>";
          tabla+="</a></li>";             
                   
        }
      }
    }).done(function(){
      $('#contenidoNotificaciones').append(tabla)
    });    
  }
  cargar_notificacion_admins_pedidos_nuevos=function (){
    var tabla="";
    $.ajax({
      type:'GET',
      url:'/obtener_notificacion_admins_pedidos_nuevos',
      success:function(data)
      {
        var n_actuales=parseInt($('#totalNotificaciones').html() || 0);
        for (var i=0;i<data.length;i++){
          $('#totalNotificaciones').html(parseInt(data.length)+n_actuales);

          var id=data[i]['pk'];
          var id_pedido=data[i]['pedido__pk'];
          var pedido=data[i]['pedido__descripcion'];
          var total=data[i]['pedido__total'];

          tabla+="<li><a href='#'>";   
            tabla+="<div class='user-new'>";
              tabla+="<p>Pedido Nuevo</p>";
              tabla+="<i class='fa fa-cart-plus fa-4x' style='color:#393C73;' aria-hidden='true'></i>";
              tabla+="<span style='color:#4A5FA5;'>"+pedido+"</span>";                            
            tabla+="</div>";
            tabla+="<div style='position:relative; left:-15px;' class='user-new-left'>";
              tabla+="<span>RD$ "+total+"</span>";
              tabla+="<i onclick='marcar_como_leida("+id+")' style='color:#D26464;' title='Marcar como leída' class='fa fa-check-circle-o'></i>";
            tabla+="</div>";
            tabla+="<div class='clearfix'> </div>";
          tabla+="</a></li>";             
                   
        }
      }
    }).done(function(){
      $('#contenidoNotificaciones').append(tabla)
    });
  };
  cargar_notificacion_users_casi_cierre_contactos=function()
  {
    var tabla="";
    $.ajax({
      type:'GET',
      url:'/obtener_notificacion_users_casi_cierre_contactos',
      success:function(data)
      {
        var n_actuales=parseInt($('#totalNotificaciones').html() || 0);
        for (var i=0;i<data.length;i++){
          $('#totalNotificaciones').html(parseInt(data.length)+n_actuales);

          var id=data[i]['pk'];
          var fecha=data[i]['contacto__hasta'];
          var contacto=data[i]['contacto__nombre'];
          
          tabla+="<li><a href='#'>";   
            tabla+="<div class='user-new'>";
              tabla+="<p style='color:#DE3B31;'>El Contacto no estará disponible pronto</p>";
              tabla+="<i class='fa fa-calendar fa-4x' style='color:#70D342;' aria-hidden='true'></i>";
              tabla+="<span style='color:#4A5FA5;'>"+contacto+"</span>";                    
            tabla+="</div>";              
            tabla+="<div style='position:relative; left:-15px;' class='user-new-left'>";
              tabla+="<span>Fecha:"+fecha+"</span>";
              tabla+="<i onclick='marcar_como_leida("+id+")' style='color:#D26464;' title='Marcar como leída' class='fa fa-check-circle-o'></i>";              
            tabla+="</div>";
            tabla+="<div class='clearfix'> </div>";
          tabla+="</a></li>";            
                   
        }
      }
    }).done(function(){
      $('#contenidoNotificaciones').append(tabla)
    });
  }
  cargar_notificacion_admins_cierre_contactos=function(){
    var tabla="";
    $.ajax({
      type:'GET',
      url:'/obtener_notificacion_admins_cierre_contactos',
      success:function(data)
      {
        var n_actuales=parseInt($('#totalNotificaciones').html() || 0);
        for (var i=0;i<data.length;i++){
          $('#totalNotificaciones').html(parseInt(data.length)+n_actuales);

          var id=data[i]['pk'];
          var fecha=data[i]['contacto__hasta'];
          var contacto=data[i]['contacto__nombre'];
          
          tabla+="<li><a href='#'>";   
            tabla+="<div class='user-new'>";
              tabla+="<p style='color:#DE3B31;'>Contacto no disponible</p>";
              tabla+="<i class='fa fa-exclamation-circle fa-4x' style='color:#D34B42; aria-hidden='true'></i>";
              tabla+="<span style='color:#4A5FA5;'>"+contacto+"</span>";                    
            tabla+="</div>";

              
            tabla+="<div style='position:relative; left:-15px;' class='user-new-left'>";
              tabla+="<span>Fecha:"+fecha+"</span>";
              tabla+="<i onclick='marcar_como_leida("+id+")' style='color:#D26464;' title='Marcar como leída' class='fa fa-check-circle-o'></i>";             
            tabla+="</div>";
            tabla+="<div class='clearfix'> </div>";
          tabla+="</a></li>";            
                   
        }
      }
    }).done(function(){
      $('#contenidoNotificaciones').append(tabla)
    });
  }
  cargar_notificacion_admins_pedidos_llegados=function (){
    var tabla="";
    $.ajax({
      type:'GET',
      url:'/obtener_notificacion_admins_pedidos_llegados',
      success:function(data)
      {
        var n_actuales=parseInt($('#totalNotificaciones').html() || 0);
        for (var i=0;i<data.length;i++){
          $('#totalNotificaciones').html(parseInt(data.length)+n_actuales);

          var id=data[i]['pk'];
          var id_pedido=data[i]['pedido__pk'];
          var pedido=data[i]['pedido__descripcion'];
          var usuario=data[i]['usuario__usuario'];
          var total=data[i]['pedido__total'];

          tabla+="<li><a href='#'>";   
            tabla+="<div class='user-new'>";
              tabla+="<p>El pedido llegó</p>";
              tabla+="<i class='fa fa-check-square-o fa-4x' style='color:#D34B42; aria-hidden='true'></i>";
              tabla+="<span style='color:#4A5FA5;'>"+pedido+"</span>";              
            tabla+="</div>";
            tabla+="<div style='position:relative; left:-15px;' class='user-new-left'>";
              tabla+="<span>RD$ "+total+"</span>";
              tabla+="<i onclick='notificar("+id+")' style='color:#4A5FA5;' title='Notificar al usuario' class='fa fa-comments-o'></i>";
              tabla+="<i onclick='marcar_como_leida("+id+")' style='color:#D26464;' title='Marcar como leída' class='fa fa-check-circle-o'></i>";              
            tabla+="</div>";
            tabla+="<div class='clearfix'> </div>";
          tabla+="</a></li>";             
                   
        }
      }
    }).done(function(){
      $('#contenidoNotificaciones').append(tabla)
    });
  };
  crear_notificacion_pre_alerta_cierre_contacto=function(){
    var tabla="";
    $.ajax({
      type:'GET',
      url:'/crear_notificacion_pre_alerta_cierre_contacto'
    }).done(function(respuesta){
      if(respuesta=="SI"){}else{
        alertify.error("Error: "+respuesta);
      }
    });
  }
  crear_notificacion_cierre_contacto=function(){
    var tabla="";
    $.ajax({
      type:'GET',
      url:'/crear_notificacion_cierre_contacto'
    }).done(function(respuesta){
      if(respuesta=="SI"){}else{
        alertify.error("Error: "+respuesta);
      }
    });
  };

  

  cargarTODAS=function(){
    $('#contenidoNotificaciones').html('');
    $('#totalNotificaciones').html('');
    //Solo los administradores pueden ver las notificaciones de sugerencia
    if($('#txtRol').val()=="Administrador"){
      cargarNotificacionSugerencias();    
      cargar_notificacion_admins_pedidos_nuevos();
      cargar_notificacion_admins_pedidos_llegados();
      cargar_notificacion_admins_cierre_contactos();
    }
    cargarNotificacionAlertas();
    cargarNotificacionRecoger();
    cargar_notificacion_users_pedidos_llegados();    
    cargar_notificacion_users_casi_cierre_contactos();
    cargar_notificacion_users_pedidos_despachados();

  };

  var misNotificaciones;
  var misSugerencias;

  window.setInterval(function(){
    
    $.ajax({
      type:'GET',
      url:'/total_mis_notificaciones'
    }).done(function(respuesta){
      var total=parseInt(respuesta);      
      if(total>misNotificaciones){
        cargarTODAS();
        //Si los sonidos estan on, sonar la alarma
        if($('#txtNSonido').val()=="True"){
          //$('#audio_fb')[0].play();
          audio();
        }
        
      }
      misNotificaciones=parseInt(respuesta);  
      console.log("Notificaciones: "+misNotificaciones);
    });   
    if(!$('#txtRol').val()=="Contacto"){
      crear_notificacion_pre_alerta_cierre_contacto();
    }
    
    //Si soy administrador, compruebo las sugerencias
    if($('#txtRol').val()=="Administrador"){
      crear_notificacion_cierre_contacto();
      $.ajax({
        type:'GET',
        url:'/total_mis_sugerencias'
      }).done(function(respuesta){
        var total=parseInt(respuesta);      
        if(total>misSugerencias){
          cargarTODAS();
          //Si los sonidos estan on, sonar la alarma
          if($('#txtNSonido').val()=="True"){
            //$('#audio_fb')[0].play();      
            audio();
          } 
        }
        misSugerencias=parseInt(respuesta);
        console.log("Sugerencias: "+misSugerencias);   
      });
    }
                        
  }, 1000);

  cargarTODAS();

  
  $('#contenidoNotificaciones').append("<li><a href='/todas_notificaciones' class='view'>Ver todas las notificaciones</a></li>");

  

});


                               
                                    
                                    
                                    
                                    
                                    
                                
                                    
                                    
                                    
                                    
                                 
                                 
                               
                              