
$(document).on('ready',function(){

    if($('#txtRolUsuario').html()=="Contacto"){
        $('#abrirForm').hide();
    }else{
        $('#abrirForm').show();
    }
    $('#cambiarContacto').hide();

    
   if($('#txtRolUsuario').html()=="Administrador"){
        $('#form_equipos').show();
   }else{
        $('#form_equipos').hide();
   }
    
    
   $('#form_equipos').on('submit',function(e){
    e.preventDefault();
    if($('#cboEquipos').val()!="0"){
        mostrar($('#cboEquipos').val());
    }else{
        mostrar();
    }    
   });

    var id_contacto=0;
    $('#contenedorFormulario').hide();

    $('#btnCancelar').on('click',function(){
        $('#spinner_registro_pedido').html('');
        $('#btnAccion').html('<i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar');
        $('#btnAccion').addClass('btn-success');
        $('#btnAccion').removeClass('btn-warning');
        limpiar();
    });
    var n=0;
    $('#abrirForm').on('click',function(){
        if(n%2==0){
            $('#contenedorFormulario').fadeIn();
            $(this).html('Cerrar');
            $(this).removeClass('btn-success');
            $(this).addClass('btn-primary');
            $('#btnCancelar').click();
        }else{
            $('#contenedorFormulario').fadeOut();
            $(this).html('Realizar Pedido');
            $(this).removeClass('btn-primary');
            $(this).addClass('btn-success');
            $('#btnCancelar').click();
        }
        n++;
    });
    limpiar = function(){
        $("#conItbis" ).prop( "checked", false );
        $('#cambiarContacto').hide();
        id_contacto=0;
        $('#txtItbis,#txtSubTotal,#txtTotal').addClass('has-error');
        $('#txtItbis,#txtSubTotal,#txtTotal').removeClass('has-success');
        $('#txtItbis,#txtSubTotal,#txtTotal').val('');


        $('#contacto,#descripcion').val('');
        $('#categoria').val('0');
        $('#divContacto,#divDescripcion').removeClass('has-error');
        $('#divContacto,#divDescripcion').removeClass('has-success');
        $("#tblDetallePedido tbody tr").each(function (index) 
        {
            $(this).remove();
        });

        $('#contacto').prop('disabled', false);
    };
    $('#form_registro_pedidos').on('submit',function(e){
        e.preventDefault();
        var detalles=0;
        $("#tblDetallePedido tbody tr").each(function (index) 
        {
            detalles++;                                        
        });

        if($('#contacto').val()!="" &&
            $('#descripcion').val()!="" &&
            id_contacto!=0 &&
            detalles>0){
            $('#txtItbis,#txtSubTotal,#txtTotal').addClass('has-success');
            $('#txtItbis,#txtSubTotal,#txtTotal').removeClass('has-error');

            $('#divContacto, #divDescripcion').removeClass('has-error');
            $('#divContacto, #divDescripcion').addClass('has-success');

            var DATA=[];
            $("#tblDetallePedido tbody tr").each(function (index) 
            {
                var iditem,cantidad,precio,importe;
                $(this).children("td").each(function (index2) 
                {
                    switch (index2) 
                    {
                        case 0: iditem = $(this).html();
                                break;
                        case 2: cantidad = $(this).html();
                                break;
                        case 3: precio = $(this).html();
                                break;
                        case 4: importe = $(this).html();
                                break;                        
                    }
                })

                item={};
                item['iditem']=iditem;
                item['cantidad']=cantidad;
                item['precio']=precio;
                item['importe']=importe;               
                //Agrego la fila al conjunto de datos
                DATA.push(item);                               

            });
            var itbis="false";
            if($('#conItbis').is(":checked")){
                itbis="true";
            }
            $.ajax(
                {
                    type:'POST',
                    url:'/registro_pedido',
                    data:{
                        contacto:id_contacto,
                        descripcion:$('#descripcion').val(),
                        total:$('#txtTotal').val(),
                        itbis:itbis,
                        DETALLES: JSON.stringify(DATA),
                        csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val()
                    },
                    traditional: true,
                    beforeSend:function()
                    {
                        $('#spinner_registro_pedido').html('<i class="fa fa-spinner fa-spin fa-3x"></i>');
                    }
                }
            )
            .done(function(respuesta){
                $('#spinner_registro_pedido').html('');
                if (respuesta=="SI"){
                    alertify.log("Pedido registrado");
                    limpiar();
                    mostrar();
                    $('#contenedorFormulario').fadeOut();
                    $('#abrirForm').html('Realizar Pedido');
                    $('#abrirForm').removeClass('btn-primary');
                    $('#abrirForm').addClass('btn-success');
                    n++;
                }else if(respuesta=="Error1"){
                    alertify.error('Ocurrió un error en la creacion del pedido');
                }else if(respuesta=="Error2"){
                    alertify.error('Datos enviados por el método incorrecto');
                }else{
                    alertify.error("Error: "+ respuesta);
                }
            });
            }else{               

                //VALIDA LAS errores
                var errores="";

                if(detalles==0){
                    if(errores!=""){errores+="<br> • Debe introducir items en la lista";}
                    else
                        {errores+="• Debe introducir items en la lista";}
                }

                if($('#contacto').val()==""){
                    $('#divContacto').addClass('has-error');
                    $('#divContacto').removeClass('has-success');
                    if(errores!=""){errores+="<br>• Escriba el nombre del Contacto";}
                    else
                        {errores+="• Escriba el nombre del Contacto";}
                }else{
                    if(id_contacto==0){
                        if(errores!=""){errores+="<br>• El Contacto no es válido";}
                        else
                            {errores+="• El Contacto no es válido";}
                    }else{
                        $('#divContacto').removeClass('has-error');
                        $('#divContacto').addClass('has-success');
                    }                    
                }

                if($('#descripcion').val()==""){
                    $('#divDescripcion').addClass('has-error');
                    $('#divDescripcion').removeClass('has-success');
                    if(errores!=""){errores+=" <br> • Introduzca la Descripción";}
                    else 
                        {errores+="• Introduzca la Descripción";}
                }else{
                    $('#divDescripcion').removeClass('has-error');
                    $('#divDescripcion').addClass('has-success');
                }

                if(errores!=""){
                    alertify.error(errores);
                }
            }
        
    });
    recibir_pedido=function(id){
        alertify.set({ buttonReverse: true});
        alertify.set({ labels: {
            ok     : "Si",
            cancel : "No"
        } });
        alertify.confirm("Está seguro de marcar como recibido este pedido?", function (e) {
            if (e) {
                $('#spinner_registro_pedido').html('<h4>Recibiendo</h4><i class="fa fa-spinner fa-spin fa-3x"></i>');
                $.ajax(
                    {
                        type:'GET',
                        url:'/recibir_pedido/'+id
                    }
                )
                .done(function(respuesta){
                    if(respuesta=="SI"){
                        alertify.log("Pedido recibido correctamente");
                        mostrar();
                    }else if(respuesta=="NO"){
                        alertify.error("Ocurrió un error al recibir el Pedido");
                    }else{
                        alertify.error("Error: "+ respuesta);
                    }
                    $('#spinner_registro_pedido').html('');
                });
            }
        });
    };
    despachar_pedido=function(id){
        alertify.set({ buttonReverse: true});
        alertify.set({ labels: {
            ok     : "Si",
            cancel : "No"
        } });
        alertify.confirm("Está seguro de despachar este pedido?", function (e) {
            if (e) {
                $('#spinner_registro_pedido').html('<h4>Despachando</h4><i class="fa fa-spinner fa-spin fa-3x"></i>');
                $.ajax(
                    {
                        type:'GET',
                        url:'/despachar_pedido/'+id
                    }
                )
                .done(function(respuesta){
                    if(respuesta=="SI"){
                        alertify.log("Pedido despachado correctamente");
                        mostrar();
                    }else if(respuesta=="NO"){
                        alertify.error("Ocurrió un error al despachar el Pedido");
                    }else{
                        alertify.error("Error: "+ respuesta);
                    }
                    $('#spinner_registro_pedido').html('');
                });
            }
        });
    }
    duplicar_pedido=function(id){
        alertify.set({ buttonReverse: true});
        alertify.set({ labels: {
            ok     : "Si",
            cancel : "No"
        } });
        alertify.confirm("Está seguro de duplicar este pedido?", function (e) {
            if (e) {
                $('#spinner_registro_pedido').html('<h4>Duplicando</h4><i class="fa fa-spinner fa-spin fa-3x"></i>');
                $.ajax(
                    {
                        type:'GET',
                        url:'/duplicar_pedido/'+id
                    }
                )
                .done(function(respuesta){
                    if(respuesta=="SI"){
                        alertify.log("Pedido duplicado correctamente");
                        mostrar();
                    }else if(respuesta=="NO"){
                        alertify.error("Ocurrió un error al duplicar el Pedido");
                    }else{
                        alertify.error("Error: "+ respuesta);
                    }
                    $('#spinner_registro_pedido').html('');
                });
            }
        });
    };
    cancelar_pedido=function(id){
        alertify.set({ buttonReverse: true});
        alertify.set({ labels: {
            ok     : "Si",
            cancel : "No"
        } });
        alertify.confirm("Está seguro de cancelar este pedido?", function (e) {
            if (e) {
                $('#spinner_registro_pedido').html('<h4>Cancelando</h4><i class="fa fa-spinner fa-spin fa-3x"></i>');
                $.ajax(
                    {
                        type:'GET',
                        url:'/cancelar_pedido/'+id
                    }
                )
                .done(function(respuesta){
                    if(respuesta=="SI"){
                        alertify.log("Pedido cancelado correctamente");
                        mostrar();
                    }else if(respuesta=="NO"){
                        alertify.error("Ocurrió un error al cancelar el Pedido");
                    }else{
                        alertify.error("Error: "+ respuesta);
                    }
                    $('#spinner_registro_pedido').html('');
                });
            }
        });
    };    
    recibir_pedidoAutomatico=function(){
        $.ajax(
            {
                type:'GET',
                url:'/recibir_pedidos_automaticos/'
            }
        )
        .done(function(respuesta){
            if(respuesta=="SI"){
                alertify.log("Los pedidos antes de hoy fueron marcados como recibidos")
                mostrar();
            }else if(respuesta=="NO"){
                mostrar();
            }else{
                alertify.error("Error: "+ respuesta);
            }
            $('#spinner_registro_pedido').html('');
        });
    };
    recibir_pedidoAutomatico();

    ver_foto=function(idimagen){
        var ruta_foto=$('#fotoid'+idimagen).attr('ruta_foto');
        $('#imagenModal').attr('src',ruta_foto);
        $('#btnverFoto').click();
    }

    ver_pedido=function(idpedido){
        $.ajax({
            type:'GET',
            url:'/dameDetallesdePedido/'+idpedido            
        }).done(function(respuesta){
            var total=0;
            var DATA=[];                    
            for(var i=0;i<respuesta.length;i++){
                item={};
                item[0]=respuesta[i]['item__nombre'];
                item[1]=respuesta[i]['cantidad'];
                item[2]=respuesta[i]['precio'];
                item[3]=respuesta[i]['importe'];
                total+=parseFloat(respuesta[i]['importe']);
                DATA.push(item);
            }
            if(DATA.length>0){
                $('#btnVerDetalles').click();
                //Reiniciar plugin+
                
                $('#txtSubTotalDetalles').val(total);
                if(respuesta[0]['pedido__itbis']==true){
                    $('#txtItbisDetalles').val(total*0.18);
                    $('#txtTotalDetalles').val(total+(total*0.18));
                }else if(respuesta[0]['pedido__itbis']==false){
                    $('#txtItbisDetalles').val('0');
                    $('#txtTotalDetalles').val(total);
                }
                var oTable = $('#tablaVistaDetalles').dataTable();
                oTable.fnClearTable();
                oTable.fnAddData(DATA);
                oTable.fnDraw();   
            }else{
                alertify.error("Este pedido no tiene Detalles");
            }      
        });
    }
    notificar=function(idpedido){

        alertify.set({ buttonReverse: true});
        alertify.set({ labels: {
            ok     : "Si",
            cancel : "No"
        } });
        alertify.confirm("Está seguro de notificar al usuario que su pedido llego?", function (e) {
            if (e) {
                $('#spinner_registro_pedido').html('<h4>Enviando Alerta</h4><i class="fa fa-spinner fa-spin fa-3x"></i>');
                $.ajax({
                    type:'GET',
                    url:'/notificarPedido/'+idpedido
                }).done(function(respuesta){
                    $('#spinner_registro_pedido').html('');
                    if(respuesta=="SI"){
                        alertify.log("Alerta Enviada");
                    }else if(respuesta=="NOTIFICADO"){
                        alertify.error("Ya alertó este pedido");
                    }else if(respuesta=="NO"){
                        alertify.error("La alerta no pudo ser enviada");
                    }else{
                        alertify.error("Error: "+respuesta);
                    }
                });    
            }
        });      
    };
    alerta_recoger_pedido=function(idpedido){

        alertify.set({ buttonReverse: true});
        alertify.set({ labels: {
            ok     : "Si",
            cancel : "No"
        } });
        alertify.confirm("Está seguro de notificar que el pedido espera por ser recogido?", function (e) {
            if (e) {
                $('#spinner_registro_pedido').html('<h4>Notificando</h4><i class="fa fa-spinner fa-spin fa-3x"></i>');
                $.ajax({
                    type:'GET',
                    url:'/notificar_recoger_Pedido/'+idpedido
                }).done(function(respuesta){
                    $('#spinner_registro_pedido').html('');
                    if(respuesta=="SI"){
                        alertify.log("Notificación Enviada");
                    }else if(respuesta=="NOTIFICADO"){
                        alertify.error("Ya este pedido fue notificado");
                    }else if(respuesta=="NO"){
                        alertify.error("La Notificación no pudo ser enviada");
                    }else{
                        alertify.error("Error: "+respuesta);
                    }
                });    
            }
        });      
    };

    reporte_pedido=function(idpedido){
        alertify.set({ buttonReverse: true});
        alertify.set({ labels: {
            ok     : "Si",
            cancel : "No"
        } });
        alertify.confirm("Está seguro de generar un reporte de este pedido?", function (e) {
            if (e) {
                window.open('/reporte_pedido/'+idpedido);                 
            }
        });
    }
    
    mostrar=function(id_equipo="NINGUNO"){
        $.ajax({
            type:'GET',
            url:'/get_orders/'+id_equipo
        }).done(function(data){

            var DATA=[];
            for(var i=0;i<data.length;i++){
                ruta_foto="/media/"+data[i]['usuario__imagen'].substring(2);                            
                if(ruta_foto=="/media/"){
                    ruta_foto="/media/nofoto.png";
                }                                          
                var id = data[i]['pk'];
                var accion="<a href='#' onclick='ver_pedido("+id+")'><span style='color:#29C6CB; font-size:20px;' class='glyphicon glyphicon-list' title='Detalles'></span> </a>";
                var estado="";
                if(data[i]['estado']=="Activo"){
                    estado="<i style='color: #6fe04b;' class='fa fa-check-square fa-2x' aria-hidden='true'></i><h4>Activo</h4>";
                    if($('#txtRolUsuario').html()!="Contacto"){
                        accion+="<a href='#' onclick='recibir_pedido("+id+")'><span style='color:#ffc455; font-size:20px;' class='glyphicon glyphicon-ok' title='Recibir'></span> </a>";
                        accion+="<a href='#' onclick='cancelar_pedido("+id+")'><span style='color:#c72439; font-size:20px;' class='glyphicon glyphicon-remove' title='Cancelar'></span> </a>";
                        accion+="<a href='#' onclick='duplicar_pedido("+id+")'><span style='color:#42c7a2; font-size:20px;' class='glyphicon glyphicon-duplicate' title='Duplicar'></span> </a>";
                    }else{
                        //Si el pedido esta activo a los contactos les aparecera un boton, para indicar cuando fue despachado
                        accion+="<a href='#' onclick='despachar_pedido("+id+")'><span style='color:#C24441; font-size:20px;' class='glyphicon glyphicon-copy' title='Despachar'></span> </a>";
                    }                    
                }else if(data[i]['estado']=="Cancelado"){
                    accion+="<a href='#' onclick='duplicar_pedido("+id+")'><span style='color:#42c7a2; font-size:20px;' class='glyphicon glyphicon-duplicate' title='Duplicar'></span> </a>";
                    estado="<i style='color: #e04e5a;' class='fa fa-ban fa-2x' aria-hidden='true'></i><h4>Cancelado</h4>";
                }else if (data[i]['estado']=="Recibido"){
                    //Si esta recibido y es administrador le puedo mandar una notificacion al usuario
                    if($('#txtRolUsuario').html()=="Administrador"){
                        //accion+="<a href='#' onclick='notificar("+id+")'><span style='color:#FFC300; font-size:20px;' class='glyphicon glyphicon-envelope' title='Alertar'></span> </a>";
                        accion+="<a href='#' onclick='reporte_pedido("+id+")'><span style='color:#464646; font-size:20px;' class='glyphicon glyphicon-folder-open' title='Reporte'></span> </a>";
                    }                    
                    //accion+="<a href='#' onclick='alerta_recoger_pedido("+id+")'><span style='color:#0D4697; font-size:20px;' class='glyphicon glyphicon-gift' title='Notificar'></span> </a>";
                    //Si el usuario actual, no es contacto, me da la opcion de duplicarlo
                    if($('#txtRolUsuario').html()!="Contacto"){
                        accion+="<a href='#' onclick='duplicar_pedido("+id+")'><span style='color:#42c7a2; font-size:20px;' class='glyphicon glyphicon-duplicate' title='Duplicar'></span> </a>";
                    }
                    estado="<i style='color: #1c8bff;' class='fa fa-flag-checkered fa-2x' aria-hidden='true'></i><h4>Recibido</h4>";
                }
                fila={};
                fila[0]=data[i]['pk'];
                fila[1]=data[i]['descripcion'];
                fila[2]=data[i]['contacto__nombre'];
                fila[3]=data[i]['usuario__usuario'];
                fila[4]="<center><img width=50px src='"+ruta_foto+"' id='fotoid"+data[i]['pk']+"' onclick='ver_foto("+data[i]['pk']+")' ruta_foto='"+ruta_foto+"' class='img-responsive' alt='Cinque Terre'></center>";
                fila[5]=estado;
                fila[6]=data[i]['fecha'];
                fila[7]=data[i]['hora'];
                fila[8]=data[i]['usuario__direccion'];
                fila[9]=data[i]['total'];
                fila[10]=accion;
                DATA.push(fila);

            }
            var oTable=$('#tblPedidos').dataTable();
            oTable.fnClearTable();
            if(DATA.length>0){
                oTable.fnAddData(DATA);
                oTable.fnDraw();
            }else{
                oTable.fnDraw();
            }
        });
    };
    mostrar();

    

    var nombres = [];
    var ids = [];
    $.ajax(
        {
            type:'GET',
            url:'/obtenerNombrecontactos/',
            success:function(data)
            {
                for(var i=0;i<data.length;i++) {
                    nombres.push(data[i]['nombre']);
                    ids.push(data[i]['pk']);
                }
            }
        }
    );
    $.ajax(
        {
            type:'GET',
            url:'/contarContactosDisponibles',
            success:function(data)
            {
                $('#cant_contactos_avaible').html(data);
            }
        }
    );
    $('#contacto').autoComplete({
        minChars: 1,
        source: function(term, suggest) {
            term = term.toLowerCase();
            var suggestions = [];
            for (i = 0; i < nombres.length; i++)
                if (~nombres[i].toLowerCase().indexOf(term))  suggestions.push(nombres[i]);
            suggest(suggestions);
        }
    });
    
    $('#contacto').keypress(function(){
        for(var i=0;i < nombres.length;i++){
            if ($(this).val()==nombres[i]){
                id_contacto=ids[i];
                console.log("FIJADO");               
                //Se bloquea esto
                $(this).prop('disabled', true);
                $('#cambiarContacto').show();
                break;
            }else{
                id_contacto=0;
                $(this).prop('disabled', false);
                $('#cambiarContacto').hide();
            }
        }
        console.log("el id es: "+id_contacto);
    });
    $('#cambiarContacto').on('click',function(){
        id_contacto=0;
        $('#contacto').prop('disabled', false);
        $(this).hide();
        $('#contacto').val('');
        $('#divContacto').removeClass('has-success');
        $("#tblDetallePedido tbody tr").each(function (index) 
        {
            $(this).remove();
        });
        $('#txtItbis,#txtSubTotal,#txtTotal').addClass('has-error');
        $('#txtItbis,#txtSubTotal,#txtTotal').removeClass('has-success');
        $('#txtItbis,#txtSubTotal,#txtTotal').val('');
    });
    agregar=function(iditem){
        var idItem=$('#divIditems'+iditem).html();

        var agregar=false;        
        var details=0;
        $('.divIditemsClase').each(function(index){
            details++;   

            if($(this).html()==idItem){
                alertify.log("Este Item ya está en la lista");
                agregar=false;
                return false;
            }else{
                agregar=true;                                 
            }
        });
        if(details==0){
            agregar=true;      
        }
        if(agregar==true){
            var categoria=$('#divCategoria'+iditem).html();
            var nombre=$('#divNombre'+iditem).html();
            var precio=$('#divPrecio'+iditem).html();
            var cantidad=parseInt($('#InputCantidad'+iditem).val());
            
            if(cantidad>0){
                var tr="";
                tr+="<tr id='TR"+idItem+"'>"+
                "<td class ='divIditemsClase text-center'>"+idItem+"</td>"+
                "<td class='text-center' id='TDNOMBRE"+idItem+"'>"+nombre+"</td>"+
                "<td class='text-center' id='TDCANTIDAD"+idItem+"'>"+cantidad+"</td>"+
                "<td class='text-center' id='TDPRECIO"+idItem+"'>"+precio+"</td>"+
                "<td class='text-center' id='TDIMPORTE"+idItem+"'>"+(cantidad*precio)+"</td>"+
                "<td class='text-center'>"+
                    "<a onclick='borrarTr("+idItem+")' class='btn btn-danger'><span class='glyphicon glyphicon-remove-circle'></span></a>"+
                    "<a onclick='subirCantidad("+idItem+")' class='btn btn-primary'><span class='glyphicon glyphicon-plus'></span></a>"+
                    "<a onclick='bajarCantidad("+idItem+")' class='btn btn-warning'><span class='glyphicon glyphicon-minus'></span></a>"+                        
                "</td>"+          
                "</tr>";
                $('#contenidoDetalles').append(tr);
                $('#divCantidad'+iditem).removeClass('has-error');   
                $('#InputCantidad'+iditem).val('');   
                resetModalItems();
                totalizar(); 
            }else{
                $('#divCantidad'+iditem).addClass('has-error');            
            }
        }

        
    };
    $('#btnVolverAMenu').on('click',function(){
        //Cierro el modal de Items
        $('#modalItems').modal('toggle');
        resetModalMenus();
        $('#btnDispararModalMenus').click();
    });
    subirCantidad=function(idItem){
        var cantidad=parseInt($('#TDCANTIDAD'+idItem).html());
        cantidad+=1;
        var precio=parseFloat($('#TDPRECIO'+idItem).html());
        var importe=parseFloat(cantidad*precio);
        $('#TDIMPORTE'+idItem).html(importe);
        $('#TDCANTIDAD'+idItem).html(cantidad);
        totalizar();
    };
    bajarCantidad=function(idItem){
        var cantidad=parseInt($('#TDCANTIDAD'+idItem).html());
        if(cantidad>1){
            cantidad-=1;
            var precio=parseFloat($('#TDPRECIO'+idItem).html());
            var importe=parseFloat(cantidad*precio);
            $('#TDIMPORTE'+idItem).html(importe);
            $('#TDCANTIDAD'+idItem).html(cantidad);
            totalizar();
        }else{
            alertify.log("El mínimo es un (1) ítem");
        }
        
    }
    borrarTr=function(idItem){
        $('#TR'+idItem).remove();
        totalizar();
    };
    $('#conItbis').on('change',function(){
        totalizar();
    });
    totalizar=function(){
        var importe=0;
        $("#tblDetallePedido tbody tr").each(function (index) 
        {
            var precio=0, cantidad=0;
            $(this).children("td").each(function (index2) 
            {
                switch (index2) 
                {
                    case 2: cantidad = $(this).html();
                            break;
                    case 3: precio = $(this).html();
                            break;
                }               
            })
            importe+=parseFloat(precio)*parseFloat(cantidad);            
        });
        $('#txtSubTotal').val(importe);
        
        if($('#conItbis').is(":checked")){
            $('#txtItbis').val(importe*0.18);
            $('#txtTotal').val(importe+importe*0.18);
        }else{
            $('#txtItbis').val('');
            $('#txtTotal').val(importe);
        }
    }
    function resetModalItems() {
        var table = $('#tablaItems').DataTable();
        table
         .search( '' )
         .columns().search( '' )
         .draw();                       
    }
    function resetModalMenus() {
        var table = $('#tablaMenus').DataTable();
        table
         .search( '' )
         .columns().search( '' )
         .draw();                       
    }
    verItems_del_menu=function(id_menu){
        //Procedo a seleccionar los items de este menú
        $.ajax({
            type:'GET',
            url:'/dameLosItemsMenu/'+id_menu,
            success:function(data)
            {
                console.log(data);
                var DATA=[];                    
                for(var i=0;i<data.length;i++){
                    item={};
                    var id_item=data[i]['pk'];
                    var categoria=data[i]['CatMenu__nombre'];
                    var nombre=data[i]['nombre'];
                    var precio=data[i]['precio'];

                    item[0]="<div id='divIditems"+id_item+"'>"+id_item+"</div>";
                    item[1]="<div id='divCategoria"+id_item+"'>"+categoria+"</div>";
                    item[2]="<div id='divNombre"+id_item+"'>"+nombre+"</div>";
                    item[3]="<div id='divPrecio"+id_item+"'>"+precio+"</div>";

                    item[4]="<div id='divCantidad"+id_item+"' class='form-group'>"+
                               "<input id='InputCantidad"+id_item+"' type='text' class='form-control' style='text-align:center;'>"+
                               "</div>";

                    item[5]="<a onclick='agregar("+id_item+")' ><i style='color:#C70039;' class='fa fa-cart-plus fa-3x' aria-hidden='true'></i></a>";
                    DATA.push(item);
                }      

                if(DATA.length>0){
                    //Cierro el modal de menús
                    $('#modalMenus').modal('toggle');

                    //Reiniciar plugin+
                    var oTable = $('#tablaItems').dataTable();
                    oTable.fnClearTable();
                    oTable.fnAddData(DATA);
                    oTable.fnDraw();         

                    $('#btnDispararModalItems').click();
                }else{
                    alertify.error("Este Menú no tiene Items");
                } 
                
            }
        })
    }
    $('#btnBuscarItems').on('click',function(){
        if(id_contacto==0){
            alertify.error("Debe especificar el contacto");
        }else{
            //Procedo a seleccionar los menús de este contacto
            $.ajax({
                type:'GET',
                url:'/dameLosMenusContacto/'+id_contacto,
                success:function(data)
                {
                    var DATA=[];                    
                    for(var i=0;i<data.length;i++){
                        item={};
                        var id_menu=data[i]['pk'];
                        item[0]=id_menu;
                        item[1]=data[i]['nombre'];
                        item[2]="<a onclick='verItems_del_menu("+id_menu+")' ><i style='color:#FFC300;' class='fa fa-glass fa-2x' aria-hidden='true'></i></span></a>";
                        DATA.push(item);
                    }
                    if(DATA.length>0){//Compruebo que este contacto tenga menús
                        //Reiniciar plugin+
                        var oTable = $('#tablaMenus').dataTable();
                        oTable.fnClearTable();
                        oTable.fnAddData(DATA);
                        oTable.fnDraw();       
                        $('#btnDispararModalMenus').click();
                    }else{
                        alertify.error("Este contacto no tiene Menús");
                    }                 

                    
                }

            });
        }
    });
});
