$(document).on('ready',function(){
    borrar_sugerencia=function(id){
            alertify.set({ buttonReverse: true });
            alertify.set({ labels: {
                ok     : "Si",
                cancel : "No"
            } });
            alertify.confirm("Está seguro de eliminar esta sugerencia?", function (e) {
                if (e) {
                    $('#spinner_carga_sugerencias').html('<h4>Eliminando</h4><i class="fa fa-spinner fa-spin fa-3x"></i>');
                    $.ajax(
                        {
                            type:'GET',
                            url:'/eliminar_sugerencia/'+id
                        }
                    )
                    .done(function(respuesta){
                        $('#spinner_carga_sugerencias').html('');
                        if(respuesta=="SI"){
                            alertify.log("Sugerencia eliminada correctamente");
                            mostrar();
                        }else if(respuesta=="NO"){
                            alertify.error("Ocurrió un error al eliminar la sugerencia");
                        }else{
                             alertify.error("Error: "+respuesta);
                        }
                    });
                }
            });
        };
    aceptar_sugerencia=function(id){
            alertify.set({ buttonReverse: true });
            alertify.set({ labels: {
                ok     : "Si",
                cancel : "No"
            } });
            alertify.confirm("Está seguro de aplicar esta sugerencia?", function (e) {
                if (e) {
                    $('#spinner_carga_sugerencias').html('<h4>Aplicando sugerencia</h4><i class="fa fa-spinner fa-spin fa-3x"></i>');
                    $.ajax(
                        {
                            type:'GET',
                            url:'/administracion/aceptar_sugerencia/'+id
                        }
                    )
                    .done(function(respuesta){
                        $('#spinner_carga_sugerencias').html('');
                        if(respuesta=="SI"){
                            alertify.log("Sugerencia aplicada correctamente");
                            mostrar();
                        }else if(respuesta=="Error1"){
                            alertify.error("Ocurrió un error al aplicar la sugerencia");
                        }else{
                            alertify.error("Error: "+ respuesta);
                        }
                    });
                }
            });
        };

    ver_foto=function(idimagen){
        var ruta_foto=$('#fotoid'+idimagen).attr('ruta_foto');
        $('#imagenModal').attr('src',ruta_foto);
        $('#btnverFoto').click();
    }
    mostrar=function(){
        $.ajax({
            type:'GET',
            url:'/get_suggestions'
        }).done(function(respuesta){
            var DATA=[];
            for(var i=0;i<respuesta.length;i++){
                ruta_foto="/media/"+respuesta[i]['usuario__imagen'].substring(2);
                            
                if(ruta_foto=="/media/"){
                    ruta_foto="/media/nofoto.png";
                }


                var id = respuesta[i]['pk'];
                var codigo_Poderoso= "<a href='#' onclick='borrar_sugerencia("+id+")'><span style='color:#e03e3c; font-size:20px;' class='glyphicon glyphicon-trash'></span> </a>";
                codigo_Poderoso += "<a href='#' onclick='aceptar_sugerencia("+id+")'><span style='color:#7de040; font-size:20px;' class='glyphicon glyphicon-ok-circle'></span> </a>";
                var aplicada="<i style='color:#bae63c;' class='fa fa-check-square' aria-hidden='true'></i>";
                if(respuesta[i]['aplicada']==true){
                    codigo_Poderoso="<h5>Ya está aplicada</h5>"
                }
                if (respuesta[i]['aplicada']==false){
                    aplicada="<i style='color:#c5454e;' class='fa fa-clock-o' aria-hidden='true'></i>"
                }
                fila={};
                fila[0]=id;
                fila[1]=respuesta[i]['nombre'];
                fila[2]=respuesta[i]['telefono'];
                fila[3]=respuesta[i]['direccion'];
                fila[4]=respuesta[i]['categoria__nombre'];
                fila[5]=respuesta[i]['usuario__usuario'];
                fila[6]="<center><img width=50px src='"+ruta_foto+"' id='fotoid"+respuesta[i]['pk']+"' onclick='ver_foto("+respuesta[i]['pk']+")' ruta_foto='"+ruta_foto+"' class='img-responsive' alt='Cinque Terre'></center>";
                fila[7]=aplicada;
                fila[8]=respuesta[i]['fecha'];
                fila[9]=codigo_Poderoso;
                DATA.push(fila);
            }
            var oTable=$('#tblSugerencias').dataTable();
            oTable.fnClearTable();
            if(DATA.length>0){
                oTable.fnAddData(DATA);
                oTable.fnDraw();
            }else{
                oTable.fnDraw();
            }
        });
    }
    mostrar();

    
});