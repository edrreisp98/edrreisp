$(document).on('ready',function(){
    
    if($('#rolUser').html()=="Administrador" || $('#rolUser').html()=="Developer" || $('#rolUser').html()=="Invitado"){
        $('#abrirForm').hide();
    }

    $('#contenedorFormulario').hide();
    limpiar = function(){
        $('#equipo,#equipo2').val('');
        $('#divEquipo').removeClass('has-error');
    };
    borrar_equipo=function(id){
        alertify.set({ buttonReverse: true});
        alertify.set({ labels: {
            ok     : "Si",
            cancel : "No"
        } });
        alertify.confirm("Está seguro de eliminar este grupo?", function (e) {
            if (e) {
                $('#spinner_registro_equipo').html('<h4>Eliminando</h4><i class="fa fa-spinner fa-spin fa-3x"></i>');
                $.ajax(
                    {
                        type:'GET',
                        url:'/eliminar_equipo/'+id
                    }
                )
                .done(function(respuesta){
                    if(respuesta=="SI"){
                        alertify.log("Equipo eliminado correctamente");
                        mostrar();
                    }else if(respuesta=="NO"){
                        alertify.error("Ocurrió un error al eliminar el Equipo");
                    }else{
                        alertify.error("Error: "+ respuesta);
                    }
                    $('#spinner_registro_equipo').html('');
                });
            }
        });
    };
    modificar_equipo=function(id){
        $('#spinner_registro_equipo').html('<h4>Obteniendo datos</h4><i class="fa fa-spinner fa-spin fa-3x"></i>');
        $.ajax(
            {
                type:'GET',
                url:'/obtener_datos_equipo/'+id,
                success:function(data)
                {
                    for(var i=0;i<data.length;i++){
                        $('#equipo2').val(data[i]['pk']);
                        $('#equipo').val(data[i]['fields']['nombre']);
                    }
                    $('#spinner_registro_equipo').html('');
                }
            }
      );
        $('#btnAccion').html('Guardar Cambios');
        $('#btnAccion').removeClass('btn-success');
        $('#btnAccion').addClass('btn-warning');

        $('#contenedorFormulario').fadeIn();
        $('#abrirForm').html('Cerrar');
        $('#abrirForm').removeClass('btn-success');
        $('#abrirForm').addClass('btn-primary');
        n++;
    };
    $('#btnCancelar').on('click',function(){
        $('#spinner_registro_equipo').html('');
        $('#btnAccion').html('Guardar');
        $('#btnAccion').addClass('btn-success');
        $('#btnAccion').removeClass('btn-warning');
        limpiar();
    });
    var n=0;
    $('#abrirForm').on('click',function(){
        if(n%2==0){
            $('#contenedorFormulario').fadeIn();
            $(this).html('Cerrar');
            $(this).removeClass('btn-success');
            $(this).addClass('btn-primary');
            $('#btnCancelar').click();
        }else{
            $('#contenedorFormulario').fadeOut();
            $(this).html('Agregar Equipo');
            $(this).removeClass('btn-primary');
            $(this).addClass('btn-success');
            $('#btnCancelar').click();
        }
        n++;
    });
    $('#form_registro_equipos').on('submit',function(e){
        e.preventDefault();
        var texto=$('#btnAccion').html();
        if(texto=="Guardar"){
            if($('#equipo').val()!=""){
                $('#divEquipo').removeClass('has-error');
                $.ajax(
                    {
                        type:'POST',
                        url:'/registro_equipos',
                        data:
                        {
                            equipo:$('#equipo').val(),
                            csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
                        },
                        beforeSend:function()
                        {
                            $('#spinner_registro_equipo').html('<i class="fa fa-spinner fa-spin fa-3x"></i>');
                        }
                    }
                )
                .done(function(respuesta){
                    $('#spinner_registro_equipo').html('');
                    if(respuesta=="SI"){
                        alertify.log("Equipo registrado");
                        limpiar();
                        mostrar();
                        $('#contenedorFormulario').fadeOut();
                        $('#abrirForm').html('Agregar Equipo');
                        $('#abrirForm').removeClass('btn-primary');
                        $('#abrirForm').addClass('btn-success');
                        n++;
                    }else if(respuesta=="Error1"){
                        alertify.error("Ocurrió un error en la creación del equipo");
                    }else if(respuesta=="Error2"){
                        alertify.error("Datos enviados por el método incorrecto");
                    }else{
                        alertify.error("Error: "+respuesta);
                        console.log(respuesta);
                    }
                });
            }else{
                //VALIDA LAS errores
              var errores="";
              if($('#equipo').val()==""){
                  $('#divEquipo').addClass('has-error');
                  if(errores!=""){errores+="<br>• Introduzca el nombre del Equipo";}
                    else
                        {errores+="• Introduzca el nombre del Equipo";}
              }else{
                  $('#divEquipo').removeClass('has-error');
              }
               if(errores!=""){
                   alertify.error(errores);
               }
            }

        }else if(texto=="Guardar Cambios"){
            if($('#equipo').val()!="" && $('#equipo2').val()!=""){
                $('#divEquipo').removeClass('has-error');
                $.ajax(
                    {
                        type:'POST',
                        url:'/modifica_equipos',
                        data:
                        {
                            equipo2:$('#equipo2').val(),
                            equipo:$('#equipo').val(),
                            csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
                        },
                        beforeSend:function()
                        {
                            $('#spinner_registro_equipo').html('<i class="fa fa-spinner fa-spin fa-3x"></i>');
                        }
                    }
                )
                .done(function(respuesta){
                    $('#spinner_registro_equipo').html('');
                    if(respuesta=="SI"){
                        alertify.log("Equipo Modificado");
                        limpiar();
                        mostrar();
                        $('#btncancelar').click();
                        $('#contenedorFormulario').fadeOut();
                        $('#abrirForm').html('Agregar Equipo');
                        $('#abrirForm').removeClass('btn-primary');
                        $('#abrirForm').addClass('btn-success');
                        n++;
                    }else if(respuesta=="Error1"){
                        alertify.error("Ocurrió un error en la modificación del equipo");
                    }else if(respuesta=="Error2"){
                        alertify.error("Datos enviados por el método incorrecto");
                    }else{
                        alertify.error("Error: "+respuesta);
                    }
                });
            }else{
                //VALIDA LAS errores
                  var errores="";
                  if($('#equipo').val()==""){
                      $('#divEquipo').addClass('has-error');
                      if(errores!=""){errores+="<br> • Introduzca el nombre del Equipo";}
                        else
                            {errores+=" • Introduzca el nombre del Equipo";}
                  }else{
                      $('#divEquipo').removeClass('has-error');
                  }
                   if(errores!=""){
                       alertify.error(errores);
                   }
            }
        }
    });
    mostrar=function(){
        $.ajax({
            type:'GET',
            url:'/get_teams_ff'
        }).done(function(data){
            var DATA=[];
            for(var i=0;i<data.length;i++){
                
                var id = data[i]['pk'];
                var acciones="";
                if($('#rolUser').html()=="Administrador") {
                    acciones += "<a href='#' onclick='borrar_equipo(" + id + ")'><span style='color:#c72439; font-size:20px;' class='glyphicon glyphicon-trash' title='Borrar Equipo'></span></a>";
                    acciones += "<a href='/ver_equipo/" + id + "'><span style='color:#5ea1c7; font-size:20px;' class='glyphicon glyphicon-eye-open' title='Ver Equipo'></span></a>";
                }else if($('#rolUser').html()=="Developer" || $('#rolUser').html()=="Invitado"){
                    acciones+="<a href='/ver_equipo/"+id+"'><span style='color:#5ea1c7; font-size:20px;' class='glyphicon glyphicon-eye-open' title='Ver Equipo'></span></a>";
                }else if($('#rolUser').html()=="Firefighter"){
                    if(data[i]['usuario_creacion']==$('#nombreUsuario_').html()){
                        acciones="<a href='#' onclick='modificar_equipo("+id+")'><span style='color:#ffc455; font-size:20px;' class='glyphicon glyphicon-pencil' title='Modificar Equipo'></span> </a>";
                        acciones+="<a href='#' onclick='borrar_equipo("+id+")'><span style='color:#c72439; font-size:20px;' class='glyphicon glyphicon-trash' title='Borrar Equipo'></span></a>";
                    }
                    acciones+="<a href='/ver_equipo/"+id+"'><span style='color:#5ea1c7; font-size:20px;' class='glyphicon glyphicon-eye-open' title='Ver Equipo'></span></a>";
                }
                if(id==1){
                    acciones="<a href='/ver_equipo/"+id+"'><span style='color:#5ea1c7; font-size:20px;' class='glyphicon glyphicon-eye-open' title='Ver Equipo'></span></a>";
                }


                fila={};
                fila[0]=data[i]['pk'];
                fila[1]=data[i]['nombre'];
                fila[2]=data[i]['usuario_creacion'];
                fila[3]=acciones;
                DATA.push(fila);                
                
            }
            var oTable=$('#tblEquipos').dataTable();
            oTable.fnClearTable();
            if(DATA.length>0){
                oTable.fnAddData(DATA);
                oTable.fnDraw();
            }else{
                oTable.fnDraw();
            }
        });
    }
    mostrar();

    
});