$(document).on('ready',function(){
	ver_foto=function(idimagen){
        var ruta_foto=$('#fotoid'+idimagen).attr('ruta_foto');
        $('#imagenModal').attr('src',ruta_foto);
        $('#btnverFoto').click();
    }
	  
    resetear_monto=function(id){
    	 alertify.set({ buttonReverse: true });
        alertify.set({ labels: {
            ok     : "Si",
            cancel : "No"
        } });
        alertify.confirm("Está seguro de resetear el monto a 0?", function (e) {
            if (e) {
            	$('#spinner_carga_sugerencias').html('<h4>Reseteando Monto</h4><i class="fa fa-spinner fa-spin fa-3x"></i>');
                $.ajax(
                    {
                        type:'GET',
                        url:'/resetear_monto/'+id
                    }
                )
                .done(function(respuesta){
                	$('#spinner_carga_sugerencias').html('');
                    if(respuesta=="SI"){
                        alertify.log("Monto reseteado correctamente");
                        mostrar();
                    }else if(respuesta=="NO"){
                        alertify.error("Ocurrió un error al resetear el monto");
                    }else{
                         alertify.error("Error: "+respuesta);
                    }
                });
            }
        });
    }
    $('#btnPagarMonto').on('click',function(){
      if($('#txtmonto').val()>0){
        $.ajax({
          type:'GET',
          url:'/actualizar_monto/'+$('#txtidmonto').val()+'/'+$('#txtmonto').val()
        }).done(function(respuesta){
          if(respuesta=="SI"){
            alertify.log("Monto actualizado");
            mostrar();         
          }else if(respuesta=="NO"){
            alertify.error("El monto no puede ser actualizado");
          }else{
            alertify.error("Error: "+respuesta);
          }
          $('#txtmonto,#txtidmonto').val('');
          $('#modalPago').modal('toggle');
        });
      }else{
        alertify.error("El monto a saldar debe ser mayor a 0");
      }
      
      
    });
    actualizar_monto=function(id){
      alertify.set({ buttonReverse: true });
      alertify.set({ labels: {
          ok     : "Si",
          cancel : "No"
      } });
      alertify.confirm("Está seguro de pagar el monto ?", function (e) {
      if (e) {
          $('#txtidmonto').val(id);
          $('#btnmodalPago').click();
        }
      });
    }
    mostrar=function(){
        $.ajax({
            type:'GET',
            url:'/get_debts'
        }).done(function(respuesta){
            var DATA=[];
            for(var i=0;i<respuesta.length;i++){
                
                ruta_foto="/media/"+respuesta[i]['usuario__imagen'].substring(2);                            
                if(ruta_foto=="/media/"){
                    ruta_foto="/media/nofoto.png";
                }
                var id = respuesta[i]['pk'];                  
                if (respuesta[i]['aplicada']==false){
                    aplicada="<i style='color:#c5454e;' class='fa fa-clock-o' aria-hidden='true'></i>"
                }
                var codigo_Poderoso= "<a href='#' onclick='resetear_monto("+id+")'><span style='color:#FFC300; font-size:20px;' class='glyphicon glyphicon-refresh'></span> </a>";
                codigo_Poderoso+="<a href='#' onclick='actualizar_monto("+id+")'><span style='color:#E03D3D; font-size:20px;' class='glyphicon glyphicon-briefcase'></span> </a>";
                
                fila={};
                fila[0]=respuesta[i]['usuario__usuario'];
                fila[1]="<center><img width=50px src='"+ruta_foto+"' id='fotoid"+respuesta[i]['pk']+"' onclick='ver_foto("+respuesta[i]['pk']+")' ruta_foto='"+ruta_foto+"' class='img-responsive' alt='Cinque Terre'></center>";
                fila[2]=respuesta[i]['total'];
                fila[3]=codigo_Poderoso;
                DATA.push(fila);
            }
            var oTable=$('#tblMontos').dataTable();
            oTable.fnClearTable();
            if(DATA.length>0){
                oTable.fnAddData(DATA);
                oTable.fnDraw();
            }else{
                oTable.fnDraw();
            }
        });
    }
    mostrar();
    
});