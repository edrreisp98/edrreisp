$(document).on('ready',function(){
    if($('#idVerEquipo').html()==1 || $('#rolUser').html()=="Developer"){
        $('#abrirForm').hide();
    }

    if($('#u_creacion').html()==$('#nombreUsuario_').html()){
        $('#abrirForm').show();
    }else{
        if($('#rolUser').html()=="Administrador" && $('#idVerEquipo').html()!=1){
            $('#abrirForm').show();
        }else{
            $('#abrirForm').hide();
        }
    }
    $('#contenedorFormulario').hide();
    limpiar=function(){
      $('#cboUsuarios').val(0);  
    };
    add_to_group=function(id,nombre){
        cargar_equipos_modal(id,nombre);    
    };
    reiniciar_modal_user=function(){
        $('#modalMasEquipos').modal('toggle');
        $('#nombreUsuario,#contenidoEquiposAgregar').html('');
    }
    agrega_equipo=function(idequipo,idusuario){
        $.ajax(
            {
                type:'GET',
                url:'/oye_ponlo_aqui/'+idusuario+'/'+idequipo,
            }
        ).done(function(respuesta){
            if(respuesta=="SI"){
                alertify.log("Usuario agregado al equipo");
                cargar_usuarios();
                mostrar_usuarios($('#idVerEquipo').html());
                reiniciar_modal_user();
            }else if(respuesta=="YA_ESTA_EN_EQUIPO"){
                alertify.error("Este usuario ya se encuentra en otro equipo");
                reiniciar_modal_user();
            }else{
                alertify.error(respuesta);//"El Usuario no pudo ser agregado al equipo");
                reiniciar_modal_user();
            }
        });
    }
    cargar_equipos_modal=function(idusuario,nombre){
        //Traeme todos los equipos si soy admin, y si soy FF solo traeme mis equipos
        $.ajax({
            type:'GET',
            url:'/equipos_del_modal'
        }).done(function(respuesta){
            var DATA=[];
            for(var i=0;i<respuesta.length;i++){
                item={};
                item[0]=respuesta[i]['pk'];
                item[1]=respuesta[i]['nombre'];
                //le paso el idequipo y le idusuario
                item[2]="<a onclick='agrega_equipo("+respuesta[i]['pk']+","+idusuario+")'><i style='color:#FFC300;' class='fa fa-check-square fa-3x' aria-hidden='true'></i></span></a>";
                DATA.push(item);
            }
            if(DATA.length>0){
                $('#nombreUsuario').html(nombre);
                $('#btnVerEquiposModal').click();
                //Reiniciar plugin+
                var oTable = $('#tablaModal').dataTable();
                oTable.fnClearTable();
                oTable.fnAddData(DATA);
                oTable.fnDraw();
            }else{
                alertify.log("No hay equipos disponibles");
            }

        });
    }
    sacar_del_equipo=function(id){
        alertify.set({ buttonReverse: true});
        alertify.set({ labels: {
            ok     : "Si",
            cancel : "No"
        } });
        alertify.confirm("Está seguro de sacar a este usuario de este equipo?", function (e) {
            if (e) {
                $('#spinner_agregar_usuario').html('<h4>Sacando</h4><i class="fa fa-spinner fa-spin fa-3x"></i>');
                $.ajax(
                    {
                        type:'GET',
                        url:'/sacar_del_equipo/'+id+'/'+$('#idVerEquipo').html()
                    }
                )
                .done(function(respuesta){
                    if(respuesta=="SI"){
                        alertify.log("Usuario sacado del equipo");
                        cargar_usuarios();
                        mostrar_usuarios($('#idVerEquipo').html());
                    }else if(respuesta=="NO"){
                        alertify.error("Ocurrió un error al sacar el usuario del Equipo");
                    }else{
                        alertify.error("Error: "+ respuesta);
                    }
                    $('#spinner_agregar_usuario').html('');
                });
            }
        });
    };
    $('#btnCancelar').on('click',function(){
        $('#spinner_agregar_usuario').html('');
        $('#btnAccion').html('Agregar');
        $('#btnAccion').addClass('btn-success');
        $('#btnAccion').removeClass('btn-warning');
        limpiar();
    });
    var n=0;
    $('#abrirForm').on('click',function(){
        if(n%2==0){
            $('#contenedorFormulario').fadeIn();
            $(this).html('Cerrar');
            $(this).removeClass('btn-success');
            $(this).addClass('btn-primary');
            $('#btnCancelar').click();
        }else{
            $('#contenedorFormulario').fadeOut();
            $(this).html('Agregar Usuario');
            $(this).removeClass('btn-primary');
            $(this).addClass('btn-success');
            $('#btnCancelar').click();
        }
        n++;
    });
    $('#form_agregar_usuarios').on('submit',function(e){
       e.preventDefault();
        if($('#cboUsuarios').val()!=0){
            $('#divUsuarios').removeClass('has-error');
            $.ajax(
                {
                    type:'POST',
                    url:'/agrega_usuarios_equipo',
                    data:
                    {
                        usuario:$('#cboUsuarios').val(),
                        equipo:$('#idVerEquipo').html(),
                        csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
                    },
                    beforeSend:function()
                    {
                        $('#spinner_agregar_usuario').html('<i class="fa fa-spinner fa-spin fa-3x"></i>');
                    }
                }
            )
            .done(function(respuesta){
                $('#spinner_agregar_usuario').html('');
                if(respuesta=="SI"){
                    alertify.log("Usuario agregado al equipo");
                    limpiar();
                    mostrar_usuarios($('#idVerEquipo').html());
                    cargar_usuarios();
                    $('#contenedorFormulario').fadeOut();
                    $('#abrirForm').html('Agregar Usuario');
                    $('#abrirForm').removeClass('btn-primary');
                    $('#abrirForm').addClass('btn-success');
                    n++;
                }else if(respuesta=="YA_ESTA_EN_EQUIPO"){
                    alertify.error("Este usuario ya se encuentra en otro equipo");
                }else if(respuesta=="Error1"){
                    alertify.error("Ocurrió un error al agregar el usuario al equipo");
                }else if(respuesta=="Error2"){
                    alertify.error("Datos enviados por el método incorrecto");
                }else{
                    alertify.error("Error: "+respuesta);
                }
            });
        }else{

            // VALIDA los errores
            var errores="";
            if($('#cboUsuarios').val()==0){
                $('#divUsuarios').addClass('has-error');
                if(errores!=""){errores+="<br> • Seleccione un usuario";}
                else
                    {errores+=" • Seleccione un Usuario";}
            }else{
                $('#divUsuarios').removeClass('has-error');
            }
            if(errores!=""){
                   alertify.error(errores);
               }
        }
    });

    cargar_usuarios=function(){
        $('#spinner_agregar_usuario').html('<h4>Cargando datos</h4><i class="fa fa-spinner fa-spin fa-3x"></i>');
        var contenido="";
        $.ajax(
            {
                type:'GET',
                url:'/obtener_usuariosDeveloper_paraEquipos',
                success:function(data)
                {
                    contenido+="<option value='0'>- Seleccione un Usuario -</option>";
                    for(var i=0;i<data.length;i++)
                    {
                        contenido+="<option value='"+data[i]['usuario__pk']+"'>"+data[i]['usuario__usuario']+"</option>"
                    }
                }
            }
        )
        .done(function(respuesta){
           $('#spinner_agregar_usuario').html('');
            $('#cboUsuarios').html(contenido);
        });
    };
    cargar_usuarios();
     ver_foto=function(idimagen){
        var ruta_foto=$('#fotoid'+idimagen).attr('ruta_foto');
        $('#imagenModal').attr('src',ruta_foto);
        $('#btnverFoto').click();
    }
    mostrar_usuarios=function(idequipo){

        $.ajax({
            type:'GET',
            url:'/obtener_usuario_Equipo/'+idequipo
        }).done(function(data){
            var DATA=[];
            for(var i=0;i<data.length;i++){
                var produccion=false;
                if(produccion){                                
                    ruta_foto="/media/"+data[i]['usuario__imagen'].substring(2);
                
                    if(ruta_foto=="/media/"){
                        ruta_foto="/media/nofoto.png";
                    }
                }else{
                    ruta_foto="/static/images/"+data[i]['usuario__imagen'].substring(2);
                
                    if(ruta_foto=="/static/images/"){
                        ruta_foto="/static/images/nofoto.png";
                    }
                }
                

                var id=data[i]['usuario__pk'];
                var acciones="";
                var id_usuario_creaciongrupo=data[i]['equipo__id_usuario'];

                var name=data[i]["usuario__usuario"];                                        
                if($('#rolUser').html()=="Administrador"){
                     if($('#idVerEquipo').html()==1){
                        
                        //validar que el usuario sea developer para poder meterlo a un equipo
                        //que yo quiera porque soy admin

                        if(data[i]['usuario__rol']==3){
                            acciones+='<a class="" href="#" onclick="add_to_group('+id+',`'+String(name)+'`)"><i class="fa fa-plus-square fa-3x text-success" aria-hidden="true"></a>';
                        }else{
                            acciones+="<h4>No disponible</h4>";
                        }

                    }else{
                        //Si es FF no lo puedo borrar
                        if(data[i]['usuario__rol']==2){
                            acciones+="<h4>Dueño del Equipo</h4>";
                        }else{
                            acciones+="<a href='#' onclick='sacar_del_equipo("+id+")'><span style='color:#c72439; font-size:20px;' class='glyphicon glyphicon-trash' title='Sacar del Equipo'></span></a>";
                        }
                        
                    }
                }else if($('#rolUser').html()=="Firefighter"){

                    if($('#idUser').html()==id_usuario_creaciongrupo && $('#idVerEquipo').html()!=1){
                        //Solo puedo sacarlo del equipo si es es developer
                        if(data[i]['usuario__rol']==3){
                            acciones+="<a href='#' onclick='sacar_del_equipo("+id+")'><span style='color:#c72439; font-size:20px;' class='glyphicon glyphicon-trash' title='Sacar del Equipo'></span></a>";
                        }else{
                            acciones+="<h4>Owner</h4>";
                        }
                        
                    }else{
                        if(data[i]['usuario__rol']==3){
                            acciones+='<a class="" href="#" onclick="add_to_group('+id+',`'+String(name)+'`)"><i class="fa fa-plus-square fa-3x text-success" aria-hidden="true"></a>';
                        }else{
                            acciones+="<h4>No disponible</h4>";
                        }  
                        //validar que el usuario sea developer para poder meterlo a un equipo
                        //que sea mio porque soy Firefighter                               
                    }
                }else if($('#rolUser').html()=="Developer" || $('#rolUser').html()=="Invitado"){
                    acciones="<h4>No disponible</h4>";
                }
                var rol="";
                if(data[i]['usuario__rol']==1){rol="Administrador";}
                else if(data[i]['usuario__rol']==2){rol="Firefighter";}
                else if(data[i]['usuario__rol']==3){rol="Developer";}
                else if(data[i]['usuario__rol']==4){rol="Invitado";}
                else if(data[i]['usuario__rol']==5){rol="Contacto";}


                fila={};
                fila[0]="<center><img width=50px src='"+ruta_foto+"' id='fotoid"+data[i]['usuario__pk']+"' onclick='ver_foto("+data[i]['usuario__pk']+")' ruta_foto='"+ruta_foto+"' class='img-responsive' alt='Cinque Terre'></center>";
                fila[1]=data[i]['usuario__usuario'];
                fila[2]=rol;
                fila[3]=acciones;
                DATA.push(fila);
            }
            var oTable=$('#tblUsuarios').dataTable();
            oTable.fnClearTable();
            if(DATA.length>0){
                oTable.fnAddData(DATA);
                oTable.fnDraw();
            }else{
                oTable.fnDraw();
            }
        });
    };
    mostrar_usuarios($('#idVerEquipo').html());

    
});