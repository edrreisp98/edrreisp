$(document).on('ready',function(){


    mostrar=function(){
      $.ajax(
          {
              type:'GET',
              url:'/get_suggestions_personals',
              success:function(data)
              {
                var DATA=[];
                for (var i=0;i<data.length;i++){
                    var aplicada="<i style='color:#bae63c;' class='fa fa-check-square' aria-hidden='true'></i>";
                    var id_creo_sugerencia=data[i]['usuario'];

                    if (data[i]['aplicada']==false){
                        aplicada="<i style='color:#c5454e;' class='fa fa-clock-o' aria-hidden='true'></i>"
                    }
                    if($('#idUser').html()==id_creo_sugerencia){//Solo muestro las sugerencias del usuario que esta logeado
                        
                        fila={};
                        fila[0]=data[i]['nombre'];
                        fila[1]=data[i]['telefono'];
                        fila[2]=data[i]['direccion'];
                        fila[3]=data[i]['categoria__nombre'];
                        fila[4]=aplicada;
                        fila[5]=data[i]['fecha'];
                        DATA.push(fila);
                    }
                    
                }
                var oTable=$('#tblSugerencias').dataTable();
                oTable.fnClearTable();
                if(DATA.length>0){
                    oTable.fnAddData(DATA);
                    oTable.fnDraw();
                }else{
                    oTable.fnDraw();
                }

                    
                  
                  
              }
          }
      );
  };
  mostrar();
  
});