$(document).on('ready',function(){
	mostrar=function(){
        $.ajax({
            type:'GET',
            url:'/get_my_orders'
        }).done(function(data){
            var DATA=[];
            for(var i=0;i<data.length;i++){                
                fila={};
                fila[0]=data[i]['pk'],
                fila[1]=data[i]['descripcion'],
                fila[2]=data[i]['contacto__nombre'],
                fila[3]=data[i]['usuario__usuario'],
                fila[4]=data[i]['estado'];
                fila[5]=data[i]['fecha'];
                fila[6]=data[i]['hora'];
                fila[7]=data[i]['total'];
                DATA.push(fila);
            }
            var oTable=$('#tblPedidos').dataTable();
            oTable.fnClearTable();
            if(DATA.length>0){
                oTable.fnAddData(DATA);
                oTable.fnDraw();
            }else{
                oTable.fnDraw();
            }
        });
    }
    mostrar();
 
});