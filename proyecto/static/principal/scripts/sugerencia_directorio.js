$(document).on('ready',function(){
    if($('#rolUser').html()=="Administrador"){
        $('#abrirForm').hide();
    }
    
    $('#contenedorFormulario').hide();


        borrar_sugerencia_directorio=function(id){

            alertify.set({ buttonReverse: true });
            alertify.set({ labels: {
                ok     : "Si",
                cancel : "No"
            } });
            alertify.confirm("Está seguro de eliminar esta sugerencia?", function (e) {
                if (e) {
                    $('#spinner_registro_sugerencia').html('<h4>Eliminando</h4><i class="fa fa-spinner fa-spin fa-3x"></i>');
                    $.ajax(
                        {
                            type:'GET',
                            url:'/eliminar_sugerencia/'+id
                        }
                    )
                    .done(function(respuesta){
                        if(respuesta=="SI"){
                            alertify.log("Sugerencia eliminada correctamente");
                            mostrar();
                        }else if(respuesta=="NO"){
                            alertify.error("Ocurrió un error al eliminar la sugerencia");
                        }else if(respuesta=="No hay sesion"){
                            alertify.error("No puede eliminar la sugerencia, porque no existe sesión");
                        }else{
                            console.log(respuesta);
                        }
                        $('#spinner_registro_sugerencia').html('');
                    });
                }
            });
        };
        $('#btnCancelar').on('click',function(){
            $('#spinner_registro_sugerencia').html('');
              $('#btnAccion').html('Guardar');
              $('#btnAccion').addClass('btn-success');
              $('#btnAccion').removeClass('btn-warning');
              limpiar_directorio();
        });
          var n=0;
          $('#abrirForm').on('click',function(){
              if(n%2==0){
                  $('#contenedorFormulario').fadeIn();
                  $(this).html('Cerrar');
                  $(this).removeClass('btn-success');
                  $(this).addClass('btn-primary');
                  $('#btnCancelar').click();
              }else{
                  $('#contenedorFormulario').fadeOut();
                  $(this).html('Agregar Nueva');
                  $(this).removeClass('btn-primary');
                  $(this).addClass('btn-success');
                  $('#btnCancelar').click();
              }
              n++;
          });
        limpiar_directorio = function(){
            $('#contacto,#telefono,#direccion').val('');
            $('#categoria').val('0');
            $('#divNombre,#divTelefono,#divDireccion,#divCategoria').removeClass('has-error');
        };
        $('#form_registro_sugerencia').on('submit',function(e){
           e.preventDefault();
            if($('#contacto').val()!="" &&
               $('#telefono').val()!="" &&
               $('#direccion').val()!="" &&
               $('#categoria').val()!=0)
            {
              $('#divNombre,#divTelefono,#divDireccion,#divCategoria').removeClass('has-error');
                $.ajax(
                    {
                        type:'POST',
                        url:'/registro_sugerencia',
                        data:
                        {
                            contacto:$('#contacto').val(),
                            telefono:$('#telefono').val(),
                            direccion:$('#direccion').val(),
                            categoria:$('#categoria').val(),
                            csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
                        },
                        beforeSend:function()
                        {
                            $('#spinner_registro_sugerencia').html('<i class="fa fa-spinner fa-spin fa-3x"></i>');
                        }
                    }
                )
                    .done(function(respuesta){
                        $('#spinner_registro_sugerencia').html('');
                        if(respuesta=="SI"){
                            alertify.log("Sugerencia registrada");
                            limpiar_directorio();
                            mostrar();
                            $('#contenedorFormulario').fadeOut();
                            $('#abrirForm').html('Agregar Nueva');
                            $('#abrirForm').removeClass('btn-primary');
                            $('#abrirForm').addClass('btn-success');
                            n++;
                        }else if (respuesta=="Error1") {
                            alertify.error('Ocurrió un error en la creacion de la sugerencia');
                        }else if (respuesta=="Error2") {
                            alertify.error('Datos enviados por el método incorrecto');
                        }else {
                            alertify.error('Ocurrió un error');
                            console.log(respuesta);
                        }
                    });
            }else{
                //VARIABLE DE ERRORES
                var errores="";

                //Valida las alertas
                if($('#categoria').val()=="0"){
                  $('#divCategoria').addClass('has-error');
                  if(errores!=""){errores+="<br> • Seleccione una Categoría";}
                  else
                    {errores+="• Seleccione una categoría";}
                }else{
                  $('#divCategoria').removeClass('has-error');
                }

                if($('#contacto').val()==""){
                  $('#divNombre').addClass('has-error');              
                  if(errores!=""){errores+="<br> • Debe Introducir el Contacto";}
                  else
                    {errores+="• Debe Introducir el Contacto";}
                }else{
                  $('#divNombre').removeClass('has-error');
                }
                if($('#telefono').val()==""){
                  $('#divTelefono').addClass('has-error');
                  if(errores!="") {errores+="<br> • Debe Introducir el Teléfono";}
                  else
                    {errores+="• Debe Introducir el Teléfono";}            
                }else{
                  $('#divTelefono').removeClass('has-error');
                }

                if($('#direccion').val()==""){
                  $('#divDireccion').addClass('has-error');
                  if(errores!=""){errores+="<br> • Debe Introducir la Dirección";}
                  else
                    {errores+="• Debe Introducir la Dirección";}
                }else{
                  $('#divDireccion').removeClass('has-error');
                }

                //Valida los campos
                if(errores!=""){
                  alertify.error(errores);
                }
            }
        });
        ver_foto=function(idimagen){
            var ruta_foto=$('#fotoid'+idimagen).attr('ruta_foto');
            $('#imagenModal').attr('src',ruta_foto);
            $('#btnverFoto').click();
        }

        mostrar=function(){
          $.ajax({
              type:'GET',
              url:'/get_suggestions_directory'
          }).done(function(respuesta){
              var DATA=[];
              for(var i=0;i<respuesta.length;i++){
                  
                var produccion=false;
                if(produccion){                                
                    ruta_foto="/media/"+respuesta[i]['usuario__imagen'].substring(2);
                
                    if(ruta_foto=="/media/"){
                        ruta_foto="/media/nofoto.png";
                    }
                }else{
                    ruta_foto="/static/images/"+respuesta[i]['usuario__imagen'].substring(2);
                
                    if(ruta_foto=="/static/images/"){
                        ruta_foto="/static/images/nofoto.png";
                    }
                }              

                var id = respuesta[i]['pk'];
                var codigo_Poderoso= "<a href='#' onclick='borrar_sugerencia_directorio("+id+")'><span style='color:#e03e3c; font-size:20px;' class='glyphicon glyphicon-trash'></span> </a>";
                var id_creo_sugerencia=respuesta[i]['usuario'];
                var aplicada="<i style='color:#bae63c;' class='fa fa-check-square' aria-hidden='true'></i>";

                if($('#idUser').html()!=id_creo_sugerencia){
                    codigo_Poderoso="<h5>No disponible</h5>"
                }else{
                    if(respuesta[i]['aplicada']==true){
                        codigo_Poderoso="<h5>Ya está aplicada</h5>"
                    }
                }

                if (respuesta[i]['aplicada']==false){
                    aplicada="<i style='color:#c5454e;' class='fa fa-clock-o' aria-hidden='true'></i>"
                }
                
                fila={};
                fila[0]=respuesta[i]['nombre'];
                fila[1]=respuesta[i]['telefono'];
                fila[2]=respuesta[i]['direccion'];
                fila[3]=respuesta[i]['categoria__nombre'];
                fila[4]=respuesta[i]['usuario__usuario'];
                fila[5]="<center><img width=50px src='"+ruta_foto+"' id='fotoid"+respuesta[i]['pk']+"' onclick='ver_foto("+respuesta[i]['pk']+")' ruta_foto='"+ruta_foto+"' class='img-responsive' alt='Cinque Terre'></center>";
                fila[6]=aplicada;
                fila[7]=respuesta[i]['fecha'];
                fila[8]=codigo_Poderoso;
                DATA.push(fila);
              }
              var oTable=$('#tblSugerencias').dataTable();
              oTable.fnClearTable();
              if(DATA.length>0){
                  oTable.fnAddData(DATA);
                  oTable.fnDraw();
              }else{
                  oTable.fnDraw();
              }
          });
        }
        mostrar();

        
});