$(document).on('ready',function(){
  //COMENTARIOS
  $('#contenedorFormularioComentario').hide();   
  var cn=0;
  $('#abrirFormComentario').on('click',function(){
      if(cn%2==0){
          $('#contenedorFormularioComentario').fadeIn();
          $(this).html('Cerrar');
          $(this).removeClass('btn-success');
          $(this).addClass('btn-primary');
          $('#btnCancelar').click();
      }else{
          $('#contenedorFormularioComentario').fadeOut();
          $(this).html('Agregar Nuevo');
          $(this).removeClass('btn-primary');
          $(this).addClass('btn-success');
          $('#btnCancelar').click();
      }
      cn++;
  });
  borrar_comentario=function(id){
      alertify.set({ buttonReverse: true });
      alertify.set({ labels: {
          ok     : "Si",
          cancel : "No"
      } });
      alertify.confirm("Está seguro de eliminar este comentario?", function (e) {
          if (e) {
              $('#spinner_rate').html('<h4>Eliminando</h4><i class="fa fa-spinner fa-spin fa-3x"></i>');
              $.ajax(
                  {
                      type:'GET',
                      url:'eliminar_comentario/'+id
                  }
              )
              .done(function(respuesta){
                  if(respuesta=="SI"){
                      alertify.log("Comentario eliminado correctamente");
                      pagina_actualComentarios=1;
                      cargar_comentarios(pagina_actualComentarios,$('#idVerContacto').html());
                  }else if(respuesta=="NO"){
                      alertify.error("Ocurrió un error al eliminar el comentario");
                  }else{
                      alertify.error("Error: "+respuesta);
                  }
                  $('#spinner_rate').html('');
              });
          }
      });
  };
  $('#btnCancelar').on('click',function(){
      $('#btnAccion').html('Comentar');
      $('#btnAccion').removeClass('btn-warning');
      $('#btnAccion').addClass('btn-primary');
      $('#comentario').val('');        
      $('#divComentario').removeClass('has-error');
  });
  modificar_comentario=function(id){
    $('#spinner_rate').html('<h4>Obteniendo Datos</h4><i class="fa fa-spinner fa-spin fa-3x"></i>');
    $.ajax(
      {
        type:'GET',
        url:'obtener_datos_comentario/'+id,
        success:function(data)
        {
          for(var i=0;i<data.length;i++){
            $('#comentario').val(data[i]['fields']['comentario']);
            $('#comentario2').val(data[i]['pk']);
          }
          $('#spinner_rate').html('');
        }
      }
    );            
  $('#btnAccion').html('Guardar Cambios');
  $('#btnAccion').removeClass('btn-primary');
  $('#btnAccion').addClass('btn-warning');
  };

  var total_paginasComentarios=0;
  var pagina_actualComentarios=1;
  var registroPorPaginaComentarios=5;
  var total_registrosComentarios=0;

  ver_foto=function(idimagen){
      var ruta_foto=$('#fotoid'+idimagen).attr('ruta_foto');
      $('#imagenModal').attr('src',ruta_foto);
      $('#btnverFoto').click();
  }
  cargar_comentarios=function(idcontacto){
    $.ajax(
        {
            type:'GET',
            url:'/obtener_comentarios_contacto/'+idcontacto
        }
    )
    .done(function(respuesta){
      var DATA=[];
      for(var i=0;i<respuesta.length;i++){
        ruta_foto="/media/"+respuesta[i]['usuario__imagen'].substring(2);
                        
        if(ruta_foto=="/media/"){
            ruta_foto="/media/nofoto.png";
        }

        var id=respuesta[i]['pk'];
        var accion="<a href='#' onclick='modificar_comentario("+id+")'><span style='color:#ffc455; font-size:20px;' class='glyphicon glyphicon-pencil'></span> </a>";
        accion+="<a href='#' onclick='borrar_comentario("+id+")'><span style='color:#c72439; font-size:20px;' class='glyphicon glyphicon-trash'></span> </a>";
        if($('#idUser').html()!=respuesta[i]['usuario']){
            accion="<h4>No disponible</h4>";
        }
        fila={};
        fila[0]=respuesta[i]['comentario'];
        fila[1]=respuesta[i]['usuario__usuario'];
        fila[2]="<center><img width=50px src='"+ruta_foto+"' id='fotoid"+respuesta[i]['pk']+"' onclick='ver_foto("+respuesta[i]['pk']+")' ruta_foto='"+ruta_foto+"' class='img-responsive' alt='Cinque Terre'></center>";
        fila[3]=respuesta[i]['fecha'];
        fila[4]=accion;
        DATA.push(fila);
       
      }
      var oTable=$('#tblComentarios').dataTable();
      oTable.fnClearTable();
      if(DATA.length>0){
          oTable.fnAddData(DATA);
          oTable.fnDraw();
      }else{
          oTable.fnDraw();
      }
        
      $('#spinner_rate').html('');
      
    });
  };  

  $('#btnAccion').on('click',function(){
    if($('#comentario').val()!=""){
      $('#divComentario').removeClass('has-error');
      var texto=$(this).html();
      if(texto=="Comentar"){
        $('#spinner_rate').html('<h4>Comentando</h4><i class="fa fa-spinner fa-spin fa-3x"></i>');
        $.ajax(
          {
            type:'GET',
            url:'/registrar_comentario',
            data:
            {
                comentario:$('#comentario').val(),
                usuario:$('#idUser').html(),
                contacto:$('#idVerContacto').html()
            }
          }
        )
        .done(function(respuesta){
          if(respuesta=="SI"){
            alertify.log("Comentario creado exitosamente");
            cargar_comentarios($('#idVerContacto').html());
          }else if(respuesta=="No"){
            alertify.error("Ocurrió un error en la creación del comentario");
          }else if(respuesta=="Error1"){
            alertify.error("Datos enviados por el método incorrecto");
          }else{
            alertify.error("Error: "+respuesta);
          }
          $('#spinner_rate').html('');
          $('#comentario').val('');
        });         
      }else if(texto=="Guardar Cambios"){
        $('#spinner_rate').html('<h4>Guardando cambios</h4><i class="fa fa-spinner fa-spin fa-3x"></i>');
        $.ajax(
          {
            type:'GET',
            url:'/editar_comentario',
            data:
            {
                comentario2:$('#comentario2').val(),
                comentario:$('#comentario').val()
            }
          }
        )
        .done(function(respuesta){
          if(respuesta=="SI"){
            alertify.log("Comentario modificado exitosamente");
            cargar_comentarios($('#idVerContacto').html());
          }else if(respuesta=="No"){
            alertify.error("Ocurrió un error en la modificacion del comentario");
          }else if(respuesta=="Error1"){
            alertify.error("Datos enviados por el método incorrecto");
          }else{
            alertify.error("Error: "+respuesta);
          }
          $('#spinner_rate').html('');
          $('#comentario').val('');
          $('#btnCancelar').click();
        });
      }
    }else{
      var errores="";
      if($('#comentario').val()==""){
        $('#divComentario').addClass('has-error');
        if(errores!=""){
          errores+="<br> • Debe introducir el Comentario";
        }
        else{
          errores+="• Debe introducir el Comentario";
        }
      }else{
        $('#divComentario').removeClass('has-error');
      }
      if(errores!=""){
        alertify.error(errores);
      }
    }
  });

  cargar_comentarios($('#idVerContacto').html());

  




  //INICIO  RATINGS




  vaciar_personal=function(id){
    $('#rate_'+id).swidget().value(0);
  };
    
    
    
  var id_rating_editable;
  cargar_rating_personal=function(idcontacto){
    var contenido="";
    $('#spinner_rate').html('<h4>Cargando datos</h4><i class="fa fa-spinner fa-spin fa-3x"></i>');
    $.ajax(
      {
        type:'GET',
        url:'/obtener_ratings_contacto_personal/'+idcontacto
      }
    )
    .done(function(respuesta){
      if(respuesta.length>0){

        for(var i=0;i<respuesta.length;i++){
          var cuadrado="",vaciar="";


          if($('#idUser').html()==respuesta[i]['usuario']){
            ruta_foto="/media/"+respuesta[i]['usuario__imagen'].substring(2);
                            
            if(ruta_foto=="/media/"){
                ruta_foto="/media/nofoto.png";
            }

            id_rating_editable=respuesta[i]['pk'];
            vaciar="<a onclick='vaciar_personal("+respuesta[i]['pk']+")'>Borrar</a>";
            cuadrado="<div style='background: #214165;border-radius: 3px;color: white;font-weight: bold;padding: 3px 4px;width: 100px;' class='skill pull-left text-centen'>"+respuesta[i]['usuario__usuario']+"</div>";
            contenido+="<li class='list-group-item'>"+
                      "<center><img width=50px src='"+ruta_foto+"' id='fotoid"+respuesta[i]['pk']+"' onclick='ver_foto("+respuesta[i]['pk']+")' ruta_foto='"+ruta_foto+"' class='img-responsive' alt='Cinque Terre'></center>"+
                      "<div style='padding: 3px 4px;' class='skillLineDefault'>"+
                      cuadrado+
                      "<div style='margin-left: 30px;' class='rating' id='rate_"+respuesta[i]['pk']+"'></div>"+
                      vaciar+
                      "</div>"+
                      "</li>";
          }
        }
        $('#RatingPersonal').html(contenido);

        for(var i=0;i<respuesta.length;i++){
          $('#rate_'+respuesta[i]['pk']).shieldRating({
            max: 5,
            step: 1,
            value: respuesta[i]['rating'],
            markPreset: false
          });
        }
        //Como el usuario ha rateado, se deshabilita el boton de crear rate
        $('#btnCrearRate').hide();
        $('#btnRate').show();
      }else{
        $('#btnRate').hide();

        alertify.log("Este usuario no ha rateado a este contacto");

        var cuadrado="",vaciar="";

        cuadrado="<div style='background: #214165;border-radius: 3px;color: white;font-weight: bold;padding: 3px 4px;width: 100px;' class='skill pull-left text-centen'>Rate</div>";
        contenido+="<li class='list-group-item'>"+
                    "<div style='padding: 3px 4px;' class='skillLineDefault'>"+
                    cuadrado+
                    "<div style='margin-left: 30px;' class='rating' id='rate_crear'></div>"+
                    vaciar+
                    "</div>"+
                    "</li>";
        $('#RatingPersonal').html(contenido);
        $('#rate_crear').shieldRating({
          max: 5,
          step: 1,
          value: 0,
          markPreset: false
        });
      }
      $('#spinner_rate').html('');
    });
  };


  $('#btnCrearRate').on('click',function(){
    var rating=parseFloat($('#rate_crear').swidget().value()).toFixed(2);
    $.ajax(
      {
        type:'GET',
        url:'/crearRate',        
        data:{
          rating : rating,
          usuario : $('#idUser').html(),
          contacto : $('#idVerContacto').html()
        },
        beforeSend:function()
        {
            $('#spinner_rate').html('<h4>Creando Rate</h4><i class="fa fa-spinner fa-spin fa-3x"></i>');
        }
      }
    )
    .done(function(respuesta){
      if(respuesta=="SI"){
        cargar_rating_personal($('#idVerContacto').html());
        pagina_actualRatings=1;
        cargar_ratings(pagina_actualRatings,$('#idVerContacto').html());
        alertify.log("Rate Creado correctamente");
      }else if(respuesta=="NO"){
        alertify.error("El rate no pudo ser creado");
      }else{
        alertify.error("Error: "+respuesta);
      }
      $('#spinner_rate').html('');
    });
  });

  var total_paginasRatings=0;
  var pagina_actualRatings=1;
  var registroPorPaginaRatings=5;
  var total_registrosRatings=0;
  cargar_ratings=function(pagina,idcontacto){
    $('#spinner_rate').html('<h4>Cargando datos</h4><i class="fa fa-spinner fa-spin fa-3x"></i>');
    var contenido="";
    var promedio=0;
    $.ajax(
      {
        type:'GET',
        url:'/obtener_ratings_contacto/'+idcontacto
     }
    )
    .done(function(respuesta){

      total_registrosRatings=parseInt(respuesta.length);
      $('#totalRatings').html("Total de Ratings: "+total_registrosRatings);
      total_paginasRatings=parseInt(Math.ceil(total_registrosRatings/registroPorPaginaRatings));      
                    
      if(total_registrosRatings>0){

        console.log("Total de registros: "+total_registrosRatings);
        console.log("numero de paginas: "+total_paginasRatings);
                       

        var inicio=(pagina*registroPorPaginaRatings)-registroPorPaginaRatings;
        var fin=parseInt(inicio)+parseInt(registroPorPaginaRatings);//aqui iba +5                  
        if((total_registrosRatings-inicio)<registroPorPaginaRatings){//aqui iba <5
          fin=inicio+(total_registrosRatings-inicio);
        }               
        $('#paginaActualRatings').html("Página "+pagina+" de "+total_paginasRatings);
                        

        for(var i=inicio;i<fin;i++){

          ruta_foto="/media/"+respuesta[i]['usuario__imagen'].substring(2);
                            
          if(ruta_foto=="/media/"){
              ruta_foto="/media/nofoto.png";
          }

          var cuadrado="",vaciar="";
          if($('#idUser').html()==respuesta[i]['usuario']){
            id_rating_editable=respuesta[i]['pk'];
            
            cuadrado="<div style='background: #5cb85c;border-radius: 3px;color: white;font-weight: bold;padding: 3px 4px;width: 100px;' class='skill pull-left text-centen'>"+respuesta[i]['usuario__usuario']+"</div>";
          }else{
            cuadrado="<div style='background: #e04e5a;border-radius: 3px;color: white;font-weight: bold;padding: 3px 4px;width: 100px;' class='skill pull-left text-centen'>"+respuesta[i]['usuario__usuario']+"</div>";
          }

          contenido+="<li class='list-group-item'>"+
                     "<div style='padding: 3px 4px;' class='skillLineDefault'>"+
                     cuadrado+
                     "<div style='margin-left: 30px;' class='rating' id='rate"+respuesta[i]['pk']+"'></div>"+
                     vaciar+
                     "<center><img width=50px src='"+ruta_foto+"' id='fotoid"+respuesta[i]['pk']+"' onclick='ver_foto("+respuesta[i]['pk']+")' ruta_foto='"+ruta_foto+"' class='img-responsive' alt='Cinque Terre'></center>"+
                     
                     "</div>"+
                     "</li>";
        }

        $('#tablitaRatings').html(contenido);
        $("html, body").animate({ scrollTop: $(document).height()-$(window).height() });
        for(var i=0;i<respuesta.length;i++){
          promedio+=parseFloat(respuesta[i]['rating']);
          $('#rate'+respuesta[i]['pk']).shieldRating({
            max: 5,
            step: 1,
            value: respuesta[i]['rating'],
            markPreset: false
          });
        }

        promedio=parseFloat(promedio/respuesta.length).toFixed(2);
        $('#spinner_rate').html('');
        $('#promedio').html('Rate '+promedio);
      }else{
        $('#paginaActualRatings').html("No hay páginas que mostrar");
        $('#tablitaRatings').html('<h1>Este contacto no cuenta con ningún rate</h1>');               
      }
    });
  };


  $('#btnRate').on('click',function(){
    $.ajax(
      {
        type:'GET',
        url:'/editar_rate/'+parseInt(id_rating_editable),
        data:{
           'rate':parseFloat($('#rate_'+id_rating_editable).swidget().value()).toFixed(2)
        },
        beforeSend:function()
        {
           $('#spinner_rate').html('<h4>Guardando Rate</h4><i class="fa fa-spinner fa-spin fa-3x"></i>');
        }
      }
    )
    .done(function(respuesta){
      if(respuesta=="SI"){
        alertify.log("El Rating personal fue guardado");
        cargar_rating_personal($('#idVerContacto').html());
        pagina_actualRatings=1;
        cargar_ratings(pagina_actualRatings,$('#idVerContacto').html());
      }else if(respuesta=="NO"){
        alertify.error("Ocurrió un error al guardar el rating");
      }else if(respuesta=="Error1"){
        alertify.error("Enviado por el método incorrecto");
      }else{
        alertify.error("Error: "+respuesta);
      }
      $('#spinner_rate').html('');            
    });
  });
  cargar_rating_personal($('#idVerContacto').html());
  cargar_ratings(1,$('#idVerContacto').html());
  
  $('#siguientePaginaRatings').on('click',function(e){
      e.preventDefault();
       if(pagina_actualRatings<total_paginasRatings){
        pagina_actualRatings++;
        cargar_ratings(pagina_actualRatings,$('#idVerContacto').html());
      }     
  });

  $('#anteriorPaginaRatings').on('click',function(e){
      e.preventDefault();
      if(pagina_actualRatings>1){
        pagina_actualRatings--;
        cargar_ratings(pagina_actualRatings,$('#idVerContacto').html());
      }
  });

  $('#primeraPaginaRatings').on('click',function(e){
      e.preventDefault();
      if(pagina_actualRatings>1){
        pagina_actualRatings=1;
        cargar_ratings(pagina_actualRatings,$('#idVerContacto').html());
      }    
  });

  $('#paginaActualRatings').on('click',function(e){
        e.preventDefault();
    });
  $('#ultimaPaginaRatings').on('click',function(e){
      e.preventDefault();
      if(pagina_actualRatings<total_paginasRatings){
        pagina_actualRatings=total_paginasRatings;
        cargar_ratings(pagina_actualRatings,$('#idVerContacto').html());
      }        
  });
});



