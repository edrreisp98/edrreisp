$(document).on('ready',function(){

  $('#editarDireccion').on('click',function(){
    if($(this).html()=="Editar"){
      $('#direccion').show();
      $('#sDireccion').hide();
      $(this).html('Guardar');
      $(this).addClass('btn-primary');
      $(this).removeClass('btn-warning');
    }else if($(this).html()=="Guardar"){    

      if($('#direccion').val()!=""){
        $('#divDireccion').removeClass('has-error');        
        $.ajax({
          type:'GET',
          url:'/cambiame_direccion/'+$('#direccion').val()
        }).done(function(respuesta){
          if(respuesta=="SI"){
            alertify.log("Dirección actualizada");      
            $('#sDireccion').html($('#direccion').val());  
          }else if(respuesta=="NO"){
            alertify.error("La Dirección no pudo ser actualizada"); 
          }else{
            alertify.error("Error: "+respuesta);
          }
        });

        $('#direccion').hide();
        $('#sDireccion').show();
        $(this).html('Editar');
        $(this).addClass('btn-warning');
        $(this).removeClass('btn-primary');
      }else{
        $('#divDireccion').addClass('has-error');
      }
    }    
  });

  $('#editarNombre').on('click',function(){
    if($(this).html()=="Editar"){
      $('#nombre').show();
      $('#sNombre').hide();
      $(this).html('Guardar');
      $(this).addClass('btn-primary');
      $(this).removeClass('btn-warning');
    }else if($(this).html()=="Guardar"){    

      if($('#nombre').val()!=""){
        $('#divNombre').removeClass('has-error');        
        $.ajax({
          type:'GET',
          url:'/cambiame_nombre/'+$('#nombre').val()
        }).done(function(respuesta){
          if(respuesta=="SI"){
            alertify.log("Nombre actualizado");      
            $('#sNombre').html($('#nombre').val());  
          }else if(respuesta=="NO"){
            alertify.error("El Nombre no pudo ser actualizado"); 
          }else{
            alertify.error("Error: "+respuesta);
          }
        });

        $('#nombre').hide();
        $('#sNombre').show();
        $(this).html('Editar');
        $(this).addClass('btn-warning');
        $(this).removeClass('btn-primary');
      }else{
        $('#divNombre').addClass('has-error');
      }
    }    
  });

  function guardar_correo(){
    $('#divCorreo').removeClass('has-error');        
    $.ajax({
      type:'GET',
      url:'/cambiame_correo/'+$('#correo').val()
    }).done(function(respuesta){
      if(respuesta=="SI"){
        alertify.log("Correo actualizado");      
        $('#sCorreo').html($('#correo').val());  
      }else if(respuesta=="NO"){
        alertify.error("El Correo no pudo ser actualizado"); 
      }else{
        alertify.error("Error: "+respuesta);
      }
    });
    $('#correo').hide();
    $('#sCorreo').show();
    $('#editarCorreo').html('Editar');
    $('#editarCorreo').addClass('btn-warning');
    $('#editarCorreo').removeClass('btn-primary');
  }
  function validaCorreo($email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test( $email );
  }
  validateEmail=function (pEmail) {
    var filterValue = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
    if (filterValue.test(pEmail)) {
        if (pEmail.indexOf('@intellisys.com.do', pEmail.length - '@intellisys.com.do'.length) != -1 ||
            pEmail.indexOf('@cincinnatus.com.do', pEmail.length - '@cincinnatus.com.do'.length) != -1)
        {
            return true;
        }
        else {
            return false;
        }
    }
    else
    {
        return false;
    }
  }
  $('#editarCorreo').on('click',function(){
    if($(this).html()=="Editar"){
      $('#correo').show();
      $('#sCorreo').hide();
      $(this).html('Guardar');
      $(this).addClass('btn-primary');
      $(this).removeClass('btn-warning');
    }else if($(this).html()=="Guardar"){

      if($('#rol').val()=="Invitado"){
        if($('#correo').val()!="" && validaCorreo($('#correo').val())){
          guardar_correo();
        }else{
          $('#divCorreo').addClass('has-error');
          if($('#correo').val()==""){
            alertify.error("Debe introducir el correo");
          }else{
            alertify.error("Correo no válido");            
          }
        }
      }else{
        if($('#correo').val()!="" && validateEmail($('#correo').val())){
          guardar_correo();
        }else{
          $('#divCorreo').addClass('has-error');          
          if($('#correo').val()==""){
            alertify.error("Debe introducir el correo");
          }else{
            alertify.error("Correo no válido");            
          }
        }
      }     
    }    
  });
  $('#form_foto').on('submit',function(e){
    e.preventDefault();
    if($('#editarFoto').html()=="Editar foto"){
      $('#imagen').show();
      $('#editarFoto').html('Guardar');
      $('#editarFoto').addClass('btn-primary');
      $('#editarFoto').removeClass('btn-success');
    }else if($('#editarFoto').html()=="Guardar"){

      $('#imagen').hide();
      $('#editarFoto').html('Editar foto');
      $('#editarFoto').addClass('btn-success');
      $('#editarFoto').removeClass('btn-primary');
      formdata=new FormData();
      var file=$('#imagen')[0].files[0];
      if(formdata){
        formdata.append("imagen",file);
        formdata.append("csrfmiddlewaretoken",$('input[name=csrfmiddlewaretoken]').val());                         
      }
      $.ajax({        
        url:'/cambiame_foto',
        data:formdata,
        processData: false,
        contentType: false,
        type:'POST'      
      }).done(function(respuesta){
        if($('#imagen').val()){
          if(respuesta=="SI"){
            $('#imagen').val('');
            alertify.log("La imagen fue actualizada");
            $.ajax({
                type:'GET',
                url:'/cual_es_mi_foto',
                success:function(respuesta)
                {
                    for(var i=0;i<respuesta.length;i++){
                        ruta_foto="/media/"+respuesta[i]['imagen'].substring(2);                            
                        if(ruta_foto=="/media/"){
                            ruta_foto="/media/nofoto.png";
                        }
                        $('#fotoDePerfil,#fotico').attr('src',ruta_foto);
                    }
                    
                }
            });
          }else if(respuesta=="NO"){
            alertify.error("La imagen no pudo ser actualizada");
          }else{
            alertify.error("Error: "+respuesta);
          }
        }else{
          if(respuesta=="SI"){
            alertify.log("La imagen fue removida");
            $('#fotico,#fotoDePerfil').attr('src','/media/nofoto.png');
          }else if(respuesta=="NO"){
            alertify.error("La imagen no pudo ser removida");
          }else{
            alertify.error("Error: "+respuesta);
          }
        }
      });
      
      
    }
  });
});