$(document).on('ready',function(){
    
     ver_foto=function(idimagen){
        var ruta_foto=$('#fotoid'+idimagen).attr('ruta_foto');
        $('#imagenModal').attr('src',ruta_foto);
        $('#btnverFoto').click();
    }

    mostrar=function(){
      $.ajax({
        type:'GET',
        url:'/get_directory_list'
      }).done(function(respuesta){
        var DATA=[];
        for(var i=0;i<respuesta.length;i++){

          ruta_foto="/media/"+respuesta[i]['usuario__imagen'].substring(2);
                            
          if(ruta_foto=="/media/"){
              ruta_foto="/media/nofoto.png";
          }
          var rate=0;
          if(respuesta[i]['average_rating']!=null){
            rate=parseFloat(respuesta[i]['average_rating']).toFixed(2);
          }
          var activo="<i style='color: #e04e5a;' class='fa fa-ban fa-2x' aria-hidden='true'></i>";
          if(respuesta[i]['activo']==true){
            activo="<i style='color: #27B04B;' class='fa fa-check-circle-o fa-2x' aria-hidden='true'></i>";
          }
          var disponible="<i class='fa fa-frown-o fa-3x' style='color:#DE5B50;' aria-hidden='true'></i>";


          var now     = new Date(); 
          var hour    = now.getHours();
          var minute  = now.getMinutes();
          var second  = now.getSeconds(); 
          if(hour.toString().length == 1) {
              var hour = '0'+hour;
          }
          if(minute.toString().length == 1) {
              var minute = '0'+minute;
          }
          if(second.toString().length == 1) {
              var second = '0'+second;
          }   
          var hora = hour+':'+minute+':'+second;   
          

          if(hora>=respuesta[i]['desde'] && hora<=respuesta[i]['hasta']){
            disponible="<i class='fa fa-smile-o fa-3x' style='color:#1ABB48;' aria-hidden='true'></i>";
          }

          fila={};
          fila[0]=respuesta[i]['pk'];
          fila[1]=respuesta[i]['categoria__nombre'];
          fila[2]=respuesta[i]['nombre'];
          fila[3]=respuesta[i]['desde'];
          fila[4]=respuesta[i]['hasta'];
          fila[5]=disponible;
          fila[6]=rate;
          fila[7]=respuesta[i]['telefono'];
          fila[8]=respuesta[i]['direccion'];
          fila[9]=respuesta[i]['usuario__usuario'];
          fila[10]="<center><img width=50px src='"+ruta_foto+"' id='fotoid"+respuesta[i]['pk']+"' onclick='ver_foto("+respuesta[i]['pk']+")' ruta_foto='"+ruta_foto+"' class='img-responsive' alt='Cinque Terre'></center";
          fila[11]=activo;
          fila[12]="<a href='/ver_contacto/"+respuesta[i]['pk']+"'><span style='color:#5ea1c7; font-size:20px;' class='glyphicon glyphicon-eye-open' title='Ver Contacto'></span></a>";
          DATA.push(fila);
        }
        var oTable=$('#tblContactos').dataTable();
        oTable.fnClearTable();
        if(DATA.length>0){
          oTable.fnAddData(DATA);
          oTable.fnDraw();
        }else{
          oTable.fnDraw();
        }
      });
    };
    mostrar();
});
