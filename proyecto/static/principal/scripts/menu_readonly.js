$(document).on('ready',function(){
	

	var table = $('#tblMenus').DataTable( {
        rowReorder: {
            selector: 'td:nth-child(2)'
        },
        responsive: true,
        "language": {
          "search": "Buscar:",
          "searchPlaceholder": "Buscar por columnas",
          "lengthMenu": "Mostrar _MENU_ registros por página",
          "zeroRecords": "Nada encontrado",
          "info": "Mostrando página _PAGE_ de _PAGES_",
          "infoEmpty": "No hay registros disponibles",
          "infoFiltered": "(filtrado desde _MAX_ registros totales)",
          "paginate": {
            "previous": "Anterior",
            "next":"Siguiente"
          }
        },"aoColumnDefs": [
                { "sClass": "text-center", "aTargets": [0,1,2,3] }
            ]
    });

    var table2 = $('#tblItems').DataTable( {
        rowReorder: {
            selector: 'td:nth-child(2)'
        },
        responsive: true,
        "language": {
          "search": "Buscar:",
          "searchPlaceholder": "Buscar por columnas",
          "lengthMenu": "Mostrar _MENU_ registros por página",
          "zeroRecords": "Nada encontrado",
          "info": "Mostrando página _PAGE_ de _PAGES_",
          "infoEmpty": "No hay registros disponibles",
          "infoFiltered": "(filtrado desde _MAX_ registros totales)",
          "paginate": {
            "previous": "Anterior",
            "next":"Siguiente"
          }
        },"aoColumnDefs": [
                { "sClass": "text-center", "aTargets": [0,1,2,3] }
            ]
    });


	ver_items=function(idmenu){
		location.assign("/ver_items/"+idmenu);
		
	}
});