$(document).on('ready',function(){
  
  $('#audio_fb')[0].volume=0;
  $('#audio_fb')[0].play();
  marcar_como_leida=function(id){
    alertify.set({ buttonReverse: true});
    alertify.set({ labels: {
        ok     : "Si",
        cancel : "No"
    } });
    alertify.confirm("Está seguro de marcar como leída esta notificación?", function (e) {
        if (e) {
            $.ajax(
                {
                    type:'GET',
                    url:'/marcar_como_leida/'+id
                }
            )
            .done(function(respuesta){
                if(respuesta=="SI"){
                  //Solo los administradores pueden ver las notificaciones de sugerencia
                  cargarTODAS();   
                  alertify.log("Notificación marcada como leida");           
                }else if(respuesta=="NO"){
                    alertify.error("Ocurrió un error al marcar como leída la notificación");
                }else{
                    alertify.error("Error: "+ respuesta);
                }
            });
        }
    });
  };  
  
  cargarNotificacionSugerencias=function (){
    var tabla="";
    $.ajax({
      type:'GET',
      url:'/obtener_sugerencias_notificacion',
      success:function(data)
      {
        for (var i=0;i<data.length;i++){
          var id=data[i]['pk'];
          var nombre=data[i]['nombre'];
          var fecha=data[i]['fecha'];
                   
          
          tabla +="<tr>" +
                "<td class='text-center' style='color:#352F83'>Tiene una sugerencia</td>" +  
                "<td class='text-center'>"+nombre+"</td>" +
                "<td class='text-center'><p>No disponible</p></td>" +                
                "<td class='text-center'>"+fecha+"</td>" +
                "<td class='text-center'><a class='btn btn-danger' href='/vista_detalle/"+id+"'>Ver</a></td>" +                
            "</tr>";           
          
        }
        
      }
    }).done(function(){
      $('#contenido_tabla').append(tabla)
    });
  };

  cargar_notificacion_admins_pedidos_nuevos=function(){
    var tabla="";
    $.ajax({
      type:'GET',
      url:'/obtener_notificacion_admins_pedidos_nuevos',
      success:function(data)
      {
        for (var i=0;i<data.length;i++){
          var id=data[i]['pk'];
          var pedido=data[i]['pedido__descripcion'];
          var total=data[i]['pedido__total'];
          var fecha=data[i]['fecha'];
                   
          
          tabla +="<tr>" +
                "<td class='text-center' style='color:#352F83'>Pedido Nuevo</td>" +  
                "<td class='text-center'>"+pedido+"</td>" +
                "<td class='text-center'>"+total+"</td>" +                
                "<td class='text-center'>"+fecha+"</td>" +
                "<td class='text-center'><a class='btn btn-danger' onclick='marcar_como_leida("+id+")'>Marcar como leida</a></td>" +                
            "</tr>";           
          
        }
        
      }
    }).done(function(){
      $('#contenido_tabla').append(tabla)
    });
  }
  cargar_notificacion_admins_pedidos_llegados=function(){
    var tabla="";
    $.ajax({
      type:'GET',
      url:'/obtener_notificacion_admins_pedidos_llegados',
      success:function(data)
      {
        for (var i=0;i<data.length;i++){
          var id=data[i]['pk'];
          var pedido=data[i]['pedido__descripcion'];
          var total=data[i]['pedido__total'];
          var fecha=data[i]['fecha'];
                   
          
          tabla +="<tr>" +
                "<td class='text-center' style='color:#352F83'>El pedido llegó</td>" +  
                "<td class='text-center'>"+pedido+"</td>" +
                "<td class='text-center'>"+total+"</td>" +                
                "<td class='text-center'>"+fecha+"</td>" +
                "<td class='text-center'><a class='btn btn-danger' onclick='marcar_como_leida("+id+")'>Marcar como leida</a></td>" +                
            "</tr>";           
          
        }
        
      }
    }).done(function(){
      $('#contenido_tabla').append(tabla)
    });
  }
  cargar_notificacion_users_pedidos_llegados=function(){
    var tabla="";
    $.ajax({
      type:'GET',
      url:'/obtener_notificacion_users_pedido_llego',
      success:function(data)
      {
        for (var i=0;i<data.length;i++){
          var id=data[i]['pk'];
          var pedido=data[i]['pedido__descripcion'];
          var total=data[i]['pedido__total'];
          var fecha=data[i]['fecha'];
                   
          
          tabla +="<tr>" +
                "<td class='text-center' style='color:#352F83'>Su Pedido llegó</td>" +  
                "<td class='text-center'>"+pedido+"</td>" +
                "<td class='text-center'>"+total+"</td>" +                
                "<td class='text-center'>"+fecha+"</td>" +
                "<td class='text-center'><a class='btn btn-danger' onclick='marcar_como_leida("+id+")'>Marcar como leida</a></td>" +                
            "</tr>";           
          
        }
        
      }
    }).done(function(){
      $('#contenido_tabla').append(tabla)
    });
  }
  cargarTODAS=function(){
    $('#contenido_tabla').html('');
    //Solo los administradores pueden ver las notificaciones de sugerencia
    if($('#txtRol').val()=="Administrador"){
      cargarNotificacionSugerencias();    
      cargar_notificacion_admins_pedidos_nuevos();
      cargar_notificacion_admins_pedidos_llegados();
    }
    cargar_notificacion_users_pedidos_llegados()


  };

  cargarTODAS();

  


});


                               
                                    
                                    
                                    
                                    
                                    
                                
                                    
                                    
                                    
                                    
                                 
                                 
                               
                              