$(document).on('ready',function(){
	if($('#txtNAlerta').val()=="True"){
		$('#chkAplicacion').prop( "checked", true );
	}
	if($('#txtNCorreo').val()=="True"){
		$('#chkCorreo').prop( "checked", true );
	}
	if($('#txtNSonido').val()=="True"){
		$('#chkSonido').prop( "checked", true );
	}
	$('#horas').val($('#txtHoras').val());
	$('#minutos').val($('#txtMinutos').val());
	$('#form_configuracion').on('submit',function(e){
		e.preventDefault();
		var n_alerta=false,n_correo=false,n_sonido=false;
		if($('#chkAplicacion').is(":checked")){
			n_alerta=true;
		}
		if($('#chkCorreo').is(":checked")){
			n_correo=true;
		}
		if($('#chkSonido').is(":checked")){
			n_sonido=true;
		}
		if($('#horas').val()>=00 && $('#horas').val()<=23 &&
			$('#minutos').val()>=00 && $('#minutos').val()<=60){
			$('#divHoras,#divMinutos').removeClass('has-error');
			$.ajax({
				type:'POST',
				url:'/guardar_configuracion',
				data:
				{
					n_alerta:n_alerta,
	                n_correo:n_correo,
	                n_sonido:n_sonido,
	                horas:$('#horas').val(),
	                minutos:$('#minutos').val(),
	                csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
				}
			}).done(function(respuesta){
				if(respuesta=="SI"){
					alertify.log("Configuración guardada");
				}else if(respuesta=="NO"){
					alertify.error("Ocurrió un error al guardar la configuración");
				}else{
					alertify.error("Error: "+respuesta);
				}
			});
		}else{
			var errores="";

			if($('#horas').val()<00 || $('#horas').val()>23){
              $('#divHoras').addClass('has-error');
              if(errores!=""){errores+="<br> • El rango en hora es 00-23";}
              else
                {errores+="• El rango en hora es 00-23";}
            }else{
              $('#divHoras').removeClass('has-error');
            }

            if($('#minutos').val()<00 || $('#minutos').val()>23){
              $('#divMinutos').addClass('has-error');
              if(errores!=""){errores+="<br> • El rango en minuto es 00-60";}
              else
                {errores+="• El rango en minuto es 00-60";}
            }else{
              $('#divMinutos').removeClass('has-error');
            }

            //Valida los campos
            if(errores!=""){
              alertify.error(errores);
            }
		}
		
	});
});