$(document).on('ready',function(){
	$('#contacto').val('0');
	$('#contenedorFormulario').hide();


	$('#btnCancelar').on('click',function(){
        $('#spinner_registro_usuario').html('');
        $('#btnAccion').html('Guardar');
        $('#btnAccion').addClass('btn-success');
        $('#btnAccion').removeClass('btn-warning');
        limpiar();
    });

    var n=0;
    $('#abrirForm').on('click',function(){
        if(n%2==0){
            $('#contenedorFormulario').fadeIn();
            $(this).html('Cerrar');
            $(this).removeClass('btn-success');
            $(this).addClass('btn-primary');
            $('#btnCancelar').click();
        }else{
            $('#contenedorFormulario').fadeOut();
            $(this).html('Agregar Nuevo');
            $(this).removeClass('btn-primary');
            $(this).addClass('btn-success');
            $('#btnCancelar').click();
        }
        n++;
    });
    limpiar = function(){
        $('#menu').val('');
        $('#contacto').val(0);
        $('#divContacto,#divMenu').removeClass('has-error');        
    };
    borrar_menu = function(id){
        alertify.set({ buttonReverse: true });
        alertify.set({ labels: {
            ok     : "Si",
            cancel : "No"
        } });
        alertify.confirm("Está seguro de eliminar este menú?", function (e) {
            if (e) {
                $('#spinner_registro_usuario').html('<h4>Eliminando</h4><i class="fa fa-spinner fa-spin fa-3x"></i>');
                $.ajax(
                    {
                        type:'GET',
                        url:'/eliminar_menu/'+id
                    }
                )
                .done(function(respuesta){
                    if(respuesta=="SI"){
                        alertify.log("Menú eliminado correctamente");
                        mostrar();
                    }else if(respuesta=="NO"){
                        alertify.error("Ocurrió un error al eliminar el menú");
                    }else{
                        alertify.error("Error: "+respuesta)
                    }
                    $('#spinner_registro_usuario').html('');
                });
            }
        });
    };
	
	$('#form_registro_menus').on('submit',function(e){
		e.preventDefault();
        var texto = $('#btnAccion').html();
        var url="/registro_menu";
        if(texto!="Guardar"){
        	url="modifica_menu";
        }
        if($('#menu').val()!="" &&
        	$('#contacto').val()!=0){
        	$('#divMenu,#divContacto').removeClass('has-error');
        	$.ajax({
        		type:'POST',
        		url:url,
        		data:
        		{
        			idmenu:$('#menu2').val(),
        			nombre:$('#menu').val(),
        			contacto:$('#contacto').val(),
        			csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
        		},
                beforeSend:function()
                {
                    $('#spinner_registro_usuario').html('<i class="fa fa-spinner fa-spin fa-3x"></i>');
                }
        	}).done(function(respuesta){
        		$('#spinner_registro_usuario').html('');
                if(respuesta=="SI"){
                    if(texto=="Guardar"){
                        alertify.log('Menú registrado');
                    }else{
                        alertify.log('Menú actualizado');
                    }
                    
                    limpiar();
                    mostrar();
                    $('#contenedorFormulario').fadeOut();
                    $('#abrirForm').html('Agregar Nuevo');
                    $('#abrirForm').removeClass('btn-primary');
                    $('#abrirForm').addClass('btn-success');
                    n++;
                }else if(respuesta=="Error1"){
                    if(texto=="Guardar"){
                        alertify.error('Ocurrió un error en la creación del menú');
                    }else{
                        alertify.error('Ocurrió un error en la edición del menú');
                    }
                    
                }else if(respuesta=="Error2"){
                    alertify.error('Datos enviados por el método incorrecto');
                }else{
                    alertify.error("Error: "+respuesta)
                }
        	});
        }else{
        	var errores="";
            if($('#menu').val()==""){
                $('#divMenu').addClass('has-error');
                if(errores!=""){errores+="<br> • Debe introducir el Menú";}
                else
                    {errores+="• Debe introducir el Menú";}
            }else{
                $('#divMenu').removeClass('has-error');
            }

            if($('#contacto').val()==0){
                $('#divContacto').addClass('has-error');
                if(errores!=""){errores+="<br> • Seleccione un Establecimiento";}
                else
                    {errores+="• Seleccione un Establecimiento";}
            }else{
                $('#divContacto').removeClass('has-error');
            }
            if(errores!=""){
                alertify.error(errores);
            }
        }
    });    
    modificar_menu = function(id){
        $('#divMenu,#divContacto').removeClass('has-error');
        $('#spinner_registro_usuario').html('<h4>Obteniendo datos</h4><i class="fa fa-spinner fa-spin fa-3x"></i>');
        $.ajax(
            {
                type:'GET',
                url:'/obtener_datos_menu/'+id,
                success:function(data)
                {
                    for(var i=0;i<data.length;i++){
                        $('#menu').val(data[i]['fields']['nombre']);
                        $('#contacto').val(data[i]['fields']['contacto']);
                        $('#menu2').val(data[i]['pk']);
                    }
                    $('#spinner_registro_usuario').html('');
                }
            }
        );
        $('#btnAccion').html('Guardar Cambios');
        $('#btnAccion').removeClass('btn-success');
        $('#btnAccion').addClass('btn-warning');
        
        $('#contenedorFormulario').fadeIn();
        $('#abrirForm').html('Cerrar');
        $('#abrirForm').removeClass('btn-success');
        $('#abrirForm').addClass('btn-primary');
        n++;
    };
    mostrar=function(){
        $.ajax({
            type:'GET',
            url:'/get_menus'
        }).done(function(respuesta){
            var DATA=[];
            for(var i=0;i<respuesta.length;i++){
                
                fila={};
                fila[0]=respuesta[i]['pk'];
                fila[1]=respuesta[i]['nombre'];
                fila[2]=respuesta[i]['contacto__nombre'];
                var editar="<a href='#' onclick='modificar_menu("+respuesta[i]['pk']+")'><span title='Modificar' style='color:#ffc455; font-size:20px;' class='glyphicon glyphicon-pencil'></span> </a>";
                var eliminar="<a href='#' onclick='borrar_menu("+respuesta[i]['pk']+")'><span title='Eliminar' style='color:#c72439; font-size:20px;' class='glyphicon glyphicon-trash'></span> </a>";
                var ver_menu="<a href='/administracion/ver_menu/"+respuesta[i]['pk']+"'><span title='Ver Items' style='color:#2892A3; font-size:20px;' class='glyphicon glyphicon-list-alt'></span> </a>";
                fila[3]=editar+eliminar+ver_menu;
                DATA.push(fila);
            }
            var oTable=$('#tblMenus').dataTable();
            oTable.fnClearTable();
            if(DATA.length>0){
                oTable.fnAddData(DATA);
                oTable.fnDraw();
            }else{
                oTable.fnDraw();
            }
        });
    }
    mostrar();
    
});