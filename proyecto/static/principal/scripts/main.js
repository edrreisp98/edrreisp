$(document).on('ready',function(){

    logear=function(usuario,clave){
        metodo_ajax_login(usuario,clave,"SI");
        $('#login-form').show();
        $('#credencial-form').hide();
        $('#login-form-link').addClass('active');
        $('#credencial-form-link').removeClass('active');
    };
    limpiar_registro=function (todo){
        if(todo){
            $('#mensaje_error_registro,#mensaje_exito_registro').html('');
        }
        $('#nombre_creacion,#usuario_creacion,#email_creacion,#contraseña_creacion,#contraseña_creacion2,#direccion_creacion').val('');
        $('#spinner_register').html('');

    };
    limpiar_login=function(todo){
        if(todo){
            $('#mensaje_error_login,#mensaje_exito_login').html('');
        }
        $('#usuario,#contraseña').val('');
        $('#recordar').attr('checked', false);
        $('#spinner_login').html('');
    };
    var existe_usuario=0;
    //Cuando se manda el formulario de registro
    $('#registro-form').on('submit',function(e){
        e.preventDefault();        
        
        if($('#usuario_creacion').val()!="" &&
           $('#email_creacion').val()!="" &&
           $('#nombre_creacion').val()!="" &&
           $('#direccion_creacion').val()!="" &&
            ($('#contraseña_creacion').val()==$('#contraseña_creacion2').val()) &&
            $('#mensaje_error_registro').html()!="<p>Usuario ya registrado</p>"
            && existe_usuario==0)
        {
            $('#mensaje_error_registro').html('');
            var codigo=Math.floor(Math.random() * 999999999) + 1 ;
            $.ajax(
                {
                    type:'GET',
                    url:'/registro',
                    data:{
                        nombre:$('#nombre_creacion').val(),
                        usuario:$('#usuario_creacion').val(),
                        direccion:$('#direccion_creacion').val(),
                        correo:$('#email_creacion').val(),
                        clave:$('#contraseña_creacion').val(),
                        codigo:codigo
                    },
                    beforeSend:function()
                    {
                        $('#spinner_register').html('<i class="fa fa-spinner fa-spin fa-3x"></i>');
                        $('#mensaje_exito_registro').html('Registrando...');
                    },
                    success:function()
                    {
                        console.log("Success");
                        $('#mensaje_exito_registro').html('');
                    }
                }
            )
            .done(function(respuesta){
                $('#spinner_register').html('');
                if(respuesta=="SI"){
                    $('#mensaje_error_registro').html('');
                    $('#mensaje_exito_registro').html('Usuario registrado, Se ha enviado un link de activación a su correo');
                    limpiar_registro(false);
                }else if(respuesta=="Error1"){
                    $('#mensaje_exito_registro').html('');
                    $('#mensaje_error_registro').html('Ocurrió un error en la creacion del usuario');
                }else if(respuesta=="Error2"){
                    $('#mensaje_exito_registro').html('');
                    $('#mensaje_error_registro').html('Datos enviados por el método incorrecto');
                }else{
                    $('#mensaje_error_registro').html('Error: '+respuesta);
                }
            });
        }
        else {
            if($('#mensaje_error_registro').html()=='<p>Usuario ya registrado</p>' || existe_usuario==1){
                $('#mensaje_error_registro').html("Este usuario ya se encuentra registrado");
            }else if($('#contraseña_creacion').val() != $('#contraseña_creacion2').val()){
                $('#mensaje_error_registro').html("Las contraseñas no coinciden");
            }else if ($('#nombre_creacion,#usuario_creacion,#email_creacion,#contraseña_creacion,#contraseña_creacion2').val()=="") {
                $('#mensaje_error_registro').html("Los campos están incompletos");
            }else {
                $('#mensaje_error_registro').html("Error en el formulario");
            }
        }
    });
    $('#usuario_creacion').on('keyup',function () {
        if($('#usuario_creacion').val()!=""){
            $.ajax(
                {
                    type:'GET',
                    url:'/comprobarUsuario/'+$('#usuario_creacion').val(),
                    beforeSend:function()
                    {},
                    success:function()
                    {}
                }
            )
           .done(function(respuesta){
                console.log(respuesta);
                if(respuesta=="SI"){
                    existe_usuario=1;
                    $('#spinner_register').html('<i style="color:#c72439" class="fa fa-check fa-2x" aria-hidden="true"></i>');
                    $('#mensaje_error_registro').html('<p>Usuario ya registrado</p>');
                }else if(respuesta=="NO"){
                    existe_usuario=0;
                    $('#spinner_register').html('<i style="color:#6bff66" class="fa fa-check  fa-2x" aria-hidden="true"></i>');
                    $('#mensaje_error_registro').html('');
                }
                console.log(existe_usuario);
           })
        }else{
            $('#spinner_register').html('');
        }               
    });
    metodo_ajax_login=function(usuario,clave,cerrar_sesion){
        $.ajax(
            {

                type:'POST',
                url:'/login',
                data:{
                    usuario:usuario,
                    clave:clave,
                    cerrar_sesion:cerrar_sesion,
                    csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
                },
                beforeSend:function()
                {
                    $('#spinner_login').html('<i class="fa fa-spinner fa-spin fa-3x"></i>');
                    $('#mensaje_exito_login').html('Logeando...');
                },
                success:function()
                {
                    $('#spinner_login').html('');
                    $('#mensaje_exito_login').html('');
                    $('#mensaje_error_login').html('');
                }
            }
        )
        .done(function(respuesta){
            if(respuesta=="SI"){
                $('#mensaje_error_login').html('');
                $('#mensaje_exito_login').html(':)');
                limpiar_registro(false);
                location.assign("/principal");
            }else if(respuesta=="No-activo"){
                $('#mensaje_error_login').html('Este usuario no está activado');
                $('#mensaje_exito_login').html('');
            }else if(respuesta=="Error1"){
                $('#mensaje_error_login').html('Datos enviados por el método incorrecto');
                $('#mensaje_exito_login').html('');
            }else if(respuesta=="Error2"){
                $('#mensaje_error_login').html('Datos de Inicio de Sesión Incorrectos');
                $('#mensaje_exito_login').html('');
            }else{
                $('#mensaje_error_login').html('Ocurrió un error');
                $('#mensaje_exito_login').html('');
                console.log(respuesta);
            }

        });
    };

    //Cuando se manda el formulario de login
    $('#login-form').on('submit',function(e){
        e.preventDefault();
        var cerrar_sesion = "SI";
        if($("#recordar").is(':checked')){
           cerrar_sesion="NO";
        }
        metodo_ajax_login($('#usuario').val(),$('#contraseña').val(),cerrar_sesion);
    })
});
