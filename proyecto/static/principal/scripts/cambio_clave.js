$(document).on('ready',function(){
	limpiar=function(){
		$('#claveAnterior,#claveNueva,#claveNueva2').val('');
		$('#fotoUsuarioEditar').attr('alt',"");
        $('#fotoUsuarioEditar').attr('src',"");
	}

	$('#form_cambio_clave').on('submit',function(e){
		e.preventDefault();
		if($('#claveAnterior').val()!="" &&
			$('#claveNueva').val()!="" &&
			$('#claveNueva2').val()!="" &&
			($('#claveNueva').val()==$('#claveNueva2').val())&&
			($('#txtClaveActual').val()==$('#claveAnterior').val())){
				$('#divClaveNueva1,#divClaveNueva2,#divClaveOriginal').removeClass('has-error');

				$.ajax({
					type:'POST',
					url:'/cambio_clave',
					data:{
						clave:$('#claveNueva').val(),
						csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
					},
                    beforeSend:function()
                    {
                        $('#spinner_cambio_clave').html('<i class="fa fa-spinner fa-spin fa-3x"></i>');
                    }
				}).done(function(respuesta){
					$('#spinner_cambio_clave').html('');
					if(respuesta=="SI"){
						alertify.log("Contraseña actualizada");
						limpiar();
						setTimeout(function(){
						  window.location.href = '/principal';
						}, 2000);
					}else if(respuesta="NO"){
						alertify.error("No se pudo cambiar la contraseña");
					}else if(respuesta="Error1"){
						alertify.error("Ocurrió un error al cambio la contraseña");
					}else if(respuesta="Error2"){
						alertify.error("Datos enviados por el método incorrecto");
					}
				});
				
		}else{
			var errores="";
            if($('#claveAnterior').val()==""){
                $('#divClaveOriginal').addClass('has-error');
                if(errores!=""){errores+="<br> • Debe introducir la clave Actual";}
                else
                    {errores+="• Debe introducir la clave Actual";}
            }else{
                $('#divClaveOriginal').removeClass('has-error');
                if($('#txtClaveActual').val()!=$('#claveAnterior').val()){
                	$('#divClaveOriginal').addClass('has-error');
	                if(errores!=""){errores+="<br> • La clave actual no es la correcta";}
	                else
	                    {errores+="• La clave actual no es la correcta";}
                }else{
                	$('#divClaveOriginal').removeClass('has-error');
                }
            }

            if($('#claveNueva').val()=="" || $('#claveNueva2').val()==""){
            	if($('#claveNueva').val()==""){
	                $('#divClaveNueva1').addClass('has-error');
	                if(errores!=""){errores+="<br> • Debe introducir la clave Nueva";}
	                else
	                    {errores+="• Debe introducir la clave Nueva";}
	            }else{
	                $('#divClaveNueva1').removeClass('has-error');
	            }

	            if($('#claveNueva2').val()==""){
	                $('#divClaveNueva2').addClass('has-error');
	                if(errores!=""){errores+="<br> • Debe repetir la clave Nueva";}
	                else
	                    {errores+="• Debe repetir la clave Nueva";}
	            }else{
	                $('#divClaveNueva2').removeClass('has-error');
	            }

            }else{
            	//No estan vacias
            	$('#divClaveNueva1,#divClaveNueva2').removeClass('has-error');

            	if($('#claveNueva').val()!=$('#claveNueva2').val()){
	            	$('#divClaveNueva1,#divClaveNueva2').addClass('has-error');

	            	if(errores!=""){errores+="<br> • Las contraseñas no coinciden";}
	                else
	                    {errores+="• Las contraseñas no coinciden";}
	            }else{
	            	$('#divClaveNueva1,#divClaveNueva2').removeClass('has-error');
	            }
            }        

            if(errores!=""){
                alertify.error(errores);
            }
		}
	})
});