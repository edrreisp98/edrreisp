

$(document).on('ready',function(){
    $('#registro-form').hide();
    $('#login-form').show();

    $('#login-form-link').on('click',function() {
        $('#registro-form').hide();
        $('#login-form').show();
        $('#registro-form-link').removeClass('active');
        $('#login-form-link').addClass('active');
        limpiar_login(true);
    });
    $('#registro-form-link').on('click',function(){
        $('#login-form').hide();
        $('#registro-form').show();
        $('#login-form-link').removeClass('active');
        $('#registro-form-link').addClass('active');
        limpiar_registro(true);
    });        
});

